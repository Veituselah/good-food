<?php

namespace App\Entity\Product;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Media\ProductMedia;
use App\Entity\Product\Supplier\ProductSupplier;
use App\Entity\Restaurant\Restaurant;
use App\Interfaces\TestableEntityInterface;
use App\Repository\ProductRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Mapping\Annotation\Slug;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use JetBrains\PhpStorm\Pure;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


#[Entity(repositoryClass: ProductRepository::class)]
#[InheritanceType("JOINED")]
#[DiscriminatorColumn(name: "type", type: "string")]
#[DiscriminatorMap(
    [
        "purchasable" => "App\Entity\Product\PurchasableProduct\PurchasableProduct",
        "food"        => "App\Entity\Product\PurchasableProduct\Food\Food",
        "entree"      => "App\Entity\Product\PurchasableProduct\Food\Entree",
        "drink"       => "App\Entity\Product\PurchasableProduct\Food\Drink",
        "dish"        => "App\Entity\Product\PurchasableProduct\Food\Dish",
        "dessert"     => "App\Entity\Product\PurchasableProduct\Food\Dessert",
        "menu"        => "App\Entity\Product\PurchasableProduct\Menu\Menu",
        "variation"   => "App\Entity\Product\PurchasableProduct\Variation\Variation",
    ])]
#[HasLifecycleCallbacks()]
#[UniqueEntity(fields: ["reference"], message: "La référénce est déjà utilisée !")]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        "get" => [],
    ],
    itemOperations: [
        "get" => [],
    ],
    denormalizationContext: [
        'groups' => [],
    ],
    normalizationContext: [
        'groups' => [
            "product", "product:read",
        ],
    ]
)]
abstract class Product implements TestableEntityInterface
{

    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[Column(type: "float", nullable: true)]
    #[GreaterThanOrEqual(
        value: 0,
        message: "La profondeur doit être supérieure ou égale à {{ compared_value }}"
    )]
    #[Groups([
        'product',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
    ])]
    private ?float $depth;

    #[Column(type: "text", nullable: true)]
    #[Groups([
        "product",
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
    ])]
    private ?string $description;

    #[Column(type: "string", length: 13, nullable: true)]
    #[Length(
        min: 13,
        max: 13,
        minMessage: "Le code ean13 est composé de {{limit}} caractères.",
        maxMessage: "Le code ean13 est composé de {{limit}} caractères."
    )]
    #[Groups([
        "product",
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
    ])]
    private ?string $ean13;

    #[Column(type: "float", nullable: true)]
    #[GreaterThanOrEqual(
        value: 0,
        message: "La profondeur doit être supérieure ou égale à {{ compared_value }}"
    )]
    #[Groups([
        "product",
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
    ])]
    private ?float $height;

    /**
     * @var ProductMedia[]|Collection
     */
    #[OneToMany(mappedBy: 'product', targetEntity: ProductMedia::class, orphanRemoval: true)]
    #[Groups([
        "product",
        'order:read',
        'order_line:read',
    ])]
    #[ApiProperty(writableLink: false)]
    private array|Collection $medias;

    #[Column(type: "string", length: 100)]
    #[NotBlank(message: "Le nom doit être renseigné.")]
    #[Length(
        min: 3,
        max: 100,
        minMessage: "Le nom doit faire au moins {{limit}} caractères.",
        maxMessage: "Le nom doit faire moins de {{limit}} caractères."
    )]
    #[Groups([
        "product",
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
    ])]
    private string $name;

    /**
     * @var Restaurant[]|Collection
     */
    #[ManyToMany(targetEntity: Restaurant::class, inversedBy: "products")]
    #[JoinTable(name: "product_in_restaurant")]
    #[JoinColumn(
        name: "product",
        referencedColumnName: "reference"
    )]
    #[InverseJoinColumn(
        name: "restaurant",
        referencedColumnName: "slug"
    )]
    #[Groups([
        "product",
    ])]
    #[ApiProperty(writableLink: false)]
    private array|Collection $onlyInRestaurants;

    #[Id()]
    #[Column(type: "string", length: 15)]
    #[NotBlank(message: "La référence doit être renseignée.")]
    #[Length(
        min: 5,
        max: 15,
        minMessage: "La référence doit faire au moins {{limit}} caractères.",
        maxMessage: "La référence doit faure au maximum {{limit}} caractères."
    )]
    #[Groups([
        'product:read',
        'product:post',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
    ])]
    private string $reference;

    #[Slug(fields: ["name"])]
    #[Column(type: "string", length: 50, unique: true)]
    #[ApiProperty(identifier: true)]
    #[Groups([
        'product:read',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
    ])]
    private string $slug;

    /**
     * @var ProductSupplier[]|Collection
     */
    #[OneToMany(mappedBy: 'product', targetEntity: ProductSupplier::class, orphanRemoval: true)]
    #[Groups('product')]
    #[ApiProperty(writableLink: false)]
    private array|Collection $suppliers;

    #[Column(type: "string", length: 12, nullable: true)]
    #[Length(
        min: 14,
        max: 14,
        minMessage: "Le code upc est composé de {{limit}} caractères.",
        maxMessage: "Le code upc est composé de {{limit}} caractères."
    )]
    #[Groups([
        "product",
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
    ])]
    private ?string $upc;

    #[Column(type: "float", nullable: true)]
    #[GreaterThanOrEqual(
        value: 0,
        message: "Le poids doit être supérieure ou égale à {{ compared_value }}"
    )]
    #[Groups([
        "product",
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
    ])]
    private ?float $weight;

    #[Column(type: "float", nullable: true)]
    #[GreaterThanOrEqual(
        value: 0,
        message: "La largeur doit être supérieure ou égale à {{ compared_value }}"
    )]
    #[Groups([
        "product",
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
    ])]
    private ?float $width;

    #[Pure] public function __construct()
    {
        $this->onlyInRestaurants = new ArrayCollection();
        $this->suppliers = new ArrayCollection();
        $this->medias = new ArrayCollection();
    }

    public function addMedia(ProductMedia $media): self
    {
        if (!$this->medias->contains($media)) {
            $this->medias[] = $media;
            $media->setProduct($this);
        }

        return $this;
    }

    public function addOnlyInRestaurant(Restaurant $onlyInRestaurant): self
    {
        if (!$this->onlyInRestaurants->contains($onlyInRestaurant)) {
            $this->onlyInRestaurants[] = $onlyInRestaurant;
        }

        return $this;
    }

    public function addSupplier(ProductSupplier $supplier): self
    {
        if (!$this->suppliers->contains($supplier)) {
            $this->suppliers[] = $supplier;
            $supplier->setProduct($this);
        }

        return $this;
    }

    function autoActiveOnCreation(): bool
    {
        return false;
    }

    public function getDepth(): ?float
    {
        return $this->depth;
    }

    public function setDepth(?float $depth): self
    {
        $this->depth = $depth;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description ?? null;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEan13(): ?string
    {
        return $this->ean13;
    }

    public function setEan13(?string $ean13): self
    {
        $this->ean13 = $ean13;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(?float $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return Collection|ProductMedia[]
     */
    public function getMedias(): Collection|array
    {
        return $this->medias;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Restaurant[]|Collection
     */
    public function getOnlyInRestaurants(): array|Collection
    {
        return $this->onlyInRestaurants;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug ?? null;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|ProductSupplier[]
     */
    public function getSuppliers(): Collection|array
    {
        return $this->suppliers;
    }

    public function getUpc(): ?string
    {
        return $this->upc;
    }

    public function setUpc(?string $upc): self
    {
        $this->upc = $upc;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(?float $width): self
    {
        $this->width = $width;

        return $this;
    }

    function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function removeMedia(ProductMedia $media): self
    {
        if ($this->medias->removeElement($media)) {
            // set the owning side to null (unless already changed)
            if ($media->getProduct() === $this) {
                $media->setProduct(null);
            }
        }

        return $this;
    }

    public function removeOnlyInRestaurant(Restaurant $onlyInRestaurant): self
    {
        $this->onlyInRestaurants->removeElement($onlyInRestaurant);

        return $this;
    }

    public function removeSupplier(ProductSupplier $supplier): self
    {
        if ($this->suppliers->removeElement($supplier)) {
            // set the owning side to null (unless already changed)
            if ($supplier->getProduct() === $this) {
                $supplier->setProduct(null);
            }
        }

        return $this;
    }

    public function getEntityName(): string
    {
        return "Produit";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "products";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    public function getEntityIdentifierName(): string
    {
        return "reference";
    }

}
