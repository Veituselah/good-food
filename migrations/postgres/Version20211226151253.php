<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211226151253 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE address ALTER additional_address DROP NOT NULL');
        $this->addSql('ALTER TABLE address RENAME COLUMN post_code TO zip_code');
        $this->addSql('ALTER TABLE user_request ALTER expired_at TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE user_request ALTER expired_at DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN user_request.expired_at IS NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE address ALTER additional_address SET NOT NULL');
        $this->addSql('ALTER TABLE address RENAME COLUMN zip_code TO post_code');
        $this->addSql('ALTER TABLE user_request ALTER expired_at TYPE TIMESTAMP(0) WITHOUT TIME ZONE');
        $this->addSql('ALTER TABLE user_request ALTER expired_at DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN user_request.expired_at IS \'(DC2Type:datetime_immutable)\'');
    }
}
