<?php

namespace App\Validator\Constraint;

use App\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

#[\Attribute]
class CheckUserWithEmailExistsValidator extends ConstraintValidator
{

    public function __construct(
        private UserRepository $userRepository
    )
    {
    }

    public function validate(mixed $value, Constraint $constraint)
    {
        if (!$constraint instanceof CheckUserWithEmailExists) {
            throw new UnexpectedTypeException($constraint, CheckUserWithEmailExists::class);
        }

        $user = $this->userRepository->findOneBy(['email' => $value]);

        if (empty($user)) {
            $this->context->buildViolation('L\'email renseigné n\'existe pas')->addViolation();
        }
    }

}