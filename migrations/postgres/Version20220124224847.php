<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220124224847 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE voucher_data_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE voucher (code VARCHAR(10) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, activated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(code))');
        $this->addSql('COMMENT ON COLUMN voucher.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN voucher.activated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE voucher_data (id INT NOT NULL, voucher_id VARCHAR(10) NOT NULL, currency_id VARCHAR(10) NOT NULL, name VARCHAR(50) NOT NULL, available_from TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, available_to TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, amount_reduction_tax_excluded DOUBLE PRECISION DEFAULT NULL, free_shipping_cost BOOLEAN NOT NULL, percentage_reduction_tax_excluded DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AC9E73DB28AA1B6F ON voucher_data (voucher_id)');
        $this->addSql('CREATE INDEX IDX_AC9E73DB38248176 ON voucher_data (currency_id)');
        $this->addSql('CREATE TABLE voucher_data_purchasable_product (voucher_data_id INT NOT NULL, purchasable_product_reference VARCHAR(15) NOT NULL, PRIMARY KEY(voucher_data_id, purchasable_product_reference))');
        $this->addSql('CREATE INDEX IDX_A3C5FF2FF4388B3B ON voucher_data_purchasable_product (voucher_data_id)');
        $this->addSql('CREATE INDEX IDX_A3C5FF2F504C6194 ON voucher_data_purchasable_product (purchasable_product_reference)');
        $this->addSql('CREATE TABLE voucher_data_customer (voucher_data_id INT NOT NULL, customer_uuid UUID NOT NULL, PRIMARY KEY(voucher_data_id, customer_uuid))');
        $this->addSql('CREATE INDEX IDX_CA5D5E0AF4388B3B ON voucher_data_customer (voucher_data_id)');
        $this->addSql('CREATE INDEX IDX_CA5D5E0A85EEF4D1 ON voucher_data_customer (customer_uuid)');
        $this->addSql('COMMENT ON COLUMN voucher_data_customer.customer_uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE voucher_data ADD CONSTRAINT FK_AC9E73DB28AA1B6F FOREIGN KEY (voucher_id) REFERENCES voucher (code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE voucher_data ADD CONSTRAINT FK_AC9E73DB38248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE voucher_data_purchasable_product ADD CONSTRAINT FK_A3C5FF2FF4388B3B FOREIGN KEY (voucher_data_id) REFERENCES voucher_data (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE voucher_data_purchasable_product ADD CONSTRAINT FK_A3C5FF2F504C6194 FOREIGN KEY (purchasable_product_reference) REFERENCES purchasable_product (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE voucher_data_customer ADD CONSTRAINT FK_CA5D5E0AF4388B3B FOREIGN KEY (voucher_data_id) REFERENCES voucher_data (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE voucher_data_customer ADD CONSTRAINT FK_CA5D5E0A85EEF4D1 FOREIGN KEY (customer_uuid) REFERENCES customer (uuid) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE voucher_data DROP CONSTRAINT FK_AC9E73DB28AA1B6F');
        $this->addSql('ALTER TABLE voucher_data_purchasable_product DROP CONSTRAINT FK_A3C5FF2FF4388B3B');
        $this->addSql('ALTER TABLE voucher_data_customer DROP CONSTRAINT FK_CA5D5E0AF4388B3B');
        $this->addSql('DROP SEQUENCE voucher_data_id_seq CASCADE');
        $this->addSql('DROP TABLE voucher');
        $this->addSql('DROP TABLE voucher_data');
        $this->addSql('DROP TABLE voucher_data_purchasable_product');
        $this->addSql('DROP TABLE voucher_data_customer');
    }
}
