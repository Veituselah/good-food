<?php

namespace App\Command\TestBook\Classes\Model;

class TestLine
{

    const TEST_OK = 'OK';
    const TEST_KO = 'KO';
    const TEST_NOT_TESTED = 'N.T.';

    const HEADER = [
        "ID",
        "Tâche",
        "Etape",
        "Niveau",
        "Attendu",
        "Code résultat",
        "Commentaire",
        "Deadline",
    ];

    public int $id = 0;

    public string $task = "";

    public string $step = "";

    public string $level = "";

    public string $waitingResult = "";

    public string $resultCode = "";

    public string $deadline = "";

    public string $commentary = "";

    public function toArray(): array
    {
        return [
            $this->id,
            $this->task,
            $this->step,
            $this->level,
            $this->waitingResult,
            $this->resultCode,
            $this->commentary,
            $this->deadline,
        ];
    }

}