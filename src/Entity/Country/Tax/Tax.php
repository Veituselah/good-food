<?php

namespace App\Entity\Country\Tax;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Country\Country;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\TaxRepository;
use App\Service\Tools\Tools;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[Entity(repositoryClass: TaxRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        //	Tout le monde peut récupérer la liste des taxes
        "get"  => [],
        // Seuls les admins ou + peuvent créer un taxe
        "post" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    itemOperations: [
        //	Tout le monde peut récupérer la liste des taxes
        "get"    => [],
        // Seuls les admins ou + peuvent modifier une taxe
        "patch"  => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
        // Seuls les admins ou + peuvent supprimer une taxe
        "delete" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "tax", "tax:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "tax", "tax:read",
        ],
    ],
)]
class Tax implements TestableEntityInterface
{

    use SlugNameTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[ManyToOne(targetEntity: Country::class, inversedBy: "taxes")]
    #[JoinColumn(referencedColumnName: "iso_code", nullable: false)]
    #[NotNull(message: "La pays où s'applique la taxe doit être renseignée.")]
    #[Groups([
        "tax",
        'purchasable_product:read',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private Country $country;

    #[Column(type: "string", length: 30)]
    #[NotBlank(message: "Le nom doit être défini.")]
    #[Length(
        min: 2,
        max: 30,
        minMessage: "Le nom doit faire plus de {{ limit }} caractères.",
        maxMessage: "Le nom doit faire moins de {{ limit }} caractères."
    )]
    #[Groups([
        "tax",
        'purchasable_product:read',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])] private string $name;

    #[Column(type: "decimal", precision: 5, scale: 3)]
    #[NotNull(message: "Le montant de la taxe doit être renseigné.")]
    #[GreaterThan(
        value: 0,
        message: "Le montant de la taxe doit être supérieur à {{ compared_value }}."
    )]
    #[LessThan(
        value: 1,
        message: "Le montant de la taxe doit être inférieur à {{ compared_value }}."
    )]
    #[Groups([
        "tax",
        'purchasable_product:read',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
    ])]
    private float $rate;

    #[ManyToOne(targetEntity: TaxType::class, inversedBy: "taxes")]
    #[JoinColumn(nullable: false)]
    #[NotNull(message: "La type de taxe doit être renseigné.")]
    #[Groups("tax")]
    private TaxType $type;

    function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    #[Pure] public function getRate(): ?string
    {
        return Tools::formatPercentage($this?->rate);
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getType(): ?TaxType
    {
        return $this->type;
    }

    public function setType(?TaxType $type): self
    {
        $this->type = $type;

        return $this;
    }

    function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function getEntityName(): string
    {
        return "Taxe";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "taxes";
    }

    public function getEntityIdentifierName(): string
    {
        return "slug";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get",
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
