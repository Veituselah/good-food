<?php


namespace App\Controller\User\Request;


use ApiPlatform\Core\Validator\Exception\ValidationException;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\User\UserRequest\NewPasswordRequest;
use App\Entity\User\UserRequest\UserRequest;
use App\Event\UserRequestCheckValidityBeforeCreationEvent;
use App\Repository\CustomerRepository;
use App\Repository\UserRepository;
use App\Repository\UserRequestRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserRequestCreationAction extends AbstractController
{

    const DELAY_BETWEEN_TWO_REQUESTS_ERROR = "Vous devez attendre au moins {{ min }} minutes entre deux requêtes.";

    public static function getDelayMessageError(int $minutes): string
    {
        return str_replace("{{ min }}", $minutes, self::DELAY_BETWEEN_TWO_REQUESTS_ERROR);
    }

    /**
     * @throws Exception
     */
    #[NoReturn] public function __invoke(UserRequest $data, EntityManagerInterface $entityManager, EventDispatcherInterface $dispatcher, UserRepository $userRepository, ValidatorInterface $validator): UserRequest
    {
        $validator->validate($data);

        if ($data instanceof NewPasswordRequest && empty($data->getUserTarget())) {
            $data->setUserTarget(
                $userRepository->findOneBy(['email' => $data->email])
            );
        }

        if(empty($data->getUserTarget())){
            throw new ValidationException('Le compte utilisateur renseigné n\'existe pas');
        }

        /**
         * @var UserRequestRepository $repository
         */
        $repository = $entityManager->getRepository(get_class($data));

        $hasPendingRequestWhichDosntHaveReachTimeBeforeNextRequest =
            $repository->hasPendingRequestWhichDosntHaveReachTimeBeforeNextRequest(
                $data->getUserTarget()
            );

        if ($hasPendingRequestWhichDosntHaveReachTimeBeforeNextRequest) {
            throw new BadRequestHttpException(self::getDelayMessageError($data->getDelayInMinuteBetweenTwoRequest()));
        }

        $event = new UserRequestCheckValidityBeforeCreationEvent($data);
        $dispatcher->dispatch($event, UserRequestCheckValidityBeforeCreationEvent::NAME);

        return $data;
    }

}