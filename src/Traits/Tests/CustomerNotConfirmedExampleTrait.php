<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\User\Customer\Customer;
use App\Tests\unit\CustomerTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

trait CustomerNotConfirmedExampleTrait
{
    use UserExampleTrait;

    private ?string $customerNotConfirmedExampleIndex;
    private ?Customer $customerNotConfirmedExample;
    private ?string $customerNotConfirmedExampleUsername;
    private ?string $customerNotConfirmedExamplePassword;
    private ?string $customerNotConfirmedExampleToken;
    private ?string $customerNotConfirmedExampleItemIRI;


    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function loadCustomerNotConfirmed()
    {
        $this->customerNotConfirmedExampleIndex = CustomApiTestCase::ROLE_CUSTOMER_NOT_CONFIRMED;
        $this->customerNotConfirmedExample = CustomApiTestCase::getUserFixturesClean()[$this->customerNotConfirmedExampleIndex];
        $this->customerNotConfirmedExampleUsername = $this->customerNotConfirmedExample->getUserIdentifier();
        $this->customerNotConfirmedExamplePassword = CustomApiTestCase::PASSWORDS[$this->customerNotConfirmedExampleIndex];
        $this->customerNotConfirmedExampleToken = $this->getTokenForUser($this->customerNotConfirmedExampleUsername, $this->customerNotConfirmedExamplePassword);
        $this->customerNotConfirmedExampleItemIRI = CustomApiTestCase::replaceParamsRoute(CustomerTest::CUSTOMER_ITEM_ROUTE, ['uuid' => $this->customerNotConfirmedExample->getUuid()]);
    }
}