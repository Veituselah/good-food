<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220122164251 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE extra_extra_value (extra_id INT NOT NULL, extra_value_id INT NOT NULL, PRIMARY KEY(extra_id, extra_value_id))');
        $this->addSql('CREATE INDEX IDX_7411C1902B959FC6 ON extra_extra_value (extra_id)');
        $this->addSql('CREATE INDEX IDX_7411C190B7FCE3F1 ON extra_extra_value (extra_value_id)');
        $this->addSql('ALTER TABLE extra_extra_value ADD CONSTRAINT FK_7411C1902B959FC6 FOREIGN KEY (extra_id) REFERENCES extra (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE extra_extra_value ADD CONSTRAINT FK_7411C190B7FCE3F1 FOREIGN KEY (extra_value_id) REFERENCES extra_value (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE extra DROP CONSTRAINT fk_4d3f0d65b7fce3f1');
        $this->addSql('DROP INDEX idx_4d3f0d65b7fce3f1');
        $this->addSql('ALTER TABLE extra DROP extra_value_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE extra_extra_value');
        $this->addSql('ALTER TABLE extra ADD extra_value_id INT NOT NULL');
        $this->addSql('ALTER TABLE extra ADD CONSTRAINT fk_4d3f0d65b7fce3f1 FOREIGN KEY (extra_value_id) REFERENCES extra_value (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_4d3f0d65b7fce3f1 ON extra (extra_value_id)');
    }
}
