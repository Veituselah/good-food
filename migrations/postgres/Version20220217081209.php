<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220217081209 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_purchasable_product_food_extra_extra_group_restaurant DROP CONSTRAINT fk_a428d96b307610c4');
        $this->addSql('DROP SEQUENCE product_purchasable_product_food_extra_extra_group_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE comment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE comment (id INT NOT NULL, message TEXT DEFAULT NULL, note INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, activated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN comment.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN comment.deleted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN comment.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN comment.activated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('DROP TABLE product_purchasable_product_food_extra_extra_group');
        $this->addSql('DROP TABLE product_purchasable_product_food_extra_extra_group_restaurant');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE comment_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE product_purchasable_product_food_extra_extra_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE product_purchasable_product_food_extra_extra_group (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE product_purchasable_product_food_extra_extra_group_restaurant (product_purchasable_product_food_extra_extra_group_id INT NOT NULL, restaurant_id INT NOT NULL, PRIMARY KEY(product_purchasable_product_food_extra_extra_group_id, restaurant_id))');
        $this->addSql('CREATE INDEX idx_a428d96bb1e7706e ON product_purchasable_product_food_extra_extra_group_restaurant (restaurant_id)');
        $this->addSql('CREATE INDEX idx_a428d96b307610c4 ON product_purchasable_product_food_extra_extra_group_restaurant (product_purchasable_product_food_extra_extra_group_id)');
        $this->addSql('ALTER TABLE product_purchasable_product_food_extra_extra_group_restaurant ADD CONSTRAINT fk_a428d96b307610c4 FOREIGN KEY (product_purchasable_product_food_extra_extra_group_id) REFERENCES product_purchasable_product_food_extra_extra_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_purchasable_product_food_extra_extra_group_restaurant ADD CONSTRAINT fk_a428d96bb1e7706e FOREIGN KEY (restaurant_id) REFERENCES restaurant (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE comment');
    }
}
