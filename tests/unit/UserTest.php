<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\SuperAdmin\SuperAdmin;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\CustomerConfirmedExampleTrait;
use App\Traits\Tests\LogisticianExampleTrait;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class UserTest extends CrudApiTestCase
{

    use LogisticianExampleTrait;
    use CustomerConfirmedExampleTrait;

    // Routes
    const USER_ROUTE = self::API_ROUTE . '/users';
    const USER_ITEM_ROUTE = self::USER_ROUTE . '/{uuid}';
    const USER_UPDATE_ROLES_ROUTE = self::USER_ITEM_ROUTE . '/roles';

    // JSON LD Data
    const USER_TYPE = 'User';
    const USER_CONTEXT = self::CONTEXT . '/' . self::USER_TYPE;

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadCustomerConfirmed();
        $this->loadLogistician();
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $verifJSON = $json;
                unset($verifJSON['password']);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::USER_ROUTE,
                    context: self::USER_CONTEXT,
                    classToCheckValidity: User::class,
                    totalItems: count(self::getUserFixtures())
                );

            },
            method: self::GET_METHOD,
            url: self::USER_ROUTE,
        );
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetItem(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => CustomerTest::CUSTOMER_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => CustomerTest::CUSTOMER_TYPE,
                        "lastname"                  => $this->customerConfirmedExample->getLastname(),
                        "firstname"                 => $this->customerConfirmedExample->getFirstname(),
                        "email"                     => $this->customerConfirmedExample->getEmail(),
                        "roles"                     => $this->customerConfirmedExample->getRoles(),
                        'active'                    => $this->customerConfirmedExample->isActive(),
                        'deleted'                   => $this->customerConfirmedExample->isDeleted(),
                    ]
                );

                $customer = $response->toArray();

                $this->assertIdentifierOfResponseData(CustomerTest::CUSTOMER_ROUTE, Tools::UUID_REGEX, $customer);
                $this->assertMatchesResourceItemJsonSchema(User::class);

            },
            method: self::GET_METHOD,
            url: self::replaceParamsRoute(self::USER_ITEM_ROUTE, ['uuid' => $this->customerConfirmedExample->getUuid()]),
        );
    }

    public function testDeleteItem()
    {
        $this->inexistantEndpointTest();
    }

    public function testPatchInvalidItem()
    {
        $this->inexistantEndpointTest();
    }

    public function testPatchItem()
    {
        $this->inexistantEndpointTest();
    }

    public function testPostInvalidItem()
    {
        $this->inexistantEndpointTest();
    }

    public function testPostItem()
    {
        $this->inexistantEndpointTest();
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testUpdateRolesUser(): void
    {
        $json = [
            'roles' => [
                FranchiseEmployee::ROLE_LEAD,
            ],
        ];

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_LEAD,
                self::ROLE_HR,
                self::ROLE_ADMIN,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains($json);
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => FranchiseEmployeeTest::FRANCHISE_EMPLOYEE_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => FranchiseEmployeeTest::FRANCHISE_EMPLOYEE_TYPE,
                        'active'                    => $this->logisticianExample->isActive(),
                        'deleted'                   => $this->logisticianExample->isDeleted(),
                    ]
                );

                $employee = $response->toArray();

                $this->assertIdentifierOfResponseData(FranchiseEmployeeTest::FRANCHISE_EMPLOYEE_ROUTE, Tools::UUID_REGEX, $employee);
                $this->assertMatchesResourceItemJsonSchema(User::class);

                $this->initClient();

            },
            method: self::PATCH_METHOD,
            url: self::replaceParamsRoute(self::USER_UPDATE_ROLES_ROUTE, ['uuid' => $this->logisticianExample->getUuid()]),
            json: $json
        );
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testUpdateRolesUserEmptyRoles()
    {
        $json = [
            'roles' => [],
        ];

        $token = $this->getTokenFromUserIndex(self::CEDRIC_ADMIN);

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: self::replaceParamsRoute(self::USER_UPDATE_ROLES_ROUTE, ['uuid' => $this->logisticianExample->getUuid()]),
            json: $json,
            token: $token,
        );

        $this->assertAccessDenied();
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testUpdateRolesUserRoleDoesNotExistForEntity()
    {
        $json = [
            'roles' => [
                SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN,
            ],
        ];

        $token = $this->getTokenFromUserIndex(self::CEDRIC_ADMIN);

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: self::replaceParamsRoute(self::USER_UPDATE_ROLES_ROUTE, ['uuid' => $this->logisticianExample->getUuid()]),
            json: $json,
            token: $token,
        );

        $this->assertBadRequest();
    }

}
