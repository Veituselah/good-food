<?php


namespace App\Event;


use App\Entity\User\UserRequest\UserRequest;
use Symfony\Contracts\EventDispatcher\Event;

class UserRequestCompleteEvent extends Event
{
    public const NAME = 'user_request.complete';

    public function __construct(
        protected UserRequest $request,
    )
    {
    }

    public function getRequest(): UserRequest
    {
        return $this->request;
    }
}