<?php

namespace App\Controller\User\Customer;

use App\Repository\CountryRepository;
use App\Repository\CustomerRepository;
use App\Repository\RestaurantRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListCustomersInReservationsAction extends AbstractController
{

    public function __construct(
        private CustomerRepository $customerRepository
    )
    {
    }

    public function __invoke(): array
    {
        return $this->customerRepository->getCustomersInReservations();
    }

}