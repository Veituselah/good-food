<?php


namespace App\Traits\Fields\Date;

use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait DeletedAtTrait
 *
 * @package App\Traits\Fields
 */
trait DeletedAtTrait
{

    #[Column(type: "datetime_immutable", nullable: true)]
    private ?DateTimeImmutable $deletedAt;

    public function delete(bool $delete): self
    {
        $this->deletedAt = $delete ? new DateTimeImmutable() : null;

        return $this;
    }

    public static function getDateTimeClearValue(): DateTime
    {
        return new DateTime();
    }

    public function getDeletedAt(): ?DateTimeImmutable
    {
        return $this->deletedAt ?? null;
    }

    public function setDeletedAt(?DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public static function getIntegerClearValue(): int
    {
        return -1;
    }

    public static function getStringClearValue(): string
    {
        return 'CLEARED';
    }

    #[Groups([
        "delete:read",
        "product:read",
        "supplier:read",
        "variation_value:read",
        "variation_group:read",
        "tax:read",
        "tax_type:read",
        "user:read",
        "address:read",
        "category:read",
        "country:read",
        "currency:read",
        "restaurant:read",
        "extra_group:read",
        "extra_value:read",
        "payment_method:read",
        'voucher:read',
        'voucher_data:read',
        "carrier:read",
        "carrier_data:read",
        'order:read',
        'order_line:read',
        'order:read',
        'reservation:read',
    ])]
    public function isDeleted(): bool
    {
        return !empty($this->deletedAt);
    }

    protected function purgeData()
    {
        // DO nothing by default
    }

    abstract public function purgeDataOnDelete(): bool;

}