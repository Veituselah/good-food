<?php

namespace App\Command\TestBook\Classes\Test;

use App\Command\TestBook\Classes\Model\Entity;
use App\Command\TestBook\Classes\Model\TestLine;

use ArrayIterator;
use Generator;
use JetBrains\PhpStorm\Pure;

abstract class TestType
{

    const GET = "GET";
    const POST = "POST";
    const PUT = "PUT";
    const PATCH = "PATCH";
    const DELETE = "DELETE";

    const DEADLINE = "17/08/2022";

    protected int $idTest = 1;

    public function __construct(
        protected Entity $entity
    )
    {
    }

    /**
     * @return TestLine[]
     */
    public function getLinesToWriteInXlsx(): array
    {
        $this->idTest = 1;

        return array_merge(
            iterator_to_array($this->getLinesToWriteInXlsxForCollectionOperation()),
            iterator_to_array($this->getLinesToWriteInXlsxForItemOperation()),
            iterator_to_array($this->getLinesToWriteInXlsxForSubResourceOperation()),
            iterator_to_array($this->getOtherLinesToWriteInXlsx()),
        );
    }


    /**
     * @return iterable<TestLine>
     */
    protected abstract function getLinesToWriteInXlsxForCollectionOperation(): iterable;

    /**
     * @return iterable<TestLine>
     */
    protected abstract function getLinesToWriteInXlsxForItemOperation(): iterable;

    /**
     * @return iterable<TestLine>
     */
    protected abstract function getLinesToWriteInXlsxForSubResourceOperation(): iterable;

    /**
     * @return iterable<TestLine>
     */
    protected abstract function getOtherLinesToWriteInXlsx(): iterable;

    public abstract function getTestType(): string;

    /**
     * @return TestType[]
     */
    #[Pure]
    public static function getAllTestType(
        Entity $entity,
    ): array
    {
        return [
            new APITest($entity),
            new BOTest($entity),
            new FOTest($entity),
            new MobileTest($entity),
        ];
    }

    protected function getOperationMethod(array $operation, string $operationName): string
    {
        return $operation["method"] ?? $operationName;
    }
}