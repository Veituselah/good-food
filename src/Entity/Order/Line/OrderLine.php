<?php

namespace App\Entity\Order\Line;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Order\Order;
use App\Entity\Product\PurchasableProduct\Food\Extra\ExtraValue;
use App\Entity\Product\PurchasableProduct\Food\Food;
use App\Entity\Product\PurchasableProduct\PurchasableProduct;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\OrderLineRepository;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\IDTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[ORM\Entity(repositoryClass: OrderLineRepository::class)]
#[ApiResource(
    collectionOperations: [
        "post" => [
            "security_post_denormalize" => "object.getOrderReference().getCustomer() === user and object.getOrderReference().getOrderedAt() === null",
            'denormalization_context'   => [
                "groups" => [
                    "order_line", "order_line:write", "order_line:post",
                ],
            ],
        ],
    ],
    itemOperations: [
        "get"    => [
//            Seulement les logisticiens du restaurant pour laquelle la commande a été passé ou les comptables ou le client de la commande
"security" => "
                (
                    is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and 
                    object.getOrderReference().getRestaurant() === user.getRestaurant()
                ) or 
                is_granted('" . FranchisorEmployee::ROLE_ACCOUNTANT . "') or 
                object.getOrderReference().getCustomer() === user
            ",
        ],
        "patch"  => [
            "security" => "object.getOrderReference().getCustomer() === user and object.getOrderReference().getOrderedAt() === null",
        ],
        "delete" => [
            "security" => "object.getOrderReference().getCustomer() === user and object.getOrderReference().getOrderedAt() === null",
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "order_line", "order_line:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "order_line", "order_line:read",
        ],
    ],
    compositeIdentifier: false
)]
class OrderLine implements TestableEntityInterface
{

    use IDTrait;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups([
        'order_line',
        'order:read',
    ])]
    private ?string $commentary;

    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[Column(type: "float", nullable: true)]
    #[GreaterThanOrEqual(
        value: 0,
        message: "La profondeur doit être supérieure ou égale à {{ compared_value }}"
    )]
    #[Groups([
        'order_line:read',
        'order:read',
    ])]
    private ?float $depth;

    #[Column(type: "text", nullable: true)]
    #[Groups([
        'order_line:read',
        'order:read',
    ])]
    private ?string $description;

    #[Column(type: "string", length: 13, nullable: true)]
    #[Length(
        min: 13,
        max: 13,
        minMessage: "Le code ean13 est composé de {{limit}} caractères.",
        maxMessage: "Le code ean13 est composé de {{limit}} caractères."
    )]
    #[Groups([
        'order_line:read',
        'order:read',
    ])]
    private ?string $ean13;

    /**
     * @var ExtraValue[]|Collection
     */
    #[ORM\ManyToMany(targetEntity: ExtraValue::class)]
    #[Groups([
        'order_line',
        'order:read',
    ])]
    private array|Collection $foodExtraChoices;

    #[Column(type: "float", nullable: true)]
    #[GreaterThanOrEqual(
        value: 0,
        message: "La profondeur doit être supérieure ou égale à {{ compared_value }}"
    )]
    #[Groups([
        'order_line:read',
        'order:read',
    ])]
    private ?float $height;

    /**
     * @var Food[]|Collection
     */
    #[ORM\ManyToMany(targetEntity: Food::class)]
    #[ORM\InverseJoinColumn(referencedColumnName: 'reference')]
    #[Groups([
        'order_line',
        'order:read',
    ])]
    private array|Collection $menuFoodChoices;

    #[Column(type: "string", length: 50)]
    #[NotBlank(message: "Le nom doit être renseigné.")]
    #[Length(
        min: 3,
        max: 50,
        minMessage: "Le nom doit faire au moins {{limit}} caractères.",
        maxMessage: "Le nom doit faire moins de {{limit}} caractères."
    )]
    #[Groups([
        'order_line:read',
        'order:read',
    ])]
    private string $name;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'orderLines')]
    #[ORM\JoinColumn(referencedColumnName: 'reference', nullable: false)]
    #[NotNull(message: "La commande doit être définie.")]
    #[Groups([
        'order_line:post',
        'order_line:read',
    ])]
    #[ApiProperty(readableLink: false, writableLink: false)]
    private Order $orderReference;

    #[ORM\ManyToOne(targetEntity: PurchasableProduct::class)]
    #[ORM\JoinColumn(referencedColumnName: 'reference', nullable: false)]
    #[NotNull(message: "Le produit doit être défini.")]
    #[Groups([
        'order_line:post',
        'order_line:read',
        'order:read',
    ])]
    private ?PurchasableProduct $product = null;

    #[ORM\Column(type: 'integer')]
    #[NotNull(message: "La quantité doit être définie.")]
    #[GreaterThan(value: 0, message: 'La quantité doit être supérieure à {{compared_value}}')]
    #[Groups([
        'order_line',
        'order:read',
    ])]
    private int $qty;

    #[ORM\Column(type: 'float')]
    #[Groups([
        'order_line:read',
        'order:read',
    ])]
    private float $taxRate = 0;

    #[ORM\Column(type: 'float')]
    #[NotNull(message: "Le prix HT doit être défini.")]
    #[GreaterThan(value: 0, message: 'Le prix HT doit être supérieur à {{compared_value}}')]
    #[Groups([
        'order_line:read',
        'order:read',
    ])]
    private float $unitPriceTaxExcluded;

    #[Column(type: "string", length: 12, nullable: true)]
    #[Length(
        min: 14,
        max: 14,
        minMessage: "Le code upc est composé de {{limit}} caractères.",
        maxMessage: "Le code upc est composé de {{limit}} caractères."
    )]
    #[Groups([
        'order_line:read',
        'order:read',
    ])]
    private ?string $upc;

    #[Column(type: "float", nullable: true)]
    #[GreaterThanOrEqual(
        value: 0,
        message: "Le poids doit être supérieure ou égale à {{ compared_value }}"
    )]
    #[Groups([
        'order_line:read',
        'order:read',
    ])]
    private ?float $weight;

    #[Column(type: "float", nullable: true)]
    #[GreaterThanOrEqual(
        value: 0,
        message: "La largeur doit être supérieure ou égale à {{ compared_value }}"
    )]
    #[Groups([
        'order_line:read',
        'order:read',
    ])]
    private ?float $width;

    #[Pure] public function __construct()
    {
        $this->menuFoodChoices = new ArrayCollection();
        $this->foodExtraChoices = new ArrayCollection();
    }

    public function addFoodExtraChoice(ExtraValue $foodExtraChoice): self
    {
        if (!$this->foodExtraChoices->contains($foodExtraChoice)) {
            $this->foodExtraChoices[] = $foodExtraChoice;
        }

        return $this;
    }

    public function addMenuFoodChoice(Food $menuFoodChoice): self
    {
        if (!$this->menuFoodChoices->contains($menuFoodChoice)) {
            $this->menuFoodChoices[] = $menuFoodChoice;
        }

        return $this;
    }

    public function getCommentary(): ?string
    {
        return $this->commentary;
    }

    public function setCommentary(?string $commentary): self
    {
        $this->commentary = $commentary;

        return $this;
    }

    public function getDepth(): ?float
    {
        return $this->depth;
    }

    public function setDepth(?float $depth): self
    {
        $this->depth = $depth;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description ?? null;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEan13(): ?string
    {
        return $this->ean13;
    }

    public function setEan13(?string $ean13): self
    {
        $this->ean13 = $ean13;

        return $this;
    }

    /**
     * @return Collection|ExtraValue[]
     */
    public function getFoodExtraChoices(): array|Collection
    {
        return $this->foodExtraChoices;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(?float $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return Collection|Food[]
     */
    public function getMenuFoodChoices(): array|Collection
    {
        return $this->menuFoodChoices;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOrderReference(): ?Order
    {
        return $this->orderReference;
    }

    public function setOrderReference(?Order $orderReference): self
    {
        $this->orderReference = $orderReference;

        $this->calculateTaxRate();

        return $this;
    }

    public function getProduct(): ?PurchasableProduct
    {
        return $this->product;
    }

    public function setProduct(PurchasableProduct $product): self
    {
        $this->product = $product;

        $this->ean13 = $product->getEan13();
        $this->depth = $product->getDepth();
        $this->description = $product->getDescription();
        $this->height = $product->getHeight();
        $this->name = $product->getName();
        $this->unitPriceTaxExcluded = $product->getPriceTaxExcluded();
        $this->upc = $product->getUpc();
        $this->weight = $product->getWeight();
        $this->width = $product->getWidth();
        $this->calculateTaxRate();

        return $this;
    }

    public function calculateTaxRate()
    {
        $this->taxRate = $this?->product?->getTaxRateForRestaurant($this?->orderReference?->getRestaurant()) ?? 0;
    }

    public function getQty(): ?int
    {
        return $this->qty;
    }

    public function setQty(int $qty): self
    {
        $this->qty = $qty;

        return $this;
    }

    public function getTaxRate(): ?float
    {
        return $this->taxRate;
    }

    public function setTaxRate(float $taxRate): self
    {
        $this->taxRate = $taxRate;

        return $this;
    }

    public function getUnitPriceTaxExcluded(): ?float
    {
        return $this->unitPriceTaxExcluded;
    }

    public function setUnitPriceTaxExcluded(float $unitPriceTaxExcluded): self
    {
        $this->unitPriceTaxExcluded = $unitPriceTaxExcluded;

        return $this;
    }

    public function getUpc(): ?string
    {
        return $this->upc;
    }

    public function setUpc(?string $upc): self
    {
        $this->upc = $upc;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(?float $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function removeFoodExtraChoice(ExtraValue $foodExtraChoice): self
    {
        $this->foodExtraChoices->removeElement($foodExtraChoice);

        return $this;
    }

    public function removeMenuFoodChoice(Food $menuFoodChoice): self
    {
        $this->menuFoodChoices->removeElement($menuFoodChoice);

        return $this;
    }

    public function getEntityName(): string
    {
        return "Ligne de commande";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "order_lines";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    #[Pure]
    #[Groups([
        'order:read',
        'order_line:read'
    ])]
    public function getTotal(): float
    {
        return $this->getQty() * $this->getPriceTaxIncluded();
    }

    #[Pure]
    #[Groups([
        'order:read',
        'order_line:read'
    ])]
    public function getTotalTaxExcluded(): float
    {
        return $this->getQty() * $this->getUnitPriceTaxExcluded();
    }

    #[Pure]
    #[Groups([
        'order:read',
        'order_line:read'
    ])]
    public function getPriceTaxIncluded(): float
    {
        return $this->getUnitPriceTaxExcluded() * (1 + ($this->taxRate ?? 0));
    }

    #[Pure]
    #[Groups([
        'order:read',
        'order_line:read'
    ])]
    public function getTaxAmount(): float
    {
        return $this->getQty() * $this->getUnitPriceTaxExcluded() * ($this->taxRate ?? 0);
    }

}
