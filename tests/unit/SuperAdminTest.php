<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\User\SuperAdmin\SuperAdmin;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\SuperAdminExampleTrait;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class SuperAdminTest extends CrudApiTestCase
{

    use SuperAdminExampleTrait;

    // Fixtures
    const SUPER_ADMIN_FIXTURES_FILE = 'super_admin.yml';

    // Routes
    const SUPER_ADMIN_ROUTE = self::API_ROUTE . '/super_admins';
    const SUPER_ADMIN_ITEM_ROUTE = self::SUPER_ADMIN_ROUTE . '/{uuid}';

    // JSON LD Data
    const SUPER_ADMIN_TYPE = 'SuperAdmin';
    const SUPER_ADMIN_CONTEXT = self::CONTEXT . '/' . self::SUPER_ADMIN_TYPE;

    // Data
    const ALL_FIELDS_VIOLATION = ['email', 'lastname', 'firstname', 'password'];

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadSuperAdmin();
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection(): void
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::SUPER_ADMIN_ROUTE,
                    context: self::SUPER_ADMIN_CONTEXT,
                    classToCheckValidity: SuperAdmin::class,
                    totalItems: count(self::getSuperAdminFixtures())
                );

            },
            method: self::GET_METHOD,
            url: self::SUPER_ADMIN_ROUTE,
        );
    }

    /**
     * @return SuperAdmin[]
     */
    #[Pure] public static function getSuperAdminFixtures(): array
    {
        return self::getDataFixturesOfClass(SuperAdmin::class);
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetItem(): void
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                $this->superAdminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::SUPER_ADMIN_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::SUPER_ADMIN_TYPE,
                        "lastname"                  => $this->superAdminExample->getLastname(),
                        "firstname"                 => $this->superAdminExample->getFirstname(),
                        "email"                     => $this->superAdminExample->getEmail(),
                        "roles"                     => $this->superAdminExample->getRoles(),
                        'active'                    => $this->superAdminExample->isActive(),
                        'deleted'                   => $this->superAdminExample->isDeleted(),
                    ]
                );

                $superAdmin = $response->toArray();

                $this->assertIdentifierOfResponseData(self::SUPER_ADMIN_ROUTE, Tools::UUID_REGEX, $superAdmin);
                $this->assertMatchesResourceItemJsonSchema(SuperAdmin::class);

            },
            method: self::GET_METHOD,
            url: $this->superAdminExampleItemIRI,
        );
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testPatchItem(): void
    {
        $json = [
            "password"  => self::PASSWORDS[$this->superAdminExampleIndex],
            "lastname"  => "1" . $this->superAdminExample->getLastname(),
            "firstname" => "1" . $this->superAdminExample->getFirstname(),
            "email"     => "1" . $this->superAdminExample->getEmail(),
        ];

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                $this->superAdminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $verifJSON = $json;
                unset($verifJSON['password']);

                $this->assertSuccess();
                $this->assertJsonContains($verifJSON);
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::SUPER_ADMIN_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::SUPER_ADMIN_TYPE,
                        'active'                    => $this->superAdminExample->isActive(),
                        'deleted'                   => $this->superAdminExample->isDeleted(),
                    ]
                );

                $superAdmin = $response->toArray();

                $this->assertIdentifierOfResponseData(self::SUPER_ADMIN_ROUTE, Tools::UUID_REGEX, $superAdmin);
                $this->assertMatchesResourceItemJsonSchema(SuperAdmin::class);

            },
            method: self::PATCH_METHOD,
            url: $this->superAdminExampleItemIRI,
            json: $json
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testPatchInvalidItem()
    {
        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->superAdminExampleItemIRI,
            json: [],
            token: $this->superAdminExampleToken,
        );

        $this->assertSuccess();
        $this->assertJsonContains(
            [
                self::CONTEXT_INDEX_JSON_LD => self::SUPER_ADMIN_CONTEXT,
                self::TYPE_INDEX_JSON_LD    => self::SUPER_ADMIN_TYPE,
                "lastname"                  => $this->superAdminExample->getLastname(),
                "firstname"                 => $this->superAdminExample->getFirstname(),
                "email"                     => $this->superAdminExample->getEmail(),
                'active'                    => $this->superAdminExample->isActive(),
                'deleted'                   => $this->superAdminExample->isDeleted(),
            ]
        );

        // Max carac lastname, password, firstname, birthdate
        // wrong format email
        $requestNotRespectMinValidation = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->superAdminExampleItemIRI,
            json: [
                "password"  => 'aa&&11',
                "lastname"  => 'a',
                "firstname" => 'a',
                "email"     => 'mail non valide',
            ],
            token: $this->superAdminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac lastname, firstname
        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->superAdminExampleItemIRI,
            json: [
                "lastname"  => self::STRING_LENGTH_256,
                "firstname" => self::STRING_LENGTH_256,
            ],
            token: $this->superAdminExampleToken,
        );

        $this->assertViolations(['lastname', 'firstname'], $requestNotRespectMaxValidation);

    }

    public function testDeleteItem()
    {
        $this->inexistantEndpointTest();
    }

    public function testPostInvalidItem()
    {
        $this->inexistantEndpointTest();
    }

    public function testPostItem()
    {
        $this->inexistantEndpointTest();
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testEmailAlreadyUsedOnUpdate()
    {
        $otherAdminIndex = self::ARTHUR_ADMIN;
        $adminData = self::getUserFixturesClean()[$otherAdminIndex];

        $json = [
            "email" => $adminData->getEmail(),
        ];

        // Tentative de création du compte avec le même mail
        $responseDuplicateAccountEmail = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->superAdminExampleItemIRI,
            json: $json,
            token: $this->superAdminExampleToken,
        );

        $violations = ['email'];

        $this->assertViolations($violations, $responseDuplicateAccountEmail);
    }

}
