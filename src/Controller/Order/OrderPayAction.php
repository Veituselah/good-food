<?php

namespace App\Controller\Order;

use App\Entity\Order\Order;
use App\Repository\OrderStatusRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;

class OrderPayAction extends AbstractController
{

    public function __invoke(Order $data, OrderStatusRepository $orderStatusRepository): Order
    {
        $cartStatus = $orderStatusRepository->findOneBy(['slug' => 'payee']);
        $data->setStatus($cartStatus);
        $data->setLastPaymentTryAt(new DateTime());

        return $data;
    }

}