<?php

namespace App\Entity\User\Employee;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FranchisorEmployeeRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;


#[Entity(repositoryClass: FranchisorEmployeeRepository::class)]
#[ApiResource(
    collectionOperations: [
        //	Les rh ou + peuvent récupérer la liste des clients
        "get"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_HR . '")',
        ],
        // Les rh ou + peuvent créer un compte
        "post" => [
            "security"          => 'is_granted("' . FranchisorEmployee::ROLE_HR . '")',
            "validation_groups" => [
                "Default",
                "user:post:write",
            ],
        ],
    ],
    itemOperations: [
        //	Les rh, possésseurs ou + peuvent voir les infos du compte
        "get"                                => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_HR . '") or object === user',
        ],
        //	Le possesseur du compte peut modifier les infos du compte
        "patch"                              => [
            "security" => "object === user",
        ],
        // Les rh ou + peuvent désactiver un compte
        'franchisor_employee_active_account' => [
            'method'                  => 'PATCH',
            'path'                    => '/franchisor_employees/{uuid}/active',
            "security"                => 'is_granted("' . FranchisorEmployee::ROLE_HR . '")',
            'denormalization_context' => [
                "groups" => [
                    "active:write",
                ],
            ],
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "user", "user:write",
            "employee", "employee:write",
            "franchisor_employee", "franchisor_employee:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "user", "user:read",
            "employee", "employee:read",
            "franchisor_employee", "franchisor_employee:read",
        ],
    ]

)]
class FranchisorEmployee extends Employee
{

    const DEFAULT_ROLE_FRANCHISOR = 'ROLE_FRANCHISOR';
    const ROLE_EDITOR = 'ROLE_EDITOR';
    const ROLE_ACCOUNTANT = 'ROLE_ACCOUNTANT';
    const ROLE_HR = 'ROLE_HR';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    const AVAILABLE_ROLES = [
        self::DEFAULT_ROLE_FRANCHISOR,
        self::ROLE_EDITOR,
        self::ROLE_ACCOUNTANT,
        self::ROLE_HR,
        self::ROLE_ADMIN,
    ];

    public function getAvailableRoles(): array
    {
        return array_merge(parent::getAvailableRoles(), self::AVAILABLE_ROLES);
    }

    protected function initRoles(): array
    {
        return array_merge(parent::initRoles(), [self::DEFAULT_ROLE_FRANCHISOR]);
    }

    public function getEntityName(): string
    {
        return "Employé de la franchise";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            "franchisor_employee_active_account" => "Active/désactive un compte employé"
        ];
    }

    public function getEntityRouteName(): string
    {
        return "franchisor_employees";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return true;
    }
}
