<?php

namespace App\Interfaces;

interface TestableEntityInterface
{

    public function getEntityName(): string;

    public function routesHaveBeenTested(): bool;

    public function getMappingRouteToTaskName(): array;

    public function getEntityRouteName(): string;

    public function getEntityIdentifierName(): string;

    public function getRoutesDontNeedingAuth(): array;

    public function requireBOTest(): bool;

}