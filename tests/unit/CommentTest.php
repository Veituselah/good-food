<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\Restaurant\Restaurant;
use App\Entity\User\Comment\Comment;
use App\Entity\User\Customer\Customer;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\AdminExampleTrait;
use App\Traits\Tests\CommentExampleTrait;
use App\Traits\Tests\CustomerConfirmedExampleTrait;
use App\Traits\Tests\RestaurantExampleTrait;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class CommentTest extends CrudApiTestCase
{
    //
    // use CommentExampleTrait;
    // use AdminExampleTrait;
    // use RestaurantExampleTrait;
    // use CustomerConfirmedExampleTrait;
    //
    // //Fixtures
    // const COMMENT_FIXTURES_FILES = 'comment.yml';
    //
    // //Routes
    // const COMMENT_ROUTE = self::API_ROUTE . '/comments';
    // const COMMENT_ITEM_ROUTE = self::COMMENT_ROUTE . '/{id}';
    // const COMMENT_ACTIVE_ROUTE = self::COMMENT_ROUTE . '/active';
    //
    // //Json LD Data
    // const COMMENT_TYPE = 'Comment';
    // const COMMENT_CONTEXT = self::CONTEXT . '/' . self::COMMENT_TYPE;
    //
    // //Data
    // const ALL_FIELDS_VIOLATION = ['note'];
    // const COMMENT_EXAMPLE = "commentaire_sur_restaurant_france_1";
    //
    // /**
    //  * @throws ExceptionInterface
    //  */
    // public static function getJSONFromComment(Comment $comment): array
    // {
    //     return self::parseEntityToArray($comment);
    // }
    //
    // /**
    //  * @throws TransportExceptionInterface
    //  * @throws ServerExceptionInterface
    //  * @throws RedirectionExceptionInterface
    //  * @throws DecodingExceptionInterface
    //  * @throws ClientExceptionInterface
    //  */
    // public function setUp(): void
    // {
    //     parent::setUp();
    //     $this->loadAdmin();
    //     $this->loadComment();
    // }
    //
    // /**
    //  * @throws RedirectionExceptionInterface
    //  * @throws DecodingExceptionInterface
    //  * @throws ClientExceptionInterface
    //  * @throws TransportExceptionInterface
    //  * @throws ServerExceptionInterface
    //  */
    // public function testDeleteItem()
    // {
    //     $superAdminToken = $this->getTokenFromUserIndex(self::ARTHUR_ADMIN);
    //     $nbCustomersInCollectionBefore = $this->getTotalItemsForRequest(self::COMMENT_ROUTE, $superAdminToken);
    //
    //     $this->doRequestForEachUsers(
    //         authorizedUserIndex: [
    //             self::CEDRIC_ADMIN,
    //             self::ARTHUR_ADMIN,
    //             self::CORENTIN_ADMIN,
    //             self::ROLE_ADMIN,
    //             $this->adminExampleIndex,
    //         ],
    //         functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {
    //
    //             extract($dataToSendToFunctionAssertAhtorizeUser);
    //
    //             $this->assertSuccessDelete();
    //
    //             $this->assertNull(self::getEntityManager()->getRepository(Comment::class));
    //
    //             $this->doRequest(
    //                 method: self::GET_METHOD,
    //                 url: $this->commentExampleItemIRI,
    //                 token: $superAdminToken,
    //             );
    //
    //             $this->assertNotFound();
    //
    //             $nbCustomersInCollectionAfter = $this->getTotalItemsForRequest(self::COMMENT_ROUTE, $superAdminToken);
    //
    //             $this->assertEquals($nbCustomersInCollectionBefore - 1, $nbCustomersInCollectionAfter);
    //
    //             $this->initClient();
    //
    //         },
    //         method: self::DELETE_METHOD,
    //         url: $this->commentExampleItemIRI,
    //         dataToSendToFunctionAssertAhtorizeUser: compact('nbCustomersInCollectionBefore', 'superAdminToken')
    //     );
    // }
    //
    // /**
    //  * @throws TransportExceptionInterface
    //  * @throws ServerExceptionInterface
    //  * @throws RedirectionExceptionInterface
    //  * @throws DecodingExceptionInterface
    //  * @throws ClientExceptionInterface
    //  */
    // public function testGetCollection()
    // {
    //     $this->doRequestForEachUsers(
    //         authorizedUserIndex: [
    //             self::ALL_USERS,
    //         ],
    //         functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {
    //
    //             extract($dataToSendToFunctionAssertAhtorizeUser);
    //
    //             $this->assertCollection(
    //                 response: $response,
    //                 endpointCollectionRoute: self::COMMENT_ROUTE,
    //                 context: self::COMMENT_ROUTE,
    //                 classToCheckValidity: Comment::class,
    //                 totalItems: count(self::getCommentFixtures())
    //             );
    //         },
    //         method: self::GET_METHOD,
    //         url: self::COMMENT_ROUTE,
    //     );
    // }
    //
    // /**
    //  * @throws TransportExceptionInterface
    //  * @throws ServerExceptionInterface
    //  * @throws RedirectionExceptionInterface
    //  * @throws DecodingExceptionInterface
    //  * @throws ClientExceptionInterface
    //  */
    // public function testGetItem()
    // {
    //     $this->doRequestForEachUsers(
    //         authorizedUserIndex: [
    //             self::ALL_USERS,
    //         ],
    //         functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {
    //
    //             extract($dataToSendToFunctionAssertAhtorizeUser);
    //
    //             $this->assertSuccess();
    //
    //             //        Vérification du contenu du retour
    //             $this->assertJsonContains(
    //                 [
    //                     self::CONTEXT_INDEX_JSON_LD => self::COMMENT_CONTEXT,
    //                     self::TYPE_INDEX_JSON_LD    => self::COMMENT_TYPE,
    //                     'active'                    => $this->commentExample->isActive(),
    //                     'deleted'                   => $this->commentExample->isDeleted(),
    //                 ]
    //             );
    //
    //             $validJson = self::getJSONFromComment($this->commentExample);
    //             $this->assertJsonContains($validJson);
    //
    //             $comment = $response->toArray();
    //
    //             $this->assertIdentifierOfResponseData(self::COMMENT_ROUTE, Tools::UUID_REGEX, $comment);
    //             $this->assertMatchesResourceItemJsonSchema(Comment::class);
    //
    //         },
    //         method: self::GET_METHOD,
    //         url: $this->commentExampleItemIRI,
    //     );
    // }
    //
    // public function testPatchInvalidItem()
    // {
    //     // TODO: Implement testPatchInvalidItem() method.
    // }
    //
    // /**
    //  * @throws ExceptionInterface
    //  */
    // public function testPatchItem()
    // {
    //     $comment = new Comment();
    //     $comment->setMessage($this->commentExample->getMessage());
    //     $comment->setNote($this->commentExample->getNote());
    //     $comment->setRestaurant($this->commentExample->getRestaurant());
    //     $comment->setCustomer($this->commentExample->getCustomer());
    //
    //     $json = self::getJSONFromComment($comment);
    //
    //     $this->doRequestForEachUsers(
    //         authorizedUserIndex: [
    //             self::CEDRIC_ADMIN,
    //             self::ARTHUR_ADMIN,
    //             self::CORENTIN_ADMIN,
    //             self::ROLE_ADMIN,
    //
    //             $this->adminExampleIndex,
    //         ],
    //         functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {
    //
    //             extract($dataToSendToFunctionAssertAhtorizeUser);
    //
    //             $validJson = $json;
    //
    //             $this->assertSuccess();
    //             //        Vérification du contenu du retour
    //             $this->assertJsonContains(
    //                 [
    //                     self::CONTEXT_INDEX_JSON_LD => self::COMMENT_CONTEXT,
    //                     self::TYPE_INDEX_JSON_LD    => self::COMMENT_TYPE,
    //                     'deleted'                   => false,
    //                 ]
    //             );
    //             $this->assertJsonContains($validJson);
    //
    //             $comment = $response->toArray();
    //
    //             $this->assertIdentifierOfResponseData(self::COMMENT_ROUTE, Tools::UUID_REGEX, $comment);
    //             $this->assertMatchesResourceItemJsonSchema(Comment::class);
    //
    //             $this->initClient();
    //         },
    //         method: self::PATCH_METHOD,
    //         url: $this->commentExampleItemIRI,
    //         json: $json,
    //     );
    // }
    //
    // public function testPostInvalidItem()
    // {
    //     // TODO: Implement testPostInvalidItem() method.
    // }
    //
    // /**
    //  * @throws TransportExceptionInterface
    //  * @throws ServerExceptionInterface
    //  * @throws RedirectionExceptionInterface
    //  * @throws DecodingExceptionInterface
    //  * @throws ClientExceptionInterface
    //  * @throws ExceptionInterface
    //  */
    // public function testPostItem()
    // {
    //     $this->doRequestForEachUsers(
    //         authorizedUserIndex: [
    //             self::CEDRIC_ADMIN,
    //             self::ARTHUR_ADMIN,
    //             self::CORENTIN_ADMIN,
    //             self::ROLE_ADMIN,
    //         ],
    //         functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {
    //
    //             extract($dataToSendToFunctionAssertAhtorizeUser);
    //
    //             $validJson = $json;
    //
    //             $commentCreated = $response->toArray();
    //
    //             $this->assertSuccess();
    //             $this->assertJsonContains(
    //                 [
    //                     self::CONTEXT_INDEX_JSON_LD => self::COMMENT_CONTEXT,
    //                     self::TYPE_INDEX_JSON_LD    => self::COMMENT_TYPE,
    //                     'deleted'                   => false,
    //                     'active'                    => true,
    //                 ]
    //             );
    //
    //             $this->assertJsonContains($validJson);
    //             $this->assertIdentifierOfResponseData(self::COMMENT_ROUTE, Tools::ISO_CODE_REGEX, $commentCreated);
    //             $this->assertMatchesResourceItemJsonSchema(Comment::class);
    //
    //             $this->initClient();
    //         },
    //         method: self::POST_METHOD,
    //         url: self::COMMENT_ROUTE,
    //         json: self::getValidTestCommentInJSON(
    //             $this->restaurantExample, $this->customerConfirmedExample
    //         ),
    //     );
    //
    // }
    //
    // /**
    //  * @throws ExceptionInterface
    //  */
    // public static function getValidTestCommentInJSON(Restaurant $restaurant, Customer $customer) : array
    // {
    //     $comment = self::getValidTestComment($restaurant, $customer);
    //
    //     return self::getJSONFromComment($comment);
    // }
    //
    // public static function getValidTestComment(Restaurant $restaurant, Customer $customer) : Comment
    // {
    //     $comment = new Comment();
    //     $comment->setMessage("Coucou, un gentil message");
    //     $comment->setNote(4);
    //     $comment->setRestaurant($restaurant);
    //     $comment->setCustomer($customer);
    //
    //     return $comment;
    // }
    //
    // /**
    //  * @return Comment[]
    //  */
    // #[Pure] public static function getCommentFixtures(): array
    // {
    //     return self::getDataFixturesOfClass(Comment::class);
    // }
    //
    // public function testGetCollectionWithFilter()
    // {
    //     // TODO: Implement testGetCollectionWithFilter() method.
    // }

    public function testDeleteItem()
    {
        // TODO: Implement testDeleteItem() method.
    }

    public function testGetCollection()
    {
        // TODO: Implement testGetCollection() method.
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

    public function testGetItem()
    {
        // TODO: Implement testGetItem() method.
    }

    public function testPatchInvalidItem()
    {
        // TODO: Implement testPatchInvalidItem() method.
    }

    public function testPatchItem()
    {
        // TODO: Implement testPatchItem() method.
    }

    public function testPostInvalidItem()
    {
        // TODO: Implement testPostInvalidItem() method.
    }

    public function testPostItem()
    {
        // TODO: Implement testPostItem() method.
    }

}
