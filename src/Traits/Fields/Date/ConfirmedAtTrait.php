<?php


namespace App\Traits\Fields\Date;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait ConfirmedAtTrait
{

    #[ORM\Column(type: "datetime_immutable", nullable: true)]
    private ?DateTimeImmutable $confirmedAt;

    public function confirmAccount(): self
    {
        $this->confirmedAt = $this->confirmedAt ?? new DateTimeImmutable();

        return $this;
    }

    #[Groups([
        "date:read",
        "customer:read",
    ])]
    public function hasConfirmedAccount(): bool
    {
        return !empty($this->confirmedAt);
    }

    /**
     * @param DateTimeImmutable|null $confirmedAt
     */
    public function setConfirmedAt(?DateTimeImmutable $confirmedAt): void
    {
        $this->confirmedAt = $confirmedAt;
    }


}