<?php


namespace App\Entity\Country;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use App\Controller\Country\ListCountriesInRestaurantsAction;
use App\Entity\Country\Tax\Tax;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\CountryRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\IsoCodeTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[Entity(repositoryClass: CountryRepository::class)]
#[UniqueEntity(fields: ["isoCode"], message: "Le code iso de la devise doit être unique.")]
#[SoftDeleteable(fieldName: "deletedAt")]
#[HasLifecycleCallbacks()]
#[ApiResource(
    collectionOperations: [
        //	Tout le monde peut récupérer la liste des pays
        "get"           => [],
        // Seuls les admins ou + peuvent créer un pays
        "post"          => [
            "security"                => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
            'denormalization_context' => [
                "groups" => [
                    "iso_code", "iso_code:post",
                    "country", "country:write",
                    "country:currency", "country:currency:write",
                ],
            ],
        ],
        "in_restaurant" => [
            "method"             => "get",
            "path"               => "/countries/in_restaurants",
            "controller"         => ListCountriesInRestaurantsAction::class,
            "pagination_enabled" => false,
        ],
    ],
    itemOperations: [
        //	Tout le monde peut récupérer un pays
        "get"    => [],
        // Seuls les admins ou + peuvent modifier un pays
        "patch"  => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
        // Seuls les admins ou + peuvent supprimer un pays
        "delete" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    attributes: ['pagination_client_enabled' => true],
    denormalizationContext: [
        "groups" => [
            "country", "country:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "country", "country:read",
        ],
    ],
)]
#[ApiFilter(
    OrderFilter::class,
    properties: ['name']
)]
class Country implements TestableEntityInterface
{

    use IsoCodeTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[ManyToOne(targetEntity: Currency::class)]
    #[JoinColumn(referencedColumnName: "iso_code", nullable: false)]
    #[NotNull(message: "La devise monétaire du pays doit être renseigné")]
    #[Groups([
        'country',
        "franchise_employee:read",
        "customer:read",
        "restaurant:read",
        'purchasable_product:read',
    ])]
    private Currency $defaultCurrency;

    #[Column(type: "string", length: 50)]
    #[NotBlank(message: "Le nom doit être renseigné.")]
    #[Length(
        min: 3,
        max: 50,
        minMessage: "Le nom doit faire au moins {{limit}} caractères.",
        maxMessage: "Le nom doit faire moins de {{limit}} caractères."
    )]
    #[Groups([
        "country",
        "tax:read",
        'address:read',
        'restaurant:read',
        "franchise_employee:read",
        "customer:read",
        'purchasable_product:read',
    ])]
    private string $name;

    #[Column(type: "string", length: 10, nullable: true)]
    #[Length(
        min: 1,
        max: 20,
        minMessage: "Le préfix téléphonique doit faire au moins {{limit}} caractères.",
        maxMessage: "Le préfix téléphonique doit faire moins de {{limit}} caractères."
    )]
    #[Groups([
        "country",
        "tax:read",
        'address:read',
        'restaurant:read',
        "franchise_employee:read",
        "customer:read",
        'purchasable_product:read',
    ])]
    private ?string $phonePrefix;

    /**
     * @var Tax[]|Collection
     */
    #[OneToMany(mappedBy: "country", targetEntity: Tax::class, orphanRemoval: true)]
    private array|Collection $taxes;

    #[Pure] public function __construct()
    {
        $this->taxes = new ArrayCollection();
    }

    public function addTax(Tax $tax): self
    {
        if (!$this->taxes->contains($tax)) {
            $this->taxes[] = $tax;
            $tax->setCountry($this);
        }

        return $this;
    }

    function autoActiveOnCreation(): bool
    {
        return true;
    }

    /**
     * @return Currency|null
     */
    public function getDefaultCurrency(): ?Currency
    {
        return $this->defaultCurrency ?? null;
    }

    /**
     * @param Currency $defaultCurrency
     */
    public function setDefaultCurrency(Currency $defaultCurrency): void
    {
        $this->defaultCurrency = $defaultCurrency;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhonePrefix(): ?string
    {
        return $this->phonePrefix;
    }

    public function setPhonePrefix(string $phonePrefix): self
    {
        $this->phonePrefix = $phonePrefix;

        return $this;
    }

    /**
     * @return Tax[]|Collection
     */
    public function getTaxes(): array|Collection
    {
        return $this->taxes;
    }

    function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function removeTax(Tax $tax): self
    {
        if ($this->taxes->removeElement($tax)) {
            // set the owning side to null (unless already changed)
            if ($tax->getCountry() === $this) {
                $tax->setCountry(null);
            }
        }

        return $this;
    }

    public function getEntityName(): string
    {
        return "Pays";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    #[ArrayShape(
        [
            "in_restaurant" => "string",
        ]
    )]
    public function getMappingRouteToTaskName(): array
    {
        return [
            "in_restaurant" => "Récupère la liste des pays utilisée dans les restaurants",
        ];
    }

    public function getEntityRouteName(): string
    {
        return "countries";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get",
            "in_restaurant"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
