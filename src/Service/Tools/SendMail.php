<?php


namespace App\Service\Tools;


use App\Entity\Mail\Mail;
use App\Exceptions\EnvVariableNotFoundExcpetion;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Email;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class SendMail
{

    private string $default_mailer_dsn;

    private string $default_sender;

    /**
     * @throws Exception
     */
    public function __construct(
        private Environment            $twig,
        private EntityManagerInterface $entityManager,
        private Tools                  $tools,
        private KernelInterface        $kernel
    )
    {
        if (empty($_SERVER['MAILER_DSN'])) {
            throw new EnvVariableNotFoundExcpetion('MAILER_DSN');
        }

        $this->default_mailer_dsn = $_SERVER['MAILER_DSN'];

        if (empty($_SERVER['DEFAULT_SENDER_EMAIL'])) {
            throw new EnvVariableNotFoundExcpetion('DEFAULT_SENDER_EMAIL');
        }

        $this->default_sender = $_SERVER['DEFAULT_SENDER_EMAIL'];

    }

    /**
     * @param string  $template
     * @param array   $templateData
     * @param string  $subject
     * @param ?string $from
     * @param array   $to
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function sendMail(string $template, array $templateData, string $subject, ?string $from, array $to)
    {
        $from = $from ?? $this->default_sender;

        $html = $this->twig->render(
            $template,
            $templateData
        );

        $mail = new Mail();
        $mail->setSender($from);
        $mail->setAddresses($to);
        $mail->setContent($html);
        $mail->setSubject($subject);
        $mail->newSendingAttempts();

        if (
            $this->kernel->getEnvironment() === "prod" ||
            $this->kernel->getEnvironment() === "dev"
            // (
            //     !empty($_SERVER['EMAIL_TEST']) &&
            //     in_array($_SERVER['EMAIL_TEST'], $to)
            // )
        ) {

            $transport = Transport::fromDsn($this->default_mailer_dsn);
            $mailer = new Mailer($transport);

            $email = (new Email())
                ->from($from)
                ->subject($subject)
                ->html($html)
                ->text($this->tools->parseHTMLToText($html));

            foreach ($to as $address) {
                $email->to($address);
            }

            try {
                $mailer->send($email);
            } catch (Exception $e) {
                $mail->newFail((array)$e);
            }

        }

        $this->entityManager->persist($mail);
        $this->entityManager->flush();
    }

}
