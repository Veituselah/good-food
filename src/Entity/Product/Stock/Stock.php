<?php

namespace App\Entity\Product\Stock;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Product\Product;
use App\Entity\Restaurant\Restaurant;
use App\Interfaces\TestableEntityInterface;
use App\Repository\StockRepository;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Voter\StockVoter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotNull;


#[ORM\Entity(repositoryClass: StockRepository::class)]
#[ApiResource(
    collectionOperations: [
        "post" => [
            // Logisticien même restaurant ou +
            "security"                => 'is_granted("' . StockVoter::IS_AUTHORIZED_TO_MANAGE_STOCK . '", object)',
            "denormalization_context" => [
                'groups' => [
                    "stock", "stock:write", "stock:post",
                    // "stock", "stock:write", "stock:post",
                    // "date", "date:write",
                ],
            ],
        ],
    ],
    itemOperations: [
        "get" => [],
        "patch" => [
            // Logisticien même restaurant ou +
            "security" => 'is_granted("' . StockVoter::IS_AUTHORIZED_TO_MANAGE_STOCK . '", object)',
        ],
    ],
    attributes: [
        'composite_identifier' => false,
    ],
    denormalizationContext: [
        'groups' => [
            "stock", "stock:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "stock", "stock:read",
        ],
    ]
)]
class Stock implements TestableEntityInterface
{

    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[GreaterThanOrEqual(value: 0, message: "La quantité à partir de laquelle une alerte va être envoyée doit être supérieure à {{ compared_value }}")]
    #[Groups('stock')]
    private int $lowStockAlert;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Product::class)]
    #[ORM\JoinColumn(referencedColumnName: "reference", nullable: false)]
    #[NotNull(message: "Le produit cible du stock doit être renseigné")]
    #[Groups(['stock:post', 'stock:read'])]
    private Product $product;

    #[ORM\Column(type: 'integer')]
    #[NotNull(message: "La quantité doit être définie")]
    #[GreaterThanOrEqual(value: 0, message: "La quantité doit être supérieure à {{ compared_value }}")]
    #[NotNull(message: "Le restaurant pour lequel le stock est créé doit être renseigné")]
    #[Groups('stock')]
    private int $qty;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Restaurant::class)]
    #[ORM\JoinColumn(referencedColumnName: "slug", nullable: false)]
    #[Groups(['stock:post', 'stock:read'])]
    private Restaurant $restaurant;

    public function getLowStockAlert(): ?int
    {
        return $this->lowStockAlert;
    }

    public function setLowStockAlert(?int $lowStockAlert): self
    {
        $this->lowStockAlert = $lowStockAlert;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQty(): ?int
    {
        return $this->qty;
    }

    public function setQty(int $qty): self
    {
        $this->qty = $qty;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getEntityName(): string
    {
        return "Stock";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "stocks";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    public function getEntityIdentifierName(): string
    {
        return 'product';
    }

}
