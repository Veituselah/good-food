<?php

namespace App\Entity\Product\PurchasableProduct\Food;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\Product\PurchasableProduct\Food\Extra\Extra;
use App\Entity\Product\PurchasableProduct\PurchasableProduct;
use App\Filter\Product\SearchByNameOrReference;
use App\Repository\FoodRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\Table;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;


#[Entity(repositoryClass: FoodRepository::class)]
#[DiscriminatorColumn(name: "type", type: "string")]
#[DiscriminatorMap(
    [
        "entree"  => "Entree",
        "drink"   => "Drink",
        "dish"    => "Dish",
        "dessert" => "Dessert",
    ]
)]
#[ApiResource(
    collectionOperations: [
        'get' => [],
    ],
    itemOperations: [
        'get' => [],
    ],

    denormalizationContext: [
        'groups' => [],
    ],
    normalizationContext: [
        'groups' => [
            "product", "product:read",
            "purchasable_product", "purchasable_product:read",
            "food", "food:read",
        ],
    ]
)]
#[ApiFilter(SearchByNameOrReference::class, properties: ['search_by_name_or_reference'])]
abstract class Food extends PurchasableProduct
{

    /**
     * @var Extra[]|Collection
     */
    #[OneToMany(mappedBy: 'product', targetEntity: Extra::class, orphanRemoval: true)]
    #[Groups(['food'])]
    #[ApiSubresource]
    private array|Collection $extras;

    #[Pure] public function __construct()
    {
        parent::__construct();
        $this->extras = new ArrayCollection();
    }

    public function addExtra(Extra $extra): self
    {
        if (!$this->extras->contains($extra)) {
            $this->extras[] = $extra;
            $extra->setProduct($this);
        }

        return $this;
    }

    /**
     * @return Collection|Extra[]
     */
    public function getExtras(): Collection|array
    {
        return $this->extras;
    }

    public function removeExtra(Extra $extra): self
    {
        if ($this->extras->removeElement($extra)) {
            // set the owning side to null (unless already changed)
            if ($extra->getProduct() === $this) {
                $extra->setProduct(null);
            }
        }

        return $this;
    }

    public function getEntityName(): string
    {
        return "Food";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "foods";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
