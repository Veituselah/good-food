<?php

namespace App\Traits\Tests;

use Symfony\Contracts\HttpClient\ResponseInterface;

trait UserExampleTrait
{
    abstract function getTokenForUser(string $username, string $password, bool $returnResponse = false): ResponseInterface|String|null;
}