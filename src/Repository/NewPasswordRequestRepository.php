<?php

namespace App\Repository;

use App\Entity\User\UserRequest\NewPasswordRequest;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NewPasswordRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewPasswordRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewPasswordRequest[]    findAll()
 * @method NewPasswordRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewPasswordRequestRepository extends UserRequestRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NewPasswordRequest::class);
    }

    public function getDelayBeforeTwoRequest(): int
    {
        return NewPasswordRequest::DELAY_IN_MINUTE_BETWEEN_TWO_REQUEST;
    }

    public function getUserFieldName(): string
    {
        return 'userTarget';
    }
}
