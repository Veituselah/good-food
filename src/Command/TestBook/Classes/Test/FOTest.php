<?php

namespace App\Command\TestBook\Classes\Test;


use ArrayIterator;
use JetBrains\PhpStorm\Pure;

class FOTest extends TestType
{

    const TEST_TYPE = "FO";

    protected function getLinesToWriteInXlsxForCollectionOperation(): iterable
    {
        return new ArrayIterator();
    }

    protected function getLinesToWriteInXlsxForItemOperation(): iterable
    {
        return new ArrayIterator();
    }

    protected function getLinesToWriteInXlsxForSubResourceOperation(): iterable
    {
        return new ArrayIterator();
    }

    protected function getOtherLinesToWriteInXlsx(): iterable
    {
        yield APITest::getTestConnectionToApiLine($this->idTest, self::TEST_TYPE);
        $this->idTest++;
    }

    public function getTestType(): string
    {
        return self::TEST_TYPE;
    }

}