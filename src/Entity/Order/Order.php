<?php

namespace App\Entity\Order;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Order\OrderAddVoucherAction;
use App\Controller\Order\OrderAvailableInRestaurantAction;
use App\Controller\Order\OrderCancelAction;
use App\Controller\Order\OrderDeliveredAction;
use App\Controller\Order\OrderInDeliveryAction;
use App\Controller\Order\OrderPayAction;
use App\Controller\Order\OrderProcessingAction;
use App\Controller\Order\OrderRefundAction;
use App\Controller\Order\OrderRemoveVoucherAction;
use App\Entity\Country\Currency;
use App\Entity\Order\Carrier\CarrierData;
use App\Entity\Order\Line\OrderLine;
use App\Entity\Order\PaymentMethod\PaymentMethod;
use App\Entity\Order\Status\OrderStatus;
use App\Entity\Order\Voucher\VoucherData;
use App\Entity\Restaurant\Restaurant;
use App\Entity\User\Customer\Address;
use App\Entity\User\Customer\Customer;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Entity\User\SuperAdmin\SuperAdmin;
use App\Interfaces\TestableEntityInterface;
use App\Repository\OrderRepository;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[ORM\Table(name: '`order`')]
#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ApiResource(
    collectionOperations: [
        "get"                                  => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ACCOUNTANT . "')",
        ],
        "post"                                 => [
            "security_post_denormalize" => "object.getCustomer() === user",
            'denormalization_context'   => [
                'groups' => [
                    'order:post',
                ],
            ],
        ],
        'api_customers_orders_get_subresource' => [
            'method'                           => 'GET',
            "pagination_enabled"               => true,
            "pagination_client_items_per_page" => true,
        ],
    ],
    itemOperations: [
        "get"            => [
//            Seulement les logisticiens du restaurant pour laquelle la commande a été passé ou les comptables ou le client de la commande
"security" => "
                is_granted('" . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . "') or 
                (is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant()) or 
                is_granted('" . FranchisorEmployee::ROLE_ACCOUNTANT . "') or 
                object.getCustomer() === user
            ",
        ],
        "patch"          => [
            //            Seulement les logisticiens du restaurant pour laquelle la commande a été passé et si la commande n'a pas déjà été payé
            "security" => "object.getOrderedAt() === null",
        ],
        "update_status"  => [
            'method'                  => 'PATCH',
            'path'                    => '/orders/{reference}/status',
            //            Seulement les logisticiens du restaurant pour laquelle la commande a été passé
            //            TODO : Peaufiner la sécurité à l'aide d'un Voter
            "security"                => "is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant()",
            'validation_groups'       => [
                'order:status',
            ],
            'denormalization_context' => [
                'groups' => [
                    'order:status', 'order:status:write',
                ],
            ],
        ],
        "pay"            => [
            'method'                  => 'PATCH',
            'path'                    => '/orders/{reference}/pay',
            'controller'              => OrderPayAction::class,
            //            Seulement les clients
            "security"                => "object.getCustomer() === user and object.getOrderedAt() === null",
            'validation_groups'       => [
                'order:pay',
            ],
            'denormalization_context' => [
                'groups' => [
                    'order',
                    'order:write',
                    'order:pay:write',
                ],
            ],
        ],
        "cancel"         => [
            'method'                  => 'PATCH',
            'path'                    => '/orders/{reference}/cancel',
            'controller'              => OrderCancelAction::class,
            //            Seulement les clients
            "security"                => "(object.getCustomer() === user and object.getOrderedAt() !== null) or is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "')",
            'validation_groups'       => [
                'order:pay',
            ],
            'denormalization_context' => [
                'groups' => [
                    'order:cancel',
                    'order:cancel:write',
                ],
            ],
        ],
        "refund"         => [
            'method'                  => 'PATCH',
            'path'                    => '/orders/{reference}/refunded',
            'controller'              => OrderRefundAction::class,
            //            Seulement les logisticiens du restaurant pour laquelle la commande a été passé et qu'elle a été payé
            "security"                => "is_granted('" . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . "') or (is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant() and object.getOrderedAt() !== null)",
            'validation_groups'       => [
                'order:refund',
            ],
            'denormalization_context' => [
                'groups' => [
                    'order:refund:write',
                ],
            ],
        ],
        "processing"         => [
            'method'                  => 'PATCH',
            'path'                    => '/orders/{reference}/processing',
            'controller'              => OrderProcessingAction::class,
            //            Seulement les logisticiens du restaurant pour laquelle la commande a été passé et qu'elle a été payé
            // "security"                => "is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant()",
            "security"                => "is_granted('" . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . "') or (is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant())",
            'validation_groups'       => [
                'order:processing',
            ],
            'denormalization_context' => [
                'groups' => [
                    'order:processing:write',
                ],
            ],
        ],
        "in_delivery"         => [
            'method'                  => 'PATCH',
            'path'                    => '/orders/{reference}/in_delivery',
            'controller'              => OrderInDeliveryAction::class,
            //            Seulement les logisticiens du restaurant pour laquelle la commande a été passé et qu'elle a été payé
            // "security"                => "is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant()",
            "security"                => "is_granted('" . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . "') or (is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant())",
            'validation_groups'       => [
                'order:in_delivery',
            ],
            'denormalization_context' => [
                'groups' => [
                    'order:in_delivery:write',
                ],
            ],
        ],
        "available_in_restaurant"         => [
            'method'                  => 'PATCH',
            'path'                    => '/orders/{reference}/available_in_restaurant',
            'controller'              => OrderAvailableInRestaurantAction::class,
            //            Seulement les logisticiens du restaurant pour laquelle la commande a été passé et qu'elle a été payé
            // "security"                => "is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant()",
            "security"                => "is_granted('" . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . "') or (is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant())",
            'validation_groups'       => [
                'order:available_in_restaurant',
            ],
            'denormalization_context' => [
                'groups' => [
                    'order:available_in_restaurant:write',
                ],
            ],
        ],
        "delivered"         => [
            'method'                  => 'PATCH',
            'path'                    => '/orders/{reference}/delivered',
            'controller'              => OrderDeliveredAction::class,
            //            Seulement les logisticiens du restaurant pour laquelle la commande a été passé et qu'elle a été payé
            // "security"                => "is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant()",
            "security"                => "is_granted('" . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . "') or (is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant())",
            'validation_groups'       => [
                'order:delivered',
            ],
            'denormalization_context' => [
                'groups' => [
                    'order:delivered:write',
                ],
            ],
        ],
        "add_voucher"    => [
            'method'            => 'PATCH',
            'path'              => '/orders/{reference}/voucher/add',
            'controller'        => OrderAddVoucherAction::class,
            //            Seulement les clients
            "security"          => "object.getCustomer() === user and object.getOrderedAt() === null",
            'validation_groups' => [
                'order:voucher:add',
            ],
        ],
        "remove_voucher" => [
            'method'            => 'PATCH',
            'path'              => '/orders/{reference}/voucher/remove',
            'controller'        => OrderRemoveVoucherAction::class,
            //            Seulement les clients
            "security"          => "object.getCustomer() === user and object.getOrderedAt() === null",
            'validation_groups' => [
                'order:voucher:remove',
            ],
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "order", "order:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "order", "order:read",
        ],
    ],
    order: ["orderedAt" => "DESC"]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'reference' => 'partial',
        'restaurant.slug' => 'exact',
        'customer.email' => 'partial',
        'status.slug' => 'partial',
    ]
)]
#[ApiFilter(
    OrderFilter::class,
    properties: [
        'reference',
        'orderedAt',
    ]
)]
class Order implements TestableEntityInterface
{

    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[ORM\ManyToOne(targetEntity: Address::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[NotNull(message: 'L\'adresse de facturation doit être renseignée.', groups: ['_order:pay'])]
    #[Groups([
        'order',
    ])]
    private ?Address $billingAddress;

    #[ORM\ManyToOne(targetEntity: CarrierData::class)]
    #[NotNull(message: 'Le livreur doit être renseigné.', groups: ['_order:pay'])]
    #[Groups([
        'order',
    ])]
    private ?CarrierData $carrier;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'orders')]
    #[ORM\JoinColumn(referencedColumnName: 'uuid', nullable: false)]
    #[NotNull(message: 'Le client doit être renseigné.')]
    #[Groups([
        'order:post',
        'order:read',
    ])]
    private ?Customer $customer = null;

    #[ORM\ManyToOne(targetEntity: Address::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups([
        'order',
    ])]
    private ?Address $deliveryAddress;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups([
        'order:read',
    ])]
    private ?DateTime $lastPaymentTryAt;

    /**
     * @var OrderLine[]|Collection
     */
    #[ORM\OneToMany(mappedBy: 'orderReference', targetEntity: OrderLine::class, orphanRemoval: true)]
    #[Count(min: 1, minMessage: "Il faut au moins un produit pour réaliser une commande", groups: ['order:pay'])]
    #[Groups([
        'order:read',
    ])]
    private array|Collection $orderLines;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups([
        'order:read',
        'order:pay:write',
    ])]
    private ?DateTimeImmutable $orderedAt;

    #[ORM\Column(type: 'string', length: 100, nullable: true)]
    #[NotNull(message: 'Pour attester que la commande a bien été payé vous devez renseigner l\'identifiant de paiement renvoyé par la plateforme de paiement.', groups: ['_order:pay'])]
    #[Groups([
        'order:read',
        'order:pay',
    ])]
    private string $paymentIdentifier;

    #[ORM\ManyToOne(targetEntity: PaymentMethod::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[NotNull(message: 'Le moyen de paiement utilisé pour payer doit être renseigné.', groups: ['_order:pay'])]
    #[Groups([
        'order',
    ])]
    private ?PaymentMethod $paymentMethod;

    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 15)]
    #[NotBlank(message: "La référence doit être renseignée.")]
    #[Length(
        min: 5,
        max: 15,
        minMessage: "La référence doit faire au moins {{limit}} caractères.",
        maxMessage: "La référence doit faure au maximum {{limit}} caractères."
    )]
    #[Groups([
        'order:read',
    ])]
    private string $reference;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups([
        'order:read',
    ])]
    private ?DateTimeImmutable $refundedAt;

    #[ORM\ManyToOne(targetEntity: Restaurant::class, inversedBy: 'orders')]
    #[ORM\JoinColumn(nullable: true)]
    #[NotNull(message: 'Le restaurant qui recevra la commande doit être renseignée.', groups: ['order:pay'])]
    #[Groups([
        'order',
    ])]
    private ?Restaurant $restaurant;

    #[ORM\ManyToOne(targetEntity: OrderStatus::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull(message: 'Le statut doit être renseigné.')]
    #[Groups([
        'order:read',
        'order:status',
    ])]
//    TODO : check des status (changement uniquement autorisé par le côté back)
    private OrderStatus $status;

    /**
     * @var VoucherData[]|Collection
     */
    #[ORM\ManyToMany(targetEntity: VoucherData::class)]
    #[ORM\JoinColumn(referencedColumnName: 'reference')]
    private array|Collection $vouchers;

    #[Pure] public function __construct()
    {
        $this->vouchers = new ArrayCollection();
        $this->orderLines = new ArrayCollection();
    }

    public function addOrderLine(OrderLine $orderLine): self
    {
        if (!$this->orderLines->contains($orderLine)) {
            $this->orderLines[] = $orderLine;
            $orderLine->setOrderReference($this);
        }

        return $this;
    }

    public function addVoucher(VoucherData $voucher): self
    {
        if (!$this->vouchers->contains($voucher)) {
            $this->vouchers[] = $voucher;
        }

        return $this;
    }

    public function getBillingAddress(): ?Address
    {
        return $this->billingAddress;
    }

    public function setBillingAddress(?Address $billingAddress): self
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    public function getCarrier(): ?CarrierData
    {
        return $this->carrier;
    }

    public function setCarrier(?CarrierData $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }

    #[Pure]
    #[Groups([
        'order:read',
    ])]
    public function getCurrency(): ?Currency
    {
        return $this?->restaurant?->getDefaultCurrency();
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getDeliveryAddress(): ?Address
    {
        return $this->deliveryAddress;
    }

    public function setDeliveryAddress(?Address $deliveryAddress): self
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    public function getLastPaymentTryAt(): ?DateTimeInterface
    {
        return $this->lastPaymentTryAt;
    }

    public function setLastPaymentTryAt(?DateTimeInterface $lastPaymentTryAt): self
    {
        $this->lastPaymentTryAt = $lastPaymentTryAt;

        return $this;
    }

    /**
     * @return Collection|OrderLine[]
     */
    public function getOrderLines(): Collection
    {
        return $this->orderLines;
    }

    public function getOrderedAt(): ?DateTimeImmutable
    {
        return $this->orderedAt;
    }

    public function setOrderedAt(?DateTimeImmutable $date): self
    {
        $this->orderedAt = $this->orderedAt ?? $date;

        return $this;
    }

    public function getPaymentIdentifier(): ?string
    {
        return $this->paymentIdentifier;
    }

    public function setPaymentIdentifier(?string $paymentIdentifier): self
    {
        $this->paymentIdentifier = $paymentIdentifier;

        return $this;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getRefundedAt(): ?DateTimeImmutable
    {
        return $this->refundedAt;
    }

    public function setRefundedAt(): self
    {
        $this->refundedAt = $this->refundedAt ?? new DateTimeImmutable();

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        foreach ($this->orderLines as $line) {
            $line->calculateTaxRate();
        }

        return $this;
    }

    public function getStatus(): ?OrderStatus
    {
        return $this->status;
    }

    public function setStatus(?OrderStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|VoucherData[]
     */
    public function getVouchers(): array|Collection
    {
        return $this->vouchers;
    }

    public function removeOrderLine(OrderLine $orderLine): self
    {
        if ($this->orderLines->removeElement($orderLine)) {
            // set the owning side to null (unless already changed)
            if ($orderLine->getOrderReference() === $this) {
                $orderLine->setOrderReference(null);
            }
        }

        return $this;
    }

    public function removeVoucher(VoucherData $voucher): self
    {
        $this->vouchers->removeElement($voucher);

        return $this;
    }

    public function voucherCanBeAdded(VoucherData $voucher)
    {
//TODO : check that voucher can be applied to order
    }

    public function getEntityName(): string
    {
        return "Commande";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            "update_status"  => "Mise à jour du statut de la commande",
            "pay"            => "Paiement de la commande",
            "refund"         => "Remboursement de la commande",
            "add_voucher"    => "Ajout d'une coupon à la commande",
            "remove_voucher" => "Suppression d'un coupon à la commande",
        ];
    }

    public function getEntityRouteName(): string
    {
        return "orders";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return true;
    }


    public function getEntityIdentifierName(): string
    {
        return 'reference';
    }

    #[Pure]
    #[Groups('order:read')]
    public function getTotal(): float
    {
        $total = 0;

        foreach ($this->getOrderLines() as $orderLine) {
            $total += $orderLine->getTotal();
        }

        return $total;
    }

    #[Pure]
    #[Groups('order:read')]
    public function getTotalTaxExcluded(): float
    {
        $total = 0;

        foreach ($this->getOrderLines() as $orderLine) {
            $total += $orderLine->getTotalTaxExcluded();
        }

        return $total;
    }

    #[Pure]
    #[Groups('order:read')]
    public function getTaxAmount(): float
    {
        $total = 0;

        foreach ($this->getOrderLines() as $orderLine) {
            $total += $orderLine->getTaxAmount();
        }

        return $total;
    }

}
