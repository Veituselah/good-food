<?php

namespace App\Entity\User\Customer;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\AddressRepository;
use App\Traits\Fields\AddressTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use PHPUnit\Framework\Test;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[Entity(repositoryClass: AddressRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        //	Les logisticiens ou + peuvent récupérer la liste des addresses
        "get"  => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '")',
        ],
        // Tout le monde peut créer une adresse
        "post" => [
            "security_post_denormalize" => "is_granted('" . Customer::DEFAULT_ROLE_CUSTOMER . "') and object.getCustomer() === user and object.getCustomer().hasConfirmedAccount()",
            "denormalization_context"   => [
                "groups" => [
                    "address", "address:write",
                    "address:post", "address:post:write",
                    // "date", "date:write",
                    // "address:post", "address:post:write",
                    // "address", "address:write",
                ],
            ],
        ],
        'api_customers_addresses_get_subresource'=> [
            'method' => 'GET',
            "pagination_enabled" => true,
            "pagination_client_items_per_page" => true
        ]
    ],
    itemOperations: [
        //	Les logisticiens ou + ou le possesseur du compte peuvent récupérer l'adresse
        "get"    => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") or object.getCustomer() === user',
        ],
        //	Le possesseur du compte peut modifier l'adresse
        "patch"  => [
            "security" => "object.getCustomer() === user && is_granted('ADDRESS_MANAGE', object)",
        ],
        //	Le possesseur du compte peut supprimer son adresse
        "delete" => [
            "security" => "object.getCustomer() === user && is_granted('ADDRESS_MANAGE', object)",
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "address", "address:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "address", "address:read",
        ],
    ]

)]
class Address implements TestableEntityInterface
{

    use SlugNameTrait;
    use AddressTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use DeletedAtTrait;

    #[ManyToOne(targetEntity: Customer::class, inversedBy: "addresses")]
    #[JoinColumn(referencedColumnName: "uuid", nullable: false)]
    #[NotNull(
        message: "Le client possédant l'adresse doit être renseigné."
    )]
    #[Groups(['address:post', 'address:read'])]
    #[ApiProperty(readableLink: false)]
    private Customer $customer;

    #[Column(type: "string", length: 50)]
    #[NotBlank(message: "Le nom de l'adresse doit être renseigné (Ex: Travail, Maison, ...).")]
    #[Length(
        min: 1,
        max: 50,
        minMessage: "Le nom de l'adresse doit faire au moins {{ limit }} caractère.",
        maxMessage: "Le nom de l'adresse doit faire maximum {{ limit }} caractères.",
    )]
    #[Groups('address')]
    private string $name;

    public function getCustomer(): ?Customer
    {
        return $this->customer ?? null;
    }

    public function setCustomer(Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    function purgeData()
    {
        // Ne pas purger le nom de l'adresse sinon le slug est modifié ce qui provoque une erreur
        // $this->name = self::getStringClearValue();
        $this->street = self::getStringClearValue();
        $this->zipCode = self::getStringClearValue();
        $this->city = self::getStringClearValue();
        $this->additionalAddress = self::getStringClearValue();
        $this->phoneNumber = self::getStringClearValue();
    }

    function purgeDataOnDelete(): bool
    {
        return true;
    }

    public function getEntityName(): string
    {
        return "Adresse";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "addresses";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return true;
    }
}
