<?php

namespace App\Filter\Product;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\PropertyInfo\Type;

class SearchByNameOrReference extends AbstractContextAwareFilter
{

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {

        if (
            !$this->isPropertyEnabled($property, $resourceClass)
            // !$this->isPropertyMapped($property, $resourceClass)
        ) {
            return;
        }

        $parameterName = $queryNameGenerator->generateParameterName($property); // Generate a unique parameter name to avoid collisions with other filters
        $queryBuilder
            ->andWhere($queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('o.name', ':' . $parameterName),
                $queryBuilder->expr()->like('o.reference', ':' . $parameterName),
            ))
            ->setParameter($parameterName, '%' . $value . '%');
    }

    public function getDescription(string $resourceClass): array
    {

        return [
            'search_by_name_or_city' => [
                'property'    => 'search_by_name_or_reference',
                'type'        => Type::BUILTIN_TYPE_STRING,
                'required'    => false,
                'description' => 'Filtre les produits par nom ou référence!',
            ],
        ];
    }

}