<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Order\OrderAddVoucherAction;
use App\Controller\Order\OrderCancelAction;
use App\Controller\Order\OrderPayAction;
use App\Controller\Order\OrderRefundAction;
use App\Controller\Order\OrderRemoveVoucherAction;
use App\Controller\Reservation\ReservationCancelAction;
use App\Entity\Restaurant\Restaurant;
use App\Entity\User\Customer\Customer;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Repository\ReservationRepository;
use App\Service\Tools\Tools;
use App\Traits\Fields\Identifiers\IDTrait;
use App\Validator\Constraint\CheckReservationNotAlreadyDone;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Regex;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
#[UniqueEntity(fields: ['customer', 'restaurant', 'reservedDayPart', 'reservedFor'], message: "Vous avez déjà une réservation en cours pour ce restaurant, allez la voir dans votre profil !")]
#[ApiResource(
    collectionOperations: [
        "get"                                        => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ACCOUNTANT . "')",
        ],
        "post"                                       => [
            "security_post_denormalize" => "object.getCustomer() === user",
        ],
        'api_customers_reservations_get_subresource' => [
            'method'                           => 'GET',
            "pagination_enabled"               => true,
            "pagination_client_items_per_page" => true,
        ],
    ],
    itemOperations: [
        "get"    => [
//            Seulement les logisticiens du restaurant pour laquelle la commande a été passé ou les comptables ou le client de la commande
"security" => "
                (is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "') and object.getRestaurant() === user.getRestaurant()) or 
                is_granted('" . FranchisorEmployee::ROLE_ACCOUNTANT . "') or 
                object.getCustomer() === user
            ",
        ],
        "cancel" => [
            'method'                  => 'PATCH',
            'path'                    => '/reservations/{id}/cancel',
            'controller'              => ReservationCancelAction::class,
            //            Seulement les clients
            "security"                => "(object.getCustomer() === user and object.getCanceledAt() === null) or is_granted('" . FranchiseEmployee::ROLE_LOGISTICIAN . "')",
            'denormalization_context' => [
                'groups' => [
                    'cancel:cancel',
                    'cancel:cancel:write',
                ],
            ],
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "reservation", "reservation:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "reservation", "reservation:read",
        ],
    ],
    order: [
        "canceledAt" => "ASC",
        "reservedFor" => "DESC",
    ]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'restaurant.slug' => 'exact',
        'customer.email'  => 'partial',
    ]
)]
#[ApiFilter(
    ExistsFilter::class,
    properties: [
        'canceledAt',
    ]
)]
#[ApiFilter(
    OrderFilter::class,
    properties: [
        'reservedFor',
    ]
)]
class Reservation
{

    use IDTrait;

    #[ORM\ManyToOne(targetEntity: Restaurant::class, inversedBy: 'reservations')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull(message: 'Vous devez renseigner le restaurant')]
    #[Groups('reservation')]
    private ?Restaurant $restaurant;

    #[ORM\Column(type: 'string', length: 255)]
    #[NotBlank(message: 'Vous devez renseigner votre numéro de téléphone')]
    #[Regex(Tools::PHONE_REGEX, message: Tools::PHONE_ERROR)]
    #[Groups('reservation')]
    private string $phoneNumber;

    #[ORM\Column(type: 'datetime')]
    #[Groups('reservation')]
    #[NotNull(message: 'Vous devez renseigner la date de réservation')]
    #[GreaterThan('now', message: 'Vous ne pouvez pas réserver sur une plage horaire déjà passée')]
    #[CheckReservationNotAlreadyDone]
    private ?DateTimeInterface $reservedFor;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'reservations')]
    #[ORM\JoinColumn(referencedColumnName: 'uuid', nullable: false)]
    #[NotNull(message: 'Vous devez renseigner le client')]
    #[Groups('reservation')]
    private Customer $customer;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Groups('reservation:read')]
    private ?DateTimeImmutable $canceledAt;

    #[ORM\Column(type: 'integer')]
    #[Groups('reservation')]
    #[NotNull(message: 'Vous devez renseigner le nombre de personne qui viennent manger')]
    #[LessThanOrEqual(value: 20, message: 'Les groupes de plus de 20 personnes doivent réserver par téléphone !')]
    #[GreaterThanOrEqual(value: 1, message: 'Au moins une personne doit venir manger pour réserver')]
    private int $nbPerson;

    #[ORM\Column(type: 'string', length: 20)]
    #[NotNull(message: 'Vous devez renseigner le moment de la journée où vous réservez')]
    #[Groups('reservation')]
    private string $reservedDayPart;

    public function getRestaurant(): ?Restaurant
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getReservedFor(): ?DateTimeInterface
    {
        return $this->reservedFor;
    }

    public function setReservedFor(DateTimeInterface $reservedFor): self
    {
        $this->reservedFor = $reservedFor;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getCanceledAt(): ?DateTimeImmutable
    {
        return $this->canceledAt;
    }

    public function setCanceledAt(): self
    {
        $this->canceledAt = $canceledAt ?? new DateTimeImmutable();

        return $this;
    }

    public function getNbPerson(): ?int
    {
        return $this->nbPerson;
    }

    public function setNbPerson(int $nbPerson): self
    {
        $this->nbPerson = $nbPerson;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getReservedDayPart(): ?string
    {
        return $this->reservedDayPart;
    }

    public function setReservedDayPart(string $reservedDayPart): self
    {
        $this->reservedDayPart = $reservedDayPart;

        return $this;
    }

}
