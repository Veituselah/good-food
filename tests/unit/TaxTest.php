<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\Country\Country;
use App\Entity\Country\Tax\Tax;
use App\Entity\Country\Tax\TaxType;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\AdminExampleTrait;
use App\Traits\Tests\CountryExampleTrait;
use App\Traits\Tests\TaxExampleTrait;
use App\Traits\Tests\TaxTypeExampleTrait;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class TaxTest extends CrudApiTestCase
{

    use AdminExampleTrait;
    use TaxExampleTrait;
    use TaxTypeExampleTrait;
    use CountryExampleTrait;

    // Fixtures
    const TAX_FIXTURES_FILES = 'tax.yml';

    // Routes
    const TAX_ROUTE = self::API_ROUTE . '/taxes';
    const TAX_ITEM_ROUTE = self::TAX_ROUTE . '/{slug}';
    const TAX_ACTIVE_ROUTE = self::TAX_ITEM_ROUTE . '/active';

    // JSON LD Data
    const TAX_TYPE = 'Tax';
    const TAX_CONTEXT = self::CONTEXT . '/' . self::TAX_TYPE;

    // Data
    const ALL_FIELDS_VIOLATION = ['name', 'rate', 'type'];
    const TAX_EXAMPLE = "fr_20";

    /**
     * @return Tax[]
     */
    #[Pure] public static function getTaxFixtures(): array
    {
        return self::getDataFixturesOfClass(Tax::class);
    }

    /**
     * @throws Exception
     */
    public static function getValidTestTax(
        TaxType $type,
        Country $country
    ): Tax
    {
        $tax = new Tax();
        $tax->setName("Tax Test");
        $tax->setSlug(self::slugify($tax->getName()));
        $tax->setRate(0.25);
        $tax->setType($type);
        $tax->setCountry($country);

        return $tax;
    }

    /**
     * @throws Exception
     * @throws ExceptionInterface
     */
    public static function getValidTestTaxInJSON(
        TaxType $type,
        Country $country
    ): array
    {
        $tax = self::getValidTestTax($type, $country);
        $data = self::parseEntityToArray($tax);
        $data['country'] = self::replaceParamsRoute(CountryTest::COUNTRY_ITEM_ROUTE, ['isoCode' => $tax->getCountry()->getIsoCode()]);
        $data['type'] = self::replaceParamsRoute(TaxTypeTest::TAX_TYPE_ITEM_ROUTE, ['slug' => $tax->getType()->getSlug()]);
        unset($data['slug']);

        return $data;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAdmin();
        $this->loadTax();
        $this->loadTaxType();
        $this->loadCountry();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     */
    public function testPostInvalidItem()
    {
        $validTax = self::getValidTestTaxInJSON(
            $this->taxTypeExample,
            $this->countryExample
        );

        // Empty request error
        $requestEmptyData = $this->doRequest(
            method: self::POST_METHOD,
            url: self::TAX_ROUTE,
            json: [],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestEmptyData);

        // Min carac error

        $requestNotRespectMinValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::TAX_ROUTE,
            json: [
                'name' => "",
            ],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac error

        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::TAX_ROUTE,
            json: [
                'name' => self::STRING_LENGTH_256,
            ],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMaxValidation);

//        Malformatted currency iri error

        $data = $validTax;
        $data['type'] = 'azer';

        $this->doRequest(
            method: self::POST_METHOD,
            url: self::TAX_ROUTE,
            json: $data,
            token: $this->adminExampleToken
        );

        $this->assertBadRequest();

//        Malformatted country iri error

        $data = $validTax;
        $data['country'] = 'azer';

        $this->doRequest(
            method: self::POST_METHOD,
            url: self::TAX_ROUTE,
            json: $data,
            token: $this->adminExampleToken
        );

        $this->assertBadRequest();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function testPostItem()
    {
        $data = $this->getValidTestTaxInJSON($this->taxTypeExample, $this->countryExample);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $validJSON = $json;
                $validJSON['type'] = $this->parseEntityToArray($this->taxTypeExample);
                $validJSON['country'] = $this->parseEntityToArray($this->countryExample);
                unset($validJSON['country']['defaultCurrency']);

                $taxCreated = $response->toArray();

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::TAX_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::TAX_TYPE,
                        'deleted'                   => false,
                    ]
                );
                $this->assertJsonContains($validJSON);

                $this->assertIdentifierOfResponseData(self::TAX_ROUTE, Tools::SLUG_REGEX, $taxCreated);
                $this->assertMatchesResourceItemJsonSchema(Tax::class);

            },
            method: self::POST_METHOD,
            url: self::TAX_ROUTE,
            json: $data
        );

    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws Exception
     */
    public function testDeleteItem(): void
    {
        $superAdminToken = $this->getTokenFromUserIndex(self::CEDRIC_ADMIN);
        $nbCustomersInCollectionBefore = $this->getTotalItemsForRequest(self::TAX_ROUTE, $superAdminToken);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                $this->adminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccessDelete();

                $this->assertNull(self::getEntityManager()->getRepository(Tax::class)->findOneBy(['slug' => $this->taxExample->getSlug()]));

                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->taxExampleItemIRI,
                    token: $superAdminToken,
                );

                $this->assertNotFound();

                $nbCustomersInCollectionAfter = $this->getTotalItemsForRequest(self::TAX_ROUTE, $superAdminToken);

                $this->assertEquals($nbCustomersInCollectionBefore - 1, $nbCustomersInCollectionAfter);

                $this->initClient();

            },
            method: self::DELETE_METHOD,
            url: $this->taxExampleItemIRI,
            dataToSendToFunctionAssertAhtorizeUser: compact('nbCustomersInCollectionBefore', 'superAdminToken')
        );
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::ALL_USERS,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::TAX_ROUTE,
                    context: self::TAX_CONTEXT,
                    classToCheckValidity: Tax::class,
                    totalItems: count(self::getTaxFixtures())
                );
            },
            method: self::GET_METHOD,
            url: self::TAX_ROUTE,
        );
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetItem(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::ALL_USERS,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::TAX_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::TAX_TYPE,
                        'active'                    => $this->taxExample->isActive(),
                        'deleted'                   => $this->taxExample->isDeleted(),
                    ]
                );

                $validJson = self::parseEntityToArray($this->taxExample);
                unset($validJson['country']['defaultCurrency']);

                $this->assertJsonContains($validJson);

                $tax = $response->toArray();

                $this->assertIdentifierOfResponseData(self::TAX_ROUTE, Tools::SLUG_REGEX, $tax);
                $this->assertMatchesResourceItemJsonSchema(Tax::class);

            },
            method: self::GET_METHOD,
            url: $this->taxExampleItemIRI,
        );
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function testPatchItem(): void
    {
        $taxTest = new Tax();
        $taxTest->setName("a" . $this->taxExample->getName());
        $taxTest->setSlug(self::slugify($taxTest->getName()));
        $taxTest->setRate($this->taxExample->getRate() + 0.1);
        $taxTest->setActive(!$this->taxExample->isActive());

        $json = self::parseEntityToArray($taxTest);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                $this->adminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();
                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::TAX_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::TAX_TYPE,
                        'deleted'                   => false,
                    ]
                );
                $this->assertJsonContains($json);

                $tax = $response->toArray();

                $this->assertIdentifierOfResponseData(self::TAX_ROUTE, Tools::SLUG_REGEX, $tax);
                $this->assertMatchesResourceItemJsonSchema(Tax::class);

                $this->initClient();
            },
            method: self::PATCH_METHOD,
            url: $this->taxExampleItemIRI,
            json: $json,
        );
    }

    public function testPatchInvalidItem()
    {
        // TODO: Implement testPatchInvalidItem() method.
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

}
