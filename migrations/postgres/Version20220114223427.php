<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220114223427 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_supplier ADD currency_id VARCHAR(10) NOT NULL');
        $this->addSql('ALTER TABLE product_supplier ADD CONSTRAINT FK_509A06E938248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_509A06E938248176 ON product_supplier (currency_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product_supplier DROP CONSTRAINT FK_509A06E938248176');
        $this->addSql('DROP INDEX IDX_509A06E938248176');
        $this->addSql('ALTER TABLE product_supplier DROP currency_id');
    }
}
