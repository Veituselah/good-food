<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220129145457 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE restaurant DROP CONSTRAINT fk_eb95123fecd792c0');
        $this->addSql('DROP INDEX idx_eb95123fecd792c0');
        $this->addSql('ALTER TABLE restaurant DROP default_currency_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE restaurant ADD default_currency_id VARCHAR(10) NOT NULL');
        $this->addSql('ALTER TABLE restaurant ADD CONSTRAINT fk_eb95123fecd792c0 FOREIGN KEY (default_currency_id) REFERENCES currency (iso_code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_eb95123fecd792c0 ON restaurant (default_currency_id)');
    }
}
