<?php

namespace App\Repository;

use App\Entity\User\User;
use App\Entity\User\UserRequest\UserRequest;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Exception;
use Symfony\Component\Uid\Uuid;

/**
 * @method UserRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserRequest[]    findAll()
 * @method UserRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
abstract class UserRequestRepository extends ServiceEntityRepository
{
//    public abstract function getRequestWhichDosntHaveReachTimeBeforeNextRequest(User $user): array;

    public abstract function getDelayBeforeTwoRequest(): int;

    public abstract function getUserFieldName(): string;

    /**
     * @throws Exception
     */
    public function getRequestWhichDosntHaveReachTimeBeforeNextRequestForUser(User $user): array
    {
        return $this->getRequestWhichDosntHaveReachTimeBeforeNextRequestForUserIdentifier($user->getUuid());
    }

    /**
     * @throws Exception
     */
    public function getRequestWhichDosntHaveReachTimeBeforeNextRequestForUserIdentifier(mixed $userIdentifier): array
    {
        $date = new DateTime();
        $date->sub(new DateInterval('PT' . $this->getDelayBeforeTwoRequest() . 'M'));

        $binaryUserIdentifier = Uuid::fromString($userIdentifier)->toBinary();

        return $this
            ->createQueryBuilder("ur")
            ->where("ur.createdAt > :date")
            ->andWhere("ur." . $this->getUserFieldName() . " = :user")
            ->orderBy("ur.id", "DESC")
            ->setParameter('date', $date)
            ->setParameter('user', $binaryUserIdentifier)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @throws Exception
     */
    public function removeAllRequestsForUser(User $user): int
    {
        return $this->removeAllRequestsForUserIdentifier($user->getUuid());
    }

    public function removeAllRequestsForUserIdentifier(mixed $userIdentifier): int
    {
        $binaryUserIdentifier = Uuid::fromString($userIdentifier)->toBinary();

        return $this
            ->createQueryBuilder("ur")
            ->andWhere("ur." . $this->getUserFieldName() . " = :user")
            ->setParameter('user', $binaryUserIdentifier)
            ->delete()
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     *
     * Retourne vrai si une une requête n'a pas dépassée le temps minimum avant la prochaine requête
     *
     * @return bool
     * @throws Exception
     */
    public function hasPendingRequestWhichDosntHaveReachTimeBeforeNextRequest(User $user): bool
    {
        return count($this->getRequestWhichDosntHaveReachTimeBeforeNextRequestForUser($user)) != 0;
    }

}
