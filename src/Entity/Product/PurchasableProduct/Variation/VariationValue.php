<?php

namespace App\Entity\Product\PurchasableProduct\Variation;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\VariationValueRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[ORM\Entity(repositoryClass: VariationValueRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[UniqueEntity(fields: ['name', 'variationGroup'], message: "Il existe déjà une valeur de déclinaison nomméot'{{value}}' pour ce groupe")]
#[ApiResource(
    collectionOperations: [
        "get"  => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") or is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "post" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    itemOperations: [
        "get"    => [],
        "patch"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "delete" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "variation_value", "variation_value:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "variation_value", "variation_value:read",
        ],
    ]
)]
class VariationValue implements TestableEntityInterface
{

    use SlugNameTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[ORM\Column(type: 'string', length: 30)]
    #[NotBlank(message: "Le libellé d'une valeur de déclinaison doit être renseigné")]
    #[Length(
        min: 2,
        max: 30,
        minMessage: "Le libellé d'une valeur de déclinaison doit faire plus de {{limit}} caractères",
        maxMessage: "Le libellé d'une valeur de déclinaison doit faire moins de {{limit}} caractères",
    )]
    #[Groups([
        'variation_value',
        "variation_group:read",
        'variation:read',
    ])]
    private string $name;

    #[ORM\Column(type: 'integer')]
    #[NotNull(message: "La position doit être définie")]
    #[GreaterThanOrEqual(value: 0, message: "La position doit être supérieure ou égale à {{compared_value}}")]
    #[Groups([
        'variation_value',
        "variation_group:read",
        'variation:read',
    ])]
    private int $position;

    #[ORM\ManyToOne(targetEntity: VariationGroup::class, inversedBy: 'variationValues')]
    #[ORM\JoinColumn(nullable: false)]
    #[NotNull(message: "Le groupe de déclinaison d'une valeur de déclinaison doit être renseignée")]
    #[Groups('variation_value')]
    private VariationGroup $variationGroup;

    function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getVariationGroup(): ?VariationGroup
    {
        return $this->variationGroup;
    }

    public function setVariationGroup(?VariationGroup $variationGroup): self
    {
        $this->variationGroup = $variationGroup;

        return $this;
    }

    function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function getEntityName(): string
    {
        return "Valeur de déclinaison";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "variation_values";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }
}
