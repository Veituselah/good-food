<?php


namespace App\Event;


use App\Entity\User\UserRequest\UserRequest;
use stdClass;
use Symfony\Contracts\EventDispatcher\Event;

class UserRequestCheckValidityBeforeCreationEvent extends Event
{
    public const NAME = 'user_request.check_validity_before_creation';

    public function __construct(
        protected UserRequest $request,
    )
    {
    }

    public function getRequest(): UserRequest
    {
        return $this->request;
    }

}