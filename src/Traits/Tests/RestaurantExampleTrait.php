<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\Restaurant\Restaurant;
use App\Tests\unit\RestaurantTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

trait RestaurantExampleTrait
{

    private ?string $restaurantExampleIndex;

    private ?Restaurant $restaurantExample;

    private ?string $restaurantExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function loadRestaurant()
    {
        $this->restaurantExampleIndex = RestaurantTest::RESTAURANT_EXAMPLE;
        $this->restaurantExample = RestaurantTest::getRestaurantFixtures()[$this->restaurantExampleIndex];
        $this->restaurantExampleItemIRI = CustomApiTestCase::replaceParamsRoute(RestaurantTest::RESTAURANT_ITEM_ROUTE, ['slug' => $this->restaurantExample->getSlug()]);
    }

}