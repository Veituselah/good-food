<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\User\Customer\Customer;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\CustomerConfirmedExampleTrait;
use App\Traits\Tests\CustomerNotConfirmedExampleTrait;
use DateInterval;
use DateTime;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class CustomerTest extends CrudApiTestCase
{

    use CustomerNotConfirmedExampleTrait;
    use CustomerConfirmedExampleTrait;

    // Fixtures
    const CUSTOMER_FIXTURES_FILE = 'customer.yml';

    // Routes
    const CUSTOMER_ROUTE = self::API_ROUTE . '/customers';
    const CUSTOMER_ITEM_ROUTE = self::CUSTOMER_ROUTE . '/{uuid}';
    const CUSTOMER_ACTIVE_ACCOUNT_ROUTE = self::CUSTOMER_ITEM_ROUTE . '/active';
    const CUSTOMER_ADDRESSES_ROUTE = self::CUSTOMER_ITEM_ROUTE . '/addresses';

    // JSON LD Data
    const CUSTOMER_TYPE = 'Customer';
    const CUSTOMER_CONTEXT = self::CONTEXT . '/' . self::CUSTOMER_TYPE;

    // Data
    const ALL_FIELDS_VIOLATION = ['email', 'lastname', 'firstname', 'password', 'birthdate'];

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadCustomerNotConfirmed();
        $this->loadCustomerConfirmed();
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection(): void
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_LOGISTICIAN,
                self::ROLE_LEAD,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::CUSTOMER_ROUTE,
                    context: self::CUSTOMER_CONTEXT,
                    classToCheckValidity: Customer::class,
                    totalItems: count(self::getCustomerFixtures())
                );

            },
            method: self::GET_METHOD,
            url: self::CUSTOMER_ROUTE,
        );

    }

    /**
     * @return Customer[]
     */
    #[Pure] public static function getCustomerFixtures(): array
    {
        return self::getDataFixturesOfClass(Customer::class);
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetItem(): void
    {
        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_LOGISTICIAN,
                self::ROLE_LEAD,
                $this->customerNotConfirmedExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::CUSTOMER_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::CUSTOMER_TYPE,
                        "birthdate"                 => $this->customerNotConfirmedExample->getBirthdate()->setTime(0, 0)->format('c'),
                        "lastname"                  => $this->customerNotConfirmedExample->getLastname(),
                        "firstname"                 => $this->customerNotConfirmedExample->getFirstname(),
                        "email"                     => $this->customerNotConfirmedExample->getEmail(),
                        "roles"                     => $this->customerNotConfirmedExample->getRoles(),
                        'confirmedAccount'          => $this->customerNotConfirmedExample->hasConfirmedAccount(),
                        'active'                    => $this->customerNotConfirmedExample->isActive(),
                        'deleted'                   => $this->customerNotConfirmedExample->isDeleted(),
                    ]
                );

                $customer = $response->toArray();

                $this->assertIdentifierOfResponseData(self::CUSTOMER_ROUTE, Tools::UUID_REGEX, $customer);
                $this->assertMatchesResourceItemJsonSchema(Customer::class);

            },
            method: self::GET_METHOD,
            url: $this->customerNotConfirmedExampleItemIRI,
        );
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws Exception
     */
    public function testDeleteItem(): void
    {
        $superAdminToken = $this->getTokenFromUserIndex(self::CEDRIC_ADMIN);
        $nbCustomersInCollectionBefore = $this->getTotalItemsForRequest(self::CUSTOMER_ROUTE, $superAdminToken);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                $this->customerNotConfirmedExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccessDelete();

                $this->assertNull(self::getEntityManager()->getRepository(Customer::class)->findOneBy(['uuid' => $this->customerNotConfirmedExample->getUuid()]));

                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->customerNotConfirmedExampleItemIRI,
                    token: $superAdminToken
                );

                $this->assertNotFound();

                $this->getTokenForUser($this->customerNotConfirmedExampleUsername, $this->customerNotConfirmedExamplePassword, true);
                $this->assertNotLogin();

                $nbCustomersInCollectionAfter = $this->getTotalItemsForRequest(self::CUSTOMER_ROUTE, $superAdminToken);

                $this->assertEquals($nbCustomersInCollectionBefore - 1, $nbCustomersInCollectionAfter);

                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->replaceParamsRoute(self::CUSTOMER_ITEM_ROUTE, ['uuid' => $this->customerNotConfirmedExample->getUuid()]),
                    token: $this->customerNotConfirmedExampleToken
                );

                $this->assertNotLogin();

                $this->initClient();

            },
            method: self::DELETE_METHOD,
            url: $this->customerNotConfirmedExampleItemIRI,
            dataToSendToFunctionAssertAhtorizeUser: compact(
                'superAdminToken',
                'nbCustomersInCollectionBefore'
            )
        );

    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testPostInvalidItem()
    {
        $requestEmptyData = $this->doRequest(
            method: self::POST_METHOD,
            url: self::CUSTOMER_ROUTE,
            json: []
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestEmptyData);

        $birthdate = new DateTime('-17 years');

        // Max carac lastname, password, firstname, birthdate
        // wrong format email
        $requestNotRespectMinValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::CUSTOMER_ROUTE,
            json: [
                "birthdate" => $birthdate->format('c'),
                "password"  => 'aa&&11',
                "lastname"  => 'a',
                "firstname" => 'a',
                "email"     => 'mail non valide',
            ],
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac lastname, firstname
        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::CUSTOMER_ROUTE,
            json: [
                "lastname"  => self::STRING_LENGTH_256,
                "firstname" => self::STRING_LENGTH_256,
            ],
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMaxValidation);

        //        wrong format birthdate
        $this->doRequest(
            method: self::POST_METHOD,
            url: self::CUSTOMER_ROUTE,
            json: [
                "birthdate" => 'birthdate',
            ],
        );

        $this->assertBadRequest();

    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testPatchInvalidItem()
    {
        $birthdate = new DateTime('-17 years');

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->customerNotConfirmedExampleItemIRI,
            json: [],
            token: $this->customerNotConfirmedExampleToken
        );

        $this->assertSuccess();
        $this->assertJsonContains(
            [
                self::CONTEXT_INDEX_JSON_LD => self::CUSTOMER_CONTEXT,
                self::TYPE_INDEX_JSON_LD    => self::CUSTOMER_TYPE,
                "birthdate"                 => $this->customerNotConfirmedExample->getBirthdate()->setTime(0, 0)->format('c'),
                "lastname"                  => $this->customerNotConfirmedExample->getLastname(),
                "firstname"                 => $this->customerNotConfirmedExample->getFirstname(),
                "email"                     => $this->customerNotConfirmedExample->getEmail(),
                'confirmedAccount'          => $this->customerNotConfirmedExample->hasConfirmedAccount(),
                'active'                    => $this->customerNotConfirmedExample->isActive(),
                'deleted'                   => $this->customerNotConfirmedExample->isDeleted(),
            ]
        );


        // Max carac lastname, password, firstname, birthdate
        // wrong format email
        $requestNotRespectMinValidation = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->customerNotConfirmedExampleItemIRI,
            json: [
                "password"  => 'aa&&11',
                "lastname"  => 'a',
                "firstname" => 'a',
                "email"     => 'mail non valide',
                "birthdate" => $birthdate->format('c'),
            ],
            token: $this->customerNotConfirmedExampleToken
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac lastname, firstname
        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->customerNotConfirmedExampleItemIRI,
            json: [
                "lastname"  => self::STRING_LENGTH_256,
                "firstname" => self::STRING_LENGTH_256,
            ],
            token: $this->customerNotConfirmedExampleToken
        );

        $this->assertViolations(['lastname', 'firstname'], $requestNotRespectMaxValidation);

        //        wrong format birthdate
        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->customerNotConfirmedExampleItemIRI,
            json: [
                "birthdate" => 'birthdate',
            ],
            token: $this->customerNotConfirmedExampleToken
        );

        $this->assertBadRequest();
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testPatchItem(): void
    {
        $json = [
            "birthdate" => $this->customerNotConfirmedExample->getBirthdate()
                                                             ->sub(DateInterval::createFromDateString('1 day'))
                                                             ->setTime(0, 0)
                                                             ->format('c'),
            "password"  => self::PASSWORDS[$this->customerNotConfirmedExampleIndex],
            "lastname"  => "1" . $this->customerNotConfirmedExample->getLastname(),
            "firstname" => "1" . $this->customerNotConfirmedExample->getFirstname(),
            "email"     => "1" . $this->customerNotConfirmedExample->getEmail(),
        ];

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                $this->customerNotConfirmedExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $verifJSON = $json;
                unset($verifJSON['password']);

                $this->assertSuccess();
                $this->assertJsonContains($verifJSON);
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::CUSTOMER_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::CUSTOMER_TYPE,
                        'confirmedAccount'          => $this->customerNotConfirmedExample->hasConfirmedAccount(),
                        'active'                    => $this->customerNotConfirmedExample->isActive(),
                        'deleted'                   => $this->customerNotConfirmedExample->isDeleted(),
                    ]
                );

                $customer = $response->toArray();

                $this->assertIdentifierOfResponseData(self::CUSTOMER_ROUTE, Tools::UUID_REGEX, $customer);
                $this->assertMatchesResourceItemJsonSchema(Customer::class);

            },
            method: self::PATCH_METHOD,
            url: $this->customerNotConfirmedExampleItemIRI,
            json: $json
        );
    }

    public function testPostItem()
    {
        // TODO: Implement testPostItem() method.
    }

    public function testGetCollectionWithFilter()
    {
        $filters = [
            'uuid',
            'firstname',
            'lastname',
            'email',
            'createdAt[before]',
            'createdAt[before]',
            'createdAt[strictly_before]',
            'createdAt[strictly_before]',
            'createdAt[after]',
            'createdAt[after]',
            'createdAt[strictly_after]',
            'createdAt[strictly_after]',
            'updatedAt[before]',
            'updatedAt[before]',
            'updatedAt[strictly_before]',
            'updatedAt[strictly_before]',
            'updatedAt[after]',
            'updatedAt[after]',
            'updatedAt[strictly_after]',
        ];

        $order = [
            'order[createdAt]',
            'order[uuid]',
            'order[firstname]',
            'order[lastname]',
            'order[email]',
        ];
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testActiveCustomer()
    {
        $customerActiveRoute = self::replaceParamsRoute(self::CUSTOMER_ACTIVE_ACCOUNT_ROUTE, ['uuid' => $this->customerNotConfirmedExample->getUuid()]);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_LOGISTICIAN,
                self::ROLE_LEAD,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::CUSTOMER_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::CUSTOMER_TYPE,
                        "birthdate"                 => $this->customerNotConfirmedExample->getBirthdate()->setTime(0, 0)->format('c'),
                        "lastname"                  => $this->customerNotConfirmedExample->getLastname(),
                        "firstname"                 => $this->customerNotConfirmedExample->getFirstname(),
                        "email"                     => $this->customerNotConfirmedExample->getEmail(),
                        "roles"                     => $this->customerNotConfirmedExample->getRoles(),
                        'confirmedAccount'          => $this->customerNotConfirmedExample->hasConfirmedAccount(),
                        'active'                    => false,
                        'deleted'                   => $this->customerNotConfirmedExample->isDeleted(),
                    ]
                );

                $customer = $response->toArray();

                $this->assertIdentifierOfResponseData(self::CUSTOMER_ROUTE, Tools::UUID_REGEX, $customer);
                $this->assertMatchesResourceItemJsonSchema(Customer::class);

                // Impossibilité de créer un token
                $this->getTokenForUser($this->customerNotConfirmedExample->getUserIdentifier(), $this->customerNotConfirmedExamplePassword, true);
                $this->assertNotLogin();

                // Token inutilisable
                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->customerNotConfirmedExampleItemIRI,
                    token: $this->customerNotConfirmedExampleToken
                );

                $this->assertNotLogin();

                // On réactive le compte
                $this->doRequest(
                    method: self::PATCH_METHOD,
                    url: $customerActiveRoute,
                    json: [
                        'active' => true,
                    ],
                    token: $token
                );

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::CUSTOMER_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::CUSTOMER_TYPE,
                        "birthdate"                 => $this->customerNotConfirmedExample->getBirthdate()->setTime(0, 0)->format('c'),
                        "lastname"                  => $this->customerNotConfirmedExample->getLastname(),
                        "firstname"                 => $this->customerNotConfirmedExample->getFirstname(),
                        "email"                     => $this->customerNotConfirmedExample->getEmail(),
                        "roles"                     => $this->customerNotConfirmedExample->getRoles(),
                        'confirmedAccount'          => $this->customerNotConfirmedExample->hasConfirmedAccount(),
                        'active'                    => true,
                        'deleted'                   => $this->customerNotConfirmedExample->isDeleted(),
                    ]
                );

                // Possibilité de créer des tokens d'authentification
                $this->getTokenForUser($this->customerNotConfirmedExample->getUserIdentifier(), $this->customerNotConfirmedExamplePassword);

                // Le token est de nouveau utilisable
                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->customerNotConfirmedExampleItemIRI,
                    token: $this->customerNotConfirmedExampleToken
                );

                $this->assertSuccess();

            },
            method: self::PATCH_METHOD,
            url: $customerActiveRoute,
            json: [
                'active' => false,
            ],
            dataToSendToFunctionAssertAhtorizeUser: compact(
                'customerActiveRoute',
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     */
    public function testEmailAlreadyUsed()
    {
        $testUser = $this->getValidTestCustomerInJSON();

        // Création du compte
        $this->doRequest(
            method: self::POST_METHOD,
            url: self::CUSTOMER_ROUTE,
            json: $testUser
        );

        $this->assertSuccess();

        // Tentative de création du compte avec le même mail
        $responseDuplicateAccountEmail = $this->doRequest(
            method: self::POST_METHOD,
            url: self::CUSTOMER_ROUTE,
            json: $testUser
        );

        $violations = ['email'];

        $this->assertViolations($violations, $responseDuplicateAccountEmail);
    }

    /**
     * @throws Exception
     * @throws ExceptionInterface
     */
    public static function getValidTestCustomerInJSON(): array
    {
        $customer = self::getValidTestCustomer();

        return self::getJSONFromCustomer($customer);
    }

    /**
     * @throws Exception
     */
    public static function getValidTestCustomer(): Customer
    {
        $customer = new Customer();
        $customer->setUuid(Uuid::v4());
        $customer->setPassword("aaAA&&12");
        $customer->setLastname("CLIENT");
        $customer->setFirstname("Test");
        $customer->setEmail(self::getTestEmail());

        $birthdate = new DateTime('-18 years');
        $birthdate->setTime(0, 0);
        $customer->setBirthdate($birthdate);

        return $customer;
    }

    /**
     * @throws ExceptionInterface
     */
    public static function getJSONFromCustomer(Customer $customer): array
    {
        $data = self::parseEntityToArray($customer);
        $data['password'] = $customer->getPassword() ?? $customer->getPlainPassword();
        unset($data['uuid']);

        return $data;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testEmailAlreadyUsedOnUpdate()
    {
        $json = [
            "email" => $this->customerConfirmedExample->getEmail(),
        ];

        // Tentative de création du compte avec le même mail
        $responseDuplicateAccountEmail = $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->customerNotConfirmedExampleItemIRI,
            json: $json,
            token: $this->customerNotConfirmedExampleToken
        );

        $violations = ['email'];

        $this->assertViolations($violations, $responseDuplicateAccountEmail);
    }

}
