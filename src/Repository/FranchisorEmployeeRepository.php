<?php

namespace App\Repository;

use App\Entity\User\Employee\FranchisorEmployee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FranchisorEmployee|null find($id, $lockMode = null, $lockVersion = null)
 * @method FranchisorEmployee|null findOneBy(array $criteria, array $orderBy = null)
 * @method FranchisorEmployee[]    findAll()
 * @method FranchisorEmployee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FranchisorEmployeeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FranchisorEmployee::class);
    }

    // /**
    //  * @return FranchisorEmployee[] Returns an array of FranchisorEmployee objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FranchisorEmployee
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
