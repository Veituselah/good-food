<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220112093336 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE dessert (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference))');
        $this->addSql('CREATE TABLE dish (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference))');
        $this->addSql('CREATE TABLE drink (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference))');
        $this->addSql('CREATE TABLE entree (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference))');
        $this->addSql('CREATE TABLE food (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference))');
        $this->addSql('CREATE TABLE menu (reference VARCHAR(15) NOT NULL, PRIMARY KEY(reference))');
        $this->addSql('CREATE TABLE dessert_menu_choice (menu VARCHAR(15) NOT NULL, dessert VARCHAR(15) NOT NULL, PRIMARY KEY(menu, dessert))');
        $this->addSql('CREATE INDEX IDX_AD2C89537D053A93 ON dessert_menu_choice (menu)');
        $this->addSql('CREATE INDEX IDX_AD2C895379291B96 ON dessert_menu_choice (dessert)');
        $this->addSql('CREATE TABLE dish_menu_choice (menu VARCHAR(15) NOT NULL, dish VARCHAR(15) NOT NULL, PRIMARY KEY(menu, dish))');
        $this->addSql('CREATE INDEX IDX_C0FD31AF7D053A93 ON dish_menu_choice (menu)');
        $this->addSql('CREATE INDEX IDX_C0FD31AF957D8CB8 ON dish_menu_choice (dish)');
        $this->addSql('CREATE TABLE drink_menu_choice (menu VARCHAR(15) NOT NULL, drink VARCHAR(15) NOT NULL, PRIMARY KEY(menu, drink))');
        $this->addSql('CREATE INDEX IDX_3C708C057D053A93 ON drink_menu_choice (menu)');
        $this->addSql('CREATE INDEX IDX_3C708C05DBE40D1 ON drink_menu_choice (drink)');
        $this->addSql('CREATE TABLE entree_menu_choice (menu VARCHAR(15) NOT NULL, entree VARCHAR(15) NOT NULL, PRIMARY KEY(menu, entree))');
        $this->addSql('CREATE INDEX IDX_DF40A3137D053A93 ON entree_menu_choice (menu)');
        $this->addSql('CREATE INDEX IDX_DF40A313598377A6 ON entree_menu_choice (entree)');
        $this->addSql('CREATE TABLE product (reference VARCHAR(15) NOT NULL, depth DOUBLE PRECISION DEFAULT NULL, ean13 VARCHAR(13) DEFAULT NULL, height DOUBLE PRECISION DEFAULT NULL, name VARCHAR(50) NOT NULL, slug VARCHAR(50) NOT NULL, upc VARCHAR(12) DEFAULT NULL, weight DOUBLE PRECISION DEFAULT NULL, width DOUBLE PRECISION DEFAULT NULL, description TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, activated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(reference))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D34A04AD989D9B62 ON product (slug)');
        $this->addSql('COMMENT ON COLUMN product.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN product.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN product.activated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN product.deleted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE product_in_restaurant (product VARCHAR(15) NOT NULL, restaurant VARCHAR(255) NOT NULL, PRIMARY KEY(product, restaurant))');
        $this->addSql('CREATE INDEX IDX_7EC5667BD34A04AD ON product_in_restaurant (product)');
        $this->addSql('CREATE INDEX IDX_7EC5667BEB95123F ON product_in_restaurant (restaurant)');
        $this->addSql('CREATE TABLE purchasable_product (reference VARCHAR(15) NOT NULL, currency_id VARCHAR(10) NOT NULL, available_from TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, available_to TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, ecotax DOUBLE PRECISION NOT NULL, min_qty_in_stock_to_sell INT NOT NULL, on_sale BOOLEAN NOT NULL, price_tax_ecluded DOUBLE PRECISION NOT NULL, PRIMARY KEY(reference))');
        $this->addSql('CREATE INDEX IDX_104E187438248176 ON purchasable_product (currency_id)');
        $this->addSql('ALTER TABLE dessert ADD CONSTRAINT FK_79291B96AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dish ADD CONSTRAINT FK_957D8CB8AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE drink ADD CONSTRAINT FK_DBE40D1AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE entree ADD CONSTRAINT FK_598377A6AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE food ADD CONSTRAINT FK_D43829F7AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE menu ADD CONSTRAINT FK_7D053A93AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dessert_menu_choice ADD CONSTRAINT FK_AD2C89537D053A93 FOREIGN KEY (menu) REFERENCES menu (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dessert_menu_choice ADD CONSTRAINT FK_AD2C895379291B96 FOREIGN KEY (dessert) REFERENCES dessert (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dish_menu_choice ADD CONSTRAINT FK_C0FD31AF7D053A93 FOREIGN KEY (menu) REFERENCES menu (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE dish_menu_choice ADD CONSTRAINT FK_C0FD31AF957D8CB8 FOREIGN KEY (dish) REFERENCES dish (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE drink_menu_choice ADD CONSTRAINT FK_3C708C057D053A93 FOREIGN KEY (menu) REFERENCES menu (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE drink_menu_choice ADD CONSTRAINT FK_3C708C05DBE40D1 FOREIGN KEY (drink) REFERENCES drink (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE entree_menu_choice ADD CONSTRAINT FK_DF40A3137D053A93 FOREIGN KEY (menu) REFERENCES menu (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE entree_menu_choice ADD CONSTRAINT FK_DF40A313598377A6 FOREIGN KEY (entree) REFERENCES entree (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_in_restaurant ADD CONSTRAINT FK_7EC5667BD34A04AD FOREIGN KEY (product) REFERENCES product (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product_in_restaurant ADD CONSTRAINT FK_7EC5667BEB95123F FOREIGN KEY (restaurant) REFERENCES restaurant (slug) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE purchasable_product ADD CONSTRAINT FK_104E187438248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE purchasable_product ADD CONSTRAINT FK_104E1874AEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE tax ALTER rate TYPE NUMERIC(10, 0)');
        $this->addSql('ALTER TABLE tax ALTER rate DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE dessert_menu_choice DROP CONSTRAINT FK_AD2C895379291B96');
        $this->addSql('ALTER TABLE dish_menu_choice DROP CONSTRAINT FK_C0FD31AF957D8CB8');
        $this->addSql('ALTER TABLE drink_menu_choice DROP CONSTRAINT FK_3C708C05DBE40D1');
        $this->addSql('ALTER TABLE entree_menu_choice DROP CONSTRAINT FK_DF40A313598377A6');
        $this->addSql('ALTER TABLE dessert_menu_choice DROP CONSTRAINT FK_AD2C89537D053A93');
        $this->addSql('ALTER TABLE dish_menu_choice DROP CONSTRAINT FK_C0FD31AF7D053A93');
        $this->addSql('ALTER TABLE drink_menu_choice DROP CONSTRAINT FK_3C708C057D053A93');
        $this->addSql('ALTER TABLE entree_menu_choice DROP CONSTRAINT FK_DF40A3137D053A93');
        $this->addSql('ALTER TABLE dessert DROP CONSTRAINT FK_79291B96AEA34913');
        $this->addSql('ALTER TABLE dish DROP CONSTRAINT FK_957D8CB8AEA34913');
        $this->addSql('ALTER TABLE drink DROP CONSTRAINT FK_DBE40D1AEA34913');
        $this->addSql('ALTER TABLE entree DROP CONSTRAINT FK_598377A6AEA34913');
        $this->addSql('ALTER TABLE food DROP CONSTRAINT FK_D43829F7AEA34913');
        $this->addSql('ALTER TABLE menu DROP CONSTRAINT FK_7D053A93AEA34913');
        $this->addSql('ALTER TABLE product_in_restaurant DROP CONSTRAINT FK_7EC5667BD34A04AD');
        $this->addSql('ALTER TABLE purchasable_product DROP CONSTRAINT FK_104E1874AEA34913');
        $this->addSql('DROP TABLE dessert');
        $this->addSql('DROP TABLE dish');
        $this->addSql('DROP TABLE drink');
        $this->addSql('DROP TABLE entree');
        $this->addSql('DROP TABLE food');
        $this->addSql('DROP TABLE menu');
        $this->addSql('DROP TABLE dessert_menu_choice');
        $this->addSql('DROP TABLE dish_menu_choice');
        $this->addSql('DROP TABLE drink_menu_choice');
        $this->addSql('DROP TABLE entree_menu_choice');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_in_restaurant');
        $this->addSql('DROP TABLE purchasable_product');
        $this->addSql('ALTER TABLE tax ALTER rate TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE tax ALTER rate DROP DEFAULT');
    }
}
