<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220123185900 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE carrier_data_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE carrier (reference VARCHAR(10) NOT NULL, name VARCHAR(30) NOT NULL, site_url VARCHAR(255) DEFAULT NULL, activated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(reference))');
        $this->addSql('COMMENT ON COLUMN carrier.activated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN carrier.deleted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE carrier_data (id INT NOT NULL, carrier_id VARCHAR(10) NOT NULL, currency_id VARCHAR(10) NOT NULL, tax_id INT NOT NULL, delay VARCHAR(30) NOT NULL, shipping_cost DOUBLE PRECISION DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FF9E8FE421DFC797 ON carrier_data (carrier_id)');
        $this->addSql('CREATE INDEX IDX_FF9E8FE438248176 ON carrier_data (currency_id)');
        $this->addSql('CREATE INDEX IDX_FF9E8FE4B2A824D8 ON carrier_data (tax_id)');
        $this->addSql('COMMENT ON COLUMN carrier_data.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE carrier_data ADD CONSTRAINT FK_FF9E8FE421DFC797 FOREIGN KEY (carrier_id) REFERENCES carrier (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE carrier_data ADD CONSTRAINT FK_FF9E8FE438248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE carrier_data ADD CONSTRAINT FK_FF9E8FE4B2A824D8 FOREIGN KEY (tax_id) REFERENCES tax_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE carrier_data DROP CONSTRAINT FK_FF9E8FE421DFC797');
        $this->addSql('DROP SEQUENCE carrier_data_id_seq CASCADE');
        $this->addSql('DROP TABLE carrier');
        $this->addSql('DROP TABLE carrier_data');
    }
}
