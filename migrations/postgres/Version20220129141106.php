<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220129141106 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE order_line_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE order_line (id INT NOT NULL, currency_id VARCHAR(10) NOT NULL, order_reference_id VARCHAR(15) NOT NULL, product_id VARCHAR(15) NOT NULL, commentary TEXT DEFAULT NULL, depth DOUBLE PRECISION DEFAULT NULL, description TEXT DEFAULT NULL, ean13 VARCHAR(13) DEFAULT NULL, height DOUBLE PRECISION DEFAULT NULL, name VARCHAR(50) NOT NULL, qty INT NOT NULL, tax_amount DOUBLE PRECISION NOT NULL, unit_price_tax_excluded DOUBLE PRECISION NOT NULL, upc VARCHAR(12) DEFAULT NULL, weight DOUBLE PRECISION DEFAULT NULL, width DOUBLE PRECISION DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9CE58EE138248176 ON order_line (currency_id)');
        $this->addSql('CREATE INDEX IDX_9CE58EE112854AC3 ON order_line (order_reference_id)');
        $this->addSql('CREATE INDEX IDX_9CE58EE14584665A ON order_line (product_id)');
        $this->addSql('COMMENT ON COLUMN order_line.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN order_line.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE order_line_extra_value (order_line_id INT NOT NULL, extra_value_id INT NOT NULL, PRIMARY KEY(order_line_id, extra_value_id))');
        $this->addSql('CREATE INDEX IDX_B29C8C6BBB01DC09 ON order_line_extra_value (order_line_id)');
        $this->addSql('CREATE INDEX IDX_B29C8C6BB7FCE3F1 ON order_line_extra_value (extra_value_id)');
        $this->addSql('CREATE TABLE order_line_food (order_line_id INT NOT NULL, food_reference VARCHAR(15) NOT NULL, PRIMARY KEY(order_line_id, food_reference))');
        $this->addSql('CREATE INDEX IDX_E3F2A1D6BB01DC09 ON order_line_food (order_line_id)');
        $this->addSql('CREATE INDEX IDX_E3F2A1D636592488 ON order_line_food (food_reference)');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE138248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE112854AC3 FOREIGN KEY (order_reference_id) REFERENCES "order" (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE14584665A FOREIGN KEY (product_id) REFERENCES purchasable_product (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_line_extra_value ADD CONSTRAINT FK_B29C8C6BBB01DC09 FOREIGN KEY (order_line_id) REFERENCES order_line (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_line_extra_value ADD CONSTRAINT FK_B29C8C6BB7FCE3F1 FOREIGN KEY (extra_value_id) REFERENCES extra_value (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_line_food ADD CONSTRAINT FK_E3F2A1D6BB01DC09 FOREIGN KEY (order_line_id) REFERENCES order_line (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_line_food ADD CONSTRAINT FK_E3F2A1D636592488 FOREIGN KEY (food_reference) REFERENCES food (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE order_line_extra_value DROP CONSTRAINT FK_B29C8C6BBB01DC09');
        $this->addSql('ALTER TABLE order_line_food DROP CONSTRAINT FK_E3F2A1D6BB01DC09');
        $this->addSql('DROP SEQUENCE order_line_id_seq CASCADE');
        $this->addSql('DROP TABLE order_line');
        $this->addSql('DROP TABLE order_line_extra_value');
        $this->addSql('DROP TABLE order_line_food');
    }
}
