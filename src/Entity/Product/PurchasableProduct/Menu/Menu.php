<?php

namespace App\Entity\Product\PurchasableProduct\Menu;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Product\PurchasableProduct\Food\Dessert;
use App\Entity\Product\PurchasableProduct\Food\Dish;
use App\Entity\Product\PurchasableProduct\Food\Drink;
use App\Entity\Product\PurchasableProduct\Food\Entree;
use App\Entity\Product\PurchasableProduct\PurchasableProduct;
use App\Filter\Product\SearchByNameOrReference;
use App\Repository\MenuRepository;
use App\Voter\ProductVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\Table;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;


#[Entity(repositoryClass: MenuRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get'  => [],
        'post' => [
            // Seulement les editeurs et + ou si il n'y a qu'un seul restaurant pour le produit et que ce restaurant et celui de l'employé franchisé connecté
            'security'                => 'is_granted("' . ProductVoter::IS_AUTHORIZED_TO_MANAGE_PRODUCT . '", object)',
            'denormalization_context' => [
                'groups' => [
                    "product", "product:read", "product:post",
                    "purchasable_product", "purchasable_product:read",
                    "menu", "menu:read",
                ],
            ],
        ],
    ],
    itemOperations: [
        'get'    => [],
        'patch'  => [
            // Seulement les editeurs et + ou si il n'y a qu'un seul restaurant pour le produit et que ce restaurant et celui de l'employé franchisé connecté
            'security' => 'is_granted("' . ProductVoter::IS_AUTHORIZED_TO_MANAGE_PRODUCT . '", object)',
        ],
        'delete' => [
            // Seulement les editeurs et + ou si il n'y a qu'un seul restaurant pour le produit et que ce restaurant et celui de l'employé franchisé connecté
            'security' => 'is_granted("' . ProductVoter::IS_AUTHORIZED_TO_MANAGE_PRODUCT . '", object)',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "product", "product:write",
            "purchasable_product", "purchasable_product:write",
            "menu", "menu:write",
        ],
    ],
    normalizationContext: [
        'groups'           => [
            "product", "product:read",
            "purchasable_product", "purchasable_product:read",
            "menu", "menu:read",
        ],
        "enable_max_depth" => true,
    ]
)]
#[ApiFilter(SearchByNameOrReference::class, properties: ['search_by_name_or_reference'])]
class Menu extends PurchasableProduct
{

    /**
     * @var Dessert[]|Collection
     */
    #[ManyToMany(targetEntity: Dessert::class, inversedBy: "useInMenus")]
    #[JoinTable(name: "dessert_menu_choice")]
    #[JoinColumn(
        name: "menu",
        referencedColumnName: "reference"
    )]
    #[InverseJoinColumn(
        name: "dessert",
        referencedColumnName: "reference"
    )]
    #[Groups('menu')]
    #[ApiProperty(readableLink: false)]
    private array|Collection $dessertChoices;

    /**
     * @var Dish[]|Collection
     */
    #[ManyToMany(targetEntity: Dish::class, inversedBy: "useInMenus")]
    #[JoinTable(name: "dish_menu_choice")]
    #[JoinColumn(
        name: "menu",
        referencedColumnName: "reference"
    )]
    #[InverseJoinColumn(
        name: "dish",
        referencedColumnName: "reference"
    )]
    #[Groups('menu')]
    #[ApiProperty(readableLink: false)]
    private array|Collection $dishChoices;

    /**
     * @var Drink[]|Collection
     */
    #[ManyToMany(targetEntity: Drink::class, inversedBy: "useInMenus")]
    #[JoinTable(name: "drink_menu_choice")]
    #[JoinColumn(
        name: "menu",
        referencedColumnName: "reference"
    )]
    #[InverseJoinColumn(
        name: "drink",
        referencedColumnName: "reference"
    )]
    #[Groups('menu')]
    #[ApiProperty(readableLink: false)]
    private array|Collection $drinkChoices;

    /**
     * @var Entree[]|Collection
     */
    #[ManyToMany(targetEntity: Entree::class, inversedBy: "useInMenus")]
    #[JoinTable(name: "entree_menu_choice")]
    #[JoinColumn(
        name: "menu",
        referencedColumnName: "reference"
    )]
    #[InverseJoinColumn(
        name: "entree",
        referencedColumnName: "reference"
    )]
    #[Groups('menu')]
    #[ApiProperty(readableLink: false)]
    private array|Collection $entreeChoices;

    #[Pure] public function __construct()
    {
        parent::__construct();
        $this->drinkChoices = new ArrayCollection();
        $this->dessertChoices = new ArrayCollection();
        $this->entreeChoices = new ArrayCollection();
        $this->dishChoices = new ArrayCollection();
    }

    public function addDessertChoice(Dessert $dessertChoice): self
    {
        if (!$this->dessertChoices->contains($dessertChoice)) {
            $this->dessertChoices[] = $dessertChoice;
        }

        return $this;
    }

    public function addDishChoice(Dish $dishChoice): self
    {
        if (!$this->dishChoices->contains($dishChoice)) {
            $this->dishChoices[] = $dishChoice;
        }

        return $this;
    }

    public function addDrinkChoice(Drink $drinkChoice): self
    {
        if (!$this->drinkChoices->contains($drinkChoice)) {
            $this->drinkChoices[] = $drinkChoice;
        }

        return $this;
    }

    public function addEntreeChoice(Entree $entreeChoice): self
    {
        if (!$this->entreeChoices->contains($entreeChoice)) {
            $this->entreeChoices[] = $entreeChoice;
        }

        return $this;
    }

    /**
     * @return Dessert[]|Collection
     */
    public function getDessertChoices(): array|Collection
    {
        return $this->dessertChoices;
    }

    /**
     * @return Dish[]|Collection
     */
    public function getDishChoices(): array|Collection
    {
        return $this->dishChoices;
    }

    /**
     * @return Drink[]|Collection
     */
    public function getDrinkChoices(): array|Collection
    {
        return $this->drinkChoices;
    }

    /**
     * @return Entree[]|Collection
     */
    public function getEntreeChoices(): array|Collection
    {
        return $this->entreeChoices;
    }

    public function removeDessertChoice(Dessert $dessertChoice): self
    {
        $this->dessertChoices->removeElement($dessertChoice);

        return $this;
    }

    public function removeDishChoice(Dish $dishChoice): self
    {
        $this->dishChoices->removeElement($dishChoice);

        return $this;
    }

    public function removeDrinkChoice(Drink $drinkChoice): self
    {
        $this->drinkChoices->removeElement($drinkChoice);

        return $this;
    }

    public function removeEntreeChoice(Entree $entreeChoice): self
    {
        $this->entreeChoices->removeElement($entreeChoice);

        return $this;
    }

    public function getEntityName(): string
    {
        return "Menu";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "menus";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get",
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    #[Groups(['purchasable_product:read'])]
    public function getMainCategory(): string
    {
        return 'Menu';
    }

}
