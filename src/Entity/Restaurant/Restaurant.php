<?php

namespace App\Entity\Restaurant;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Restaurant\ListCustomersInOrdersAction;
use App\Controller\Restaurant\ListCustomersInReservationsAction;
use App\Controller\Restaurant\ListRestaurantsInOrdersAction;
use App\Controller\Restaurant\ListRestaurantsInReservationsAction;
use App\Dto\RestaurantLocationOutput;
use App\Entity\Country\Currency;
use App\Entity\Media\Media;
use App\Entity\Order\Order;
use App\Entity\Product\Product;
use App\Entity\Product\PurchasableProduct\Food\Extra\ExtraGroup;
use App\Entity\Reservation;
use App\Entity\User\Comment\Comment;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Filter\Restaurant\SearchByNameOrCity;
use App\Interfaces\TestableEntityInterface;
use App\Repository\RestaurantRepository;
use App\Traits\Fields\AddressTrait;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use App\Traits\Fields\LocationTrait;
use App\Voter\RestaurantVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


#[Entity(repositoryClass: RestaurantRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        //	Tout le monde peut récupérer la liste des restaurants
        "get"                            => [],
        // Seuls les admins ou + peuvent créer un restaurant
        "post"                           => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
        "get_restaurant_around_location" => [
            'route_name' => 'get_restaurant_around_location',
            "output"     => RestaurantLocationOutput::class,
        ],
        "in_orders" => [
            "method"             => "get",
            "path"               => "/restaurants/in_orders",
            "controller"         => ListRestaurantsInOrdersAction::class,
            "pagination_enabled" => false,
        ],
        "in_reservations" => [
            "method"             => "get",
            "path"               => "/restaurants/in_reservations",
            "controller"         => ListRestaurantsInReservationsAction::class,
            "pagination_enabled" => false,
        ],
    ],
    itemOperations: [
        //	Tout le monde peut récupérer la liste des restaurants
        "get"    => [],
        // Seuls le chef du restaurant ou les admins ou + peuvent modifier un restaurant
        "patch"  => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "') or is_granted('" . RestaurantVoter::GRANT_USER_IS_LEADER . "', object)",
        ],
        // Seuls les admins ou + peuvent supprimer un restaurant
        "delete" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    subresourceOperations: [
        "api_restaurants_employees_get_subresource"                 => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
        'api_restaurants_extra_groups_get_subresource'              => [
            'security' => 'is_granted("' . FranchisorEmployee::ROLE_ADMIN . '") or (is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") and object.restaurant = user.restaurant)',
        ],
        'api_restaurants_extra_groups_extra_values_get_subresource' => [
            'security' => 'is_granted("' . FranchisorEmployee::ROLE_ADMIN . '") or (is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") and object.restaurant = user.restaurant)',
        ],
        'api_restaurants_orders_get_subresource'                    => [
            'security' => 'is_granted("' . FranchisorEmployee::ROLE_ADMIN . '") or (is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") and object.restaurant = user.restaurant)',
        ],
        'api_restaurants_products_get_subresource'                  => [
            'security' => 'is_granted("' . FranchisorEmployee::ROLE_ADMIN . '") or (is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") and object.restaurant = user.restaurant)',
        ],

    ],
    denormalizationContext: [
        "groups" => [
            "restaurant", "restaurant:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "restaurant", "restaurant:read",
        ],
    ],
)]
#[ApiFilter(
    OrderFilter::class,
    properties: [
        'name',
        'phoneNumber',
        'country.name',
        'defaultCurrency.name',
        'createdAt',
        'updatedAt',
    ]
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'name'                            => 'partial',
        'phoneNumber'                     => 'partial',
        'country.isoCode'                 => 'exact',
        'country.defaultCurrency.isoCode' => 'exact',
    ]
)]
#[ApiFilter(SearchByNameOrCity::class, properties: ['search_by_name_or_city'])]
class Restaurant implements TestableEntityInterface
{

    use SlugNameTrait;
    use LocationTrait;
    use AddressTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[Column(type: "text", nullable: true)]
    #[Groups([
        "restaurant",
        'product:read',
        'franchise_employee:read',
        'comment:read',
        'order:read',
        'reservation:read'
    ])]
    private ?string $description;

    /**
     * @var FranchisorEmployee[]|Collection
     */
    #[OneToMany(mappedBy: "restaurant", targetEntity: FranchiseEmployee::class)]
    #[ApiSubresource]
    private array|Collection $employees;

    /**
     * @var ExtraGroup[]|Collection
     */
    #[ManyToMany(targetEntity: ExtraGroup::class, mappedBy: 'onlyInRestaurants')]
    #[ApiSubresource]
    private array|Collection $extraGroups;

    #[ManyToOne(targetEntity: Media::class)]
    #[Groups([
        'restaurant',
        'product:read',
        'franchise_employee:read',
        'comment:read',
        'order:read',
        'reservation:read'
    ])]
    private ?Media $image;

    #[Column(type: "string", length: 50)]
    #[NotBlank(message: "Le nom du restaurant doit être défini.")]
    #[Length(
        min: 2,
        max: 50,
        minMessage: "Le nom du restaurant doit faire plus de {{ limit }} caractères.",
        maxMessage: "Le nom du restaurant doit faire moins de {{ limit }} caractères."
    )]
    #[Groups([
        'restaurant',
        'product:read',
        'franchise_employee:read',
        'comment:read',
        'order:read',
        'reservation:read'
    ])]
    private string $name;

    /**
     * @var Order[]|Collection
     */
    #[OneToMany(mappedBy: 'restaurant', targetEntity: Order::class)]
    #[ApiSubresource]
    private array|Collection $orders;

    /**
     * @var Product[]|Collection
     */
    #[ManyToMany(targetEntity: Product::class, mappedBy: "onlyInRestaurants")]
    #[ApiSubresource]
    private array|Collection $products;

    /**
     * @var Comment[]|Collection
     */
    #[OneToMany(mappedBy: "restaurant", targetEntity: Comment::class)]
    #[ApiSubresource]
    #[ApiProperty(readableLink: false)]
    #[Groups('restaurant:read')]
    private array|Collection $comments;

    #[OneToMany(mappedBy: 'restaurant', targetEntity: Reservation::class)]
    private $reservations;

    public function __construct()
    {
        $this->employees = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->extraGroups = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->reservations = new ArrayCollection();
    }

    public function getComments(): array|Collection
    {
        return $this->comments;
    }

    public function addEmployees(FranchiseEmployee $employees): self
    {
        if (!$this->employees->contains($employees)) {
            $this->employees[] = $employees;
            $employees->setRestaurant($this);
        }

        return $this;
    }

    public function addExtraGroup(ExtraGroup $extraGroup): self
    {
        if (!$this->extraGroups->contains($extraGroup)) {
            $this->extraGroups[] = $extraGroup;
            $extraGroup->addOnlyInRestaurant($this);
        }

        return $this;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setRestaurant($this);
        }

        return $this;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->addOnlyInRestaurant($this);
        }

        return $this;
    }

    function autoActiveOnCreation(): bool
    {
        return false;
    }

    #[Groups([
        'restaurant:read',
        'product:read',
        'franchise_employee:read',
        'comment:read',
        'order:read',
        'reservation:read'
    ])]
    public function getDefaultCurrency(): ?Currency
    {
        return $this?->country?->getDefaultCurrency();
    }

    public function getDescription(): ?string
    {
        return $this->description ?? null;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return FranchiseEmployee[]|Collection
     */
    public function getEmployees(): array|Collection
    {
        return $this->employees;
    }

    /**
     * @return Collection|ExtraGroup[]
     */
    public function getExtraGroups(): array|Collection
    {
        return $this->extraGroups;
    }

    public function getImage(): ?Media
    {
        return $this->image;
    }

    public function setImage(?Media $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return FranchiseEmployee[]|Collection
     */
    #[Groups('restaurant:read')]
    public function getLeaders(): array|Collection
    {
        $leaders = new ArrayCollection();

        foreach ($this->employees as $employee) {
            if (in_array(FranchiseEmployee::ROLE_LEAD, $employee->getRoles())) {
                $leaders->add($employee);
            }
        }

        return $leaders;
    }

    public function getName(): ?string
    {
        return $this?->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): array|Collection
    {
        return $this->orders;
    }

    /**
     * @return Product[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function removeEmployees(FranchiseEmployee $employees): self
    {
        if ($this->employees->removeElement($employees)) {
            // set the owning side to null (unless already changed)
            if ($employees->getRestaurant() === $this) {
                $employees->setRestaurant(null);
            }
        }

        return $this;
    }

    public function removeExtraGroup(ExtraGroup $extraGroup): self
    {
        if ($this->extraGroups->removeElement($extraGroup)) {
            $extraGroup->removeOnlyInRestaurant($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getRestaurant() === $this) {
                $order->setRestaurant(null);
            }
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            $product->removeOnlyInRestaurant($this);
        }

        return $this;
    }

    public function getEntityName(): string
    {
        return "Restaurant";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [
            "api_restaurants_employees_get_subresource"                 => "Récupère les employés associés au restaurant",
            "api_restaurants_extra_groups_get_subresource"              => "Récupère les groupes d'extras associés au restaurant",
            "api_restaurants_extra_groups_extra_values_get_subresource" => "Récupère les valeurs d'extras associés au restaurant",
            "api_restaurants_orders_get_subresource"                    => "Récupère les commandes associées au restaurant",
            "api_restaurants_products_get_subresource"                  => "Récupère les produits associés au restaurant",
        ];
    }

    public function getEntityRouteName(): string
    {
        return "restaurants";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get",
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    #[Groups('restaurant:read')]
    public function getMeanNote(): int
    {
        if ($this->comments->count() < 1) {
            return 0;
        }

        $total = 0;

        foreach ($this->comments as $comment) {
            $total += $comment->getNote();
        }

        return $total / $this->comments->count();
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setRestaurant($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getRestaurant() === $this) {
                $reservation->setRestaurant(null);
            }
        }

        return $this;
    }

}
