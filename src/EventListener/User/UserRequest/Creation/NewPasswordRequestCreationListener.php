<?php


namespace App\EventListener\User\UserRequest\Creation;


use App\Entity\User\UserRequest\NewPasswordRequest;
use App\Service\Tools\SendMail;
use App\Service\Tools\Tools;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class NewPasswordRequestCreationListener
{

    public function __construct(
        private SendMail $mailer,
        private Tools $tools
    )
    {
    }

    /**
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function postPersist(NewPasswordRequest $userRequest, LifecycleEventArgs $event)
    {
        $this->mailer->sendMail(
            'email/reset_password.html.twig',
            [
                "user" => $userRequest->getUserTarget(),
                "routeResetPassword" => $this->tools->webSiteResetPasswordUrl . '/' . $userRequest->getId()
            ],
            'Réinitialisation de votre mot de passe',
            null,
            [
                $userRequest->getUserTarget()->getEmail()
            ]
        );
    }
}