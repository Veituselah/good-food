<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\User\Customer\Customer;
use App\Tests\unit\CustomerTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

trait CustomerConfirmedExampleTrait
{
    use UserExampleTrait;

    private ?string $customerConfirmedExampleIndex;
    private ?Customer $customerConfirmedExample;
    private ?string $customerConfirmedExampleUsername;
    private ?string $customerConfirmedExamplePassword;
    private ?string $customerConfirmedExampleToken;
    private ?string $customerConfirmedExampleItemIRI;


    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function loadCustomerConfirmed()
    {
        $this->customerConfirmedExampleIndex = CustomApiTestCase::ROLE_CUSTOMER_CONFIRMED;
        $this->customerConfirmedExample = CustomApiTestCase::getUserFixturesClean()[$this->customerConfirmedExampleIndex];
        $this->customerConfirmedExampleUsername = $this->customerConfirmedExample->getUserIdentifier();
        $this->customerConfirmedExamplePassword = CustomApiTestCase::PASSWORDS[$this->customerConfirmedExampleIndex];
        $this->customerConfirmedExampleToken = $this->getTokenForUser($this->customerConfirmedExampleUsername, $this->customerConfirmedExamplePassword);
        $this->customerConfirmedExampleItemIRI = CustomApiTestCase::replaceParamsRoute(CustomerTest::CUSTOMER_ITEM_ROUTE, ['uuid' => $this->customerConfirmedExample->getUuid()]);
    }
}