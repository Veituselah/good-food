<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\Country\Country;
use App\Entity\Restaurant\Restaurant;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\AdminExampleTrait;
use App\Traits\Tests\CountryExampleTrait;
use App\Traits\Tests\RestaurantExampleTrait;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class RestaurantTest extends CrudApiTestCase
{

    use AdminExampleTrait;
    use RestaurantExampleTrait;
    use CountryExampleTrait;

    // Fixtures
    const RESTAURANT_FIXTURES_FILES = 'restaurant.yml';

    // Routes
    const RESTAURANT_ROUTE = self::API_ROUTE . '/restaurants';
    const RESTAURANT_ITEM_ROUTE = self::RESTAURANT_ROUTE . '/{slug}';
    const RESTAURANT_ACTIVE_ROUTE = self::RESTAURANT_ITEM_ROUTE . '/active';
    const RESTAURANT_EMPLOYEES_ROUTE = self::RESTAURANT_ITEM_ROUTE . '/employees';

    // JSON LD Data
    const RESTAURANT_TYPE = 'Restaurant';
    const RESTAURANT_CONTEXT = self::CONTEXT . '/' . self::RESTAURANT_TYPE;

    // Data
    const ALL_FIELDS_VIOLATION = ['name', 'longitude', 'latitude', 'street', 'zipCode', 'city', 'phoneNumber', 'country'];
    const RESTAURANT_EXAMPLE = "restaurant_france";

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAdmin();
        $this->loadRestaurant();
        $this->loadCountry();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     */
    public function testPostInvalidItem()
    {
        $validRestaurant = self::getValidTestRestaurantInJSON(
            $this->countryExample
        );

        // Empty request error
        $requestEmptyData = $this->doRequest(
            method: self::POST_METHOD,
            url: self::RESTAURANT_ROUTE,
            json: [],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestEmptyData);

        // Min carac error

        $requestNotRespectMinValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::RESTAURANT_ROUTE,
            json: [
                'name'        => "",
                'longitude'   => -181,
                'latitude'    => -91,
                'street'      => "",
                'zipCode'     => "",
                'city'        => "",
                'phoneNumber' => "",
            ],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac error

        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::RESTAURANT_ROUTE,
            json: [
                'name'        => self::STRING_LENGTH_256,
                'longitude'   => 181,
                'latitude'    => 91,
                'street'      => self::STRING_LENGTH_256,
                'zipCode'     => self::STRING_LENGTH_256,
                'city'        => self::STRING_LENGTH_256,
                'phoneNumber' => self::STRING_LENGTH_256,
            ],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMaxValidation);

        // Numéro de téléphone incorrect

        $data = self::getValidTestRestaurantInJSON($this->countryExample);
        $data['phoneNumber'] = 'azer';

//        Malformatted logicistician uuid error

        $this->doRequest(
            method: self::POST_METHOD,
            url: self::RESTAURANT_ROUTE,
            json: $data,
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMaxValidation);

//        Malformatted country iri error

        $data = $validRestaurant;
        $data['country'] = 'azer';

        $this->doRequest(
            method: self::POST_METHOD,
            url: self::RESTAURANT_ROUTE,
            json: $data,
            token: $this->adminExampleToken
        );

        $this->assertBadRequest();
    }

    /**
     * @throws Exception
     * @throws ExceptionInterface
     */
    public static function getValidTestRestaurantInJSON(
        Country $country
    ): array
    {
        $restaurant = self::getValidTestRestaurant($country);
        $data = self::parseEntityToArray($restaurant);
        $data['country'] = self::replaceParamsRoute(CountryTest::COUNTRY_ITEM_ROUTE, ['isoCode' => $restaurant->getCountry()->getIsoCode()]);
        unset($data['slug']);

        return $data;
    }

    /**
     * @throws Exception
     */
    public static function getValidTestRestaurant(
        Country $country
    ): Restaurant
    {
        $restaurant = new Restaurant();
        $restaurant->setName("Restaurant Test");
        $restaurant->setSlug(self::slugify($restaurant->getName()));
        $restaurant->setLongitude(179.9);
        $restaurant->setLatitude(89.9);
        $restaurant->setStreet("2 test street");
        $restaurant->setZipCode("25000");
        $restaurant->setCity("Paris");
        $restaurant->setPhoneNumber("+33123456789");
        $restaurant->setCountry($country);

        return $restaurant;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function testPostItem()
    {
        $data = $this->getValidTestRestaurantInJSON($this->countryExample);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $validJSON = $json;
                $validJSON['country'] = $this->parseEntityToArray($this->countryExample);

                $restaurantCreated = $response->toArray();

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::RESTAURANT_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::RESTAURANT_TYPE,
                        'deleted'                   => false,
                    ]
                );
                $this->assertJsonContains($validJSON);

                $this->assertIdentifierOfResponseData(self::RESTAURANT_ROUTE, Tools::SLUG_REGEX, $restaurantCreated);
                $this->assertMatchesResourceItemJsonSchema(Restaurant::class);

            },
            method: self::POST_METHOD,
            url: self::RESTAURANT_ROUTE,
            json: $data
        );

    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws Exception
     */
    public function testDeleteItem(): void
    {
        $superAdminToken = $this->getTokenFromUserIndex(self::CEDRIC_ADMIN);
        $nbCustomersInCollectionBefore = $this->getTotalItemsForRequest(self::RESTAURANT_ROUTE, $superAdminToken);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                $this->adminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccessDelete();

                $this->assertNull(self::getEntityManager()->getRepository(Restaurant::class)->findOneBy(['slug' => $this->restaurantExample->getSlug()]));

                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->restaurantExampleItemIRI,
                    token: $superAdminToken,
                );

                $this->assertNotFound();

                $nbCustomersInCollectionAfter = $this->getTotalItemsForRequest(self::RESTAURANT_ROUTE, $superAdminToken);

                $this->assertEquals($nbCustomersInCollectionBefore - 1, $nbCustomersInCollectionAfter);

                $this->initClient();

            },
            method: self::DELETE_METHOD,
            url: $this->restaurantExampleItemIRI,
            dataToSendToFunctionAssertAhtorizeUser: compact('nbCustomersInCollectionBefore', 'superAdminToken')
        );
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::ALL_USERS,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::RESTAURANT_ROUTE,
                    context: self::RESTAURANT_CONTEXT,
                    classToCheckValidity: Restaurant::class,
                    totalItems: count(self::getRestaurantFixtures())
                );
            },
            method: self::GET_METHOD,
            url: self::RESTAURANT_ROUTE,
        );
    }

    /**
     * @return Restaurant[]
     */
    #[Pure] public static function getRestaurantFixtures(): array
    {
        return self::getDataFixturesOfClass(Restaurant::class);
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetItem(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::ALL_USERS,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::RESTAURANT_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::RESTAURANT_TYPE,
                        'active'                    => $this->restaurantExample->isActive(),
                        'deleted'                   => $this->restaurantExample->isDeleted(),
                    ]
                );

                $validJson = self::parseEntityToArray($this->restaurantExample);

                $this->assertJsonContains($validJson);

                $restaurant = $response->toArray();

                $this->assertIdentifierOfResponseData(self::RESTAURANT_ROUTE, Tools::SLUG_REGEX, $restaurant);
                $this->assertMatchesResourceItemJsonSchema(Restaurant::class);

            },
            method: self::GET_METHOD,
            url: $this->restaurantExampleItemIRI,
        );
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function testPatchItem(): void
    {
        $restaurantTest = new Restaurant();
        $restaurantTest->setName("a" . $this->restaurantExample->getName());
        $restaurantTest->setSlug(self::slugify($restaurantTest->getName()));
        $restaurantTest->setLongitude(1 + $this->restaurantExample->getLongitude());
        $restaurantTest->setLatitude(1 + $this->restaurantExample->getLatitude());
        $restaurantTest->setStreet("a" . $this->restaurantExample->getStreet());
        $restaurantTest->setZipCode("a" . $this->restaurantExample->getZipCode());
        $restaurantTest->setCity("a" . $this->restaurantExample->getCity());
        $restaurantTest->setPhoneNumber($this->restaurantExample->getPhoneNumber() . "1");
        $restaurantTest->setDescription("a" . $this->restaurantExample->getDescription());
        $restaurantTest->setAdditionalAddress("a" . $this->restaurantExample->getAdditionalAddress());

        $json = self::parseEntityToArray($restaurantTest);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                self::ROLE_LEAD,
                $this->adminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();
                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::RESTAURANT_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::RESTAURANT_TYPE,
                        'deleted'                   => false,
                        'active'                    => $this->restaurantExample->isActive(),
                    ]
                );
                $this->assertJsonContains($json);

                $restaurant = $response->toArray();

                $this->assertIdentifierOfResponseData(self::RESTAURANT_ROUTE, Tools::SLUG_REGEX, $restaurant);
                $this->assertMatchesResourceItemJsonSchema(Restaurant::class);

                $this->initClient();
            },
            method: self::PATCH_METHOD,
            url: $this->restaurantExampleItemIRI,
            json: $json,
        );
    }

    public function testPatchInvalidItem()
    {
        // TODO: Implement testPatchInvalidItem() method.
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

}
