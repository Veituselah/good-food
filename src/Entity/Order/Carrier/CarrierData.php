<?php

namespace App\Entity\Order\Carrier;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Country\Currency;
use App\Entity\Country\Tax\TaxType;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\CarrierDataRepository;
use App\Service\Tools\Tools;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Identifiers\IDTrait;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[Entity(repositoryClass: CarrierDataRepository::class)]
#[ApiResource(
    collectionOperations: [
        "post" => [
            "security"                => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
            "denormalization_context" => [
                'groups' => [
                    "carrier_data", "carrier_data:write", "carrier_data:post",
                ],
            ],
        ],
    ],
    itemOperations: [
        "get" => [],
    ],
    denormalizationContext: [
        'groups' => [
            "carrier_data", "carrier_data:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "carrier_data", "carrier_data:read",
        ],
    ]
)]
class CarrierData implements TestableEntityInterface
{

    use IDTrait;
    use CreatedAtTrait;

    #[ManyToOne(targetEntity: Carrier::class, inversedBy: 'carrierData')]
    #[JoinColumn(referencedColumnName: "reference", nullable: false)]
    #[NotNull(message: "Le transporteur possédant ces données doit être définie")]
    #[Groups([
        'carrier_data:post',
        'carrier_data:read',
        'carrier:read',
    ])]
    private Carrier $carrier;

    #[ManyToOne(targetEntity: Currency::class)]
    #[JoinColumn(referencedColumnName: "iso_code", nullable: false)]
    #[NotNull(message: "La devise utilisée pour définir le montant des frais de ports doit être définie")]
    #[Groups([
        'carrier_data',
        'carrier:read',
    ])]
    private Currency $currency;

    #[Column(type: "string", length: 30)]
    #[NotBlank(message: "Le délai doit être renseigné. (Ex: 3 jours ouvrés)")]
    #[Length(
        min: 3,
        max: 50,
        minMessage: "Le délai doit faire au moins {{limit}} caractères. (Ex: 3 jours ouvrés)",
        maxMessage: "Le délai doit faire moins de {{limit}} caractères. (Ex: 3 jours ouvrés)"
    )]
    #[Groups([
        'carrier_data',
        'carrier:read',
    ])]
    private string $delay;

    #[Column(type: 'float', nullable: true)]
    #[GreaterThan(value: 0, message: "Le montant des frais de ports doit être null ou supérieur à {{compared_value}}")]
    #[Groups([
        'carrier_data',
        'carrier:read',
    ])]
    private float $shippingCost;

    #[ManyToOne(targetEntity: TaxType::class)]
    #[JoinColumn(nullable: false)]
    #[NotNull(message: "La taxe appliquée pour ce transporteur doit être définie")]
    #[Groups([
        'carrier_data',
        'carrier:read',
    ])]
    private TaxType $tax;

    protected function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getCarrier(): ?Carrier
    {
        return $this->carrier;
    }

    public function setCarrier(?Carrier $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    #[Pure] public function getShippingCost(): ?string
    {
        return Tools::formatPrice($this?->shippingCost);
    }

    public function setShippingCost(?float $shippingCost): self
    {
        $this->shippingCost = $shippingCost;

        return $this;
    }

    public function getTax(): ?TaxType
    {
        return $this->tax;
    }

    public function setTax(?TaxType $tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    public function getEntityName(): string
    {
        return "Livreur (données variables)";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "carrier_data";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }
}