<?php


namespace App\EventListener\Entity;


use App\Service\Tools\Tools;
use App\Traits\Fields\Date\DeletedAtTrait;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class SoftDeletableEntityRemoveListener implements EventSubscriberInterface
{

    public function __construct(
        private EntityManagerInterface $entityManager,
        private Tools                  $tools
    )
    {
    }

    // this method can only return the event names; you cannot define a
    // custom method name to execute when each event triggers
    public function getSubscribedEvents(): array
    {
        return [
            Events::preRemove,
        ];
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if (
            in_array(DeletedAtTrait::class, $this->tools->getUsedTraits($entity)) &&
            $entity->purgeDataOnDelete()
        ) {
            $entity->purgeData();

            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
    }

}