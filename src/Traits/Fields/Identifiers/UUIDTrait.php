<?php


namespace App\Traits\Fields\Identifiers;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\User\User;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Serializer\Annotation\Groups;

trait UUIDTrait
{

    #[Id()]
    #[Column(type: "uuid", unique: true)]
    #[GeneratedValue(strategy: "CUSTOM")]
    #[CustomIdGenerator(class: UuidGenerator::class)]
    #[Groups([
        "uuid:read",
        "user:read",
        'voucher:read',
        'voucher_data:read',
        'comment:read'
    ])]
    #[ApiProperty(identifier: true)]
    private string $uuid;

    /**
     * @return ?string
     */
    public function getUuid(): ?string
    {
        return $this->uuid ?? null;
    }

    /**
     * @param string $uuid
     *
     * @return User|UUIDTrait
     */
    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

}