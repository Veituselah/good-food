<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Tests\unit\FranchiseEmployeeTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

trait LogisticianExampleTrait
{
    use UserExampleTrait;

    private ?string $logisticianExampleIndex;
    private ?FranchiseEmployee $logisticianExample;
    private ?string $logisticianExampleUsername;
    private ?string $logisticianExamplePassword;
    private ?string $logisticianExampleToken;
    private ?string $logisticianExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function loadLogistician()
    {
        $this->logisticianExampleIndex = CustomApiTestCase::ROLE_LOGISTICIAN;
        $this->logisticianExample = CustomApiTestCase::getUserFixturesClean()[$this->logisticianExampleIndex];
        $this->logisticianExampleUsername = $this->logisticianExample->getUserIdentifier();
        $this->logisticianExamplePassword = CustomApiTestCase::PASSWORDS[$this->logisticianExampleIndex];
        $this->logisticianExampleToken = $this->getTokenForUser($this->logisticianExampleUsername, $this->logisticianExamplePassword);
        $this->logisticianExampleItemIRI = CustomApiTestCase::replaceParamsRoute(FranchiseEmployeeTest::FRANCHISE_EMPLOYEE_ITEM_ROUTE, ['uuid' => $this->logisticianExample->getUuid()]);
    }
}