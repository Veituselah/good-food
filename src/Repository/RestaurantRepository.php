<?php

namespace App\Repository;

use App\Dto\RestaurantLocationOutput;
use App\Entity\Country\Country;
use App\Entity\Order\Order;
use App\Entity\Reservation;
use App\Entity\Restaurant\Restaurant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Restaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Restaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Restaurant[]    findAll()
 * @method Restaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Restaurant::class);
    }

    /**
     * @param float $longitude
     * @param float $latitude
     *
     * @return RestaurantLocationOutput[]
     * @throws Exception
     */
    public function getRestaurantAroundLocation(float $longitude, float $latitude): array
    {
        $sql = "
        SELECT
        ST_Distance_Sphere(
            POINT(:longitude, :latitude),
            POINT(longitude, latitude)
        ) AS distance,
               id
        FROM
            restaurant
        WHERE ST_Distance_Sphere(
                POINT(:longitude, :latitude),
                POINT(longitude, latitude)
            ) < 30000 AND
        activated_at iS NOT NULL
        ORDER BY distance
        LIMIT 5;
        ";

        $params = [
            'longitude' => $longitude,
            'latitude'  => $latitude,
        ];

        $em = $this->getEntityManager();
        $stmt = $em->getConnection()->prepare($sql);
        $res = $stmt->executeQuery($params)->fetchAllAssociative();

        $data = [];
        $restaurantRepo = $em->getRepository(Restaurant::class);

        foreach ($res as $restaurantData) {
            $dto = new RestaurantLocationOutput();
            $dto->searchedLatitude = $latitude;
            $dto->searchedLongitude = $longitude;
            $dto->distance = $restaurantData['distance'];
            $dto->restaurant = $restaurantRepo->find($restaurantData['id']);
            $data[] = $dto;
        }

        return $data;

    }

    public function getRestaurantsInOrders(): array
    {
        $query = $this->_em->createQueryBuilder()
                           ->select('DISTINCT r')
                           ->from(Restaurant::class, 'r')
                           ->innerJoin(Order::class, 'o', Join::WITH, 'r = o.restaurant');

        return $query->getQuery()->getResult();

    }

    public function getRestaurantsInReservations(): array
    {
        $query = $this->_em->createQueryBuilder()
                           ->select('DISTINCT r')
                           ->from(Restaurant::class, 'r')
                           ->innerJoin(Reservation::class, 'res', Join::WITH, 'r = res.restaurant');

        return $query->getQuery()->getResult();

    }

    // /**
    //  * @return Restaurant[] Returns an array of Restaurant objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Restaurant
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
