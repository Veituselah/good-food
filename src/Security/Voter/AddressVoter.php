<?php

namespace App\Security\Voter;

use App\Entity\User\Customer\Address;
use App\Repository\OrderRepository;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\User\Comment\Comment;
use App\Entity\User\Customer\Customer;

class AddressVoter extends Voter
{

    public function __construct(
        private OrderRepository $orderRepository
    )
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['ADDRESS_MANAGE'])
               && $subject instanceof Address;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        if ($subject instanceof Address) {

            $orders = $this->orderRepository->getOrdersUsingAddress($subject);
            return count($orders) > 0;

        }

        return false;
    }

}
