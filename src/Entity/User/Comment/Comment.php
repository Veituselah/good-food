<?php

namespace App\Entity\User\Comment;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\Restaurant\Restaurant;
use App\Entity\Product\PurchasableProduct\PurchasableProduct;
use App\Entity\User\Customer\Customer;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\CommentRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Traits\Fields\Identifiers\IDTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Date\ActivatedAtTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
#[ApiResource(
    collectionOperations: [
        // Tout le monde peut récuprérer la liste des commentaires
        "get"  => [],

        // Seul les personnes ayant acheter un produit et étant connecté peuvent écrire un commentaire
        "post" => [
            "security_post_denormalize" => "is_granted('COMMENT_CREATE', object)",
        ],
        'api_customers_comments_get_subresource'=> [
            'method' => 'GET',
            "pagination_enabled" => true,
            "pagination_client_items_per_page" => true
        ]
    ],
    itemOperations: [
        // Seuls les admins peuvent supprimer les commentaires
        "delete" => [
            "security" => "is_granted('COMMENT_CREATE', object) or is_granted('" . FranchisorEmployee::ROLE_ADMIN . "') or is_granted('" . FranchiseEmployee::ROLE_LEAD . "')",
        ],

        // Tout le monde peut récupérer une liste de commentaire
        "get"    => [],

        //Seul l'utilisateur peut modifier son commentaire ou les admins
        "patch"  => [
            "security" => "object.customer === user or is_granted('" . FranchisorEmployee::ROLE_ADMIN . "')",
        ],
    ],
    subresourceOperations: [

    ],
    denormalizationContext: [
        "groups" => [
            "comment", "comment:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "comment", "comment:read",
        ],
    ]
)]
class Comment implements TestableEntityInterface
{

    use IDTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['comment:write', 'comment:read'])]
    private string $message;

    #[ORM\Column(type: 'integer', nullable: false)]
    #[Range
    (
        min: 0,
        max: 5
    )]
    #[NotNull(message: 'Vous devez renseigner la note')]
    #[Groups(['comment:write', 'comment:read'])]
    private int $note;

    /**
     * @var Restaurant|null
     */
    #[ORM\ManyToOne(targetEntity: Restaurant::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['comment:write', 'comment:read'])]
    private ?Restaurant $restaurant = null;

    /**
     * @var PurchasableProduct|null
     */
    #[ORM\ManyToOne(targetEntity: PurchasableProduct::class)]
    #[ORM\JoinColumn(name: "purchasableProduct_id", referencedColumnName: "reference", nullable: true)]
    #[Groups(['comment:write', 'comment:read'])]
    private ?PurchasableProduct $purchasableProduct = null;

    /**
     * @var Customer|null
     */
    #[ORM\ManyToOne(targetEntity: Customer::class)]
    #[ORM\JoinColumn(name: "customer_id", referencedColumnName: "uuid", nullable: false)]
    #[NotNull(message: 'Vous devez renseigner l\'auteur du message')]
    #[Groups(['comment:write', 'comment:read'])]
    private ?Customer $customer = null;

    public function getMessage(): ?string
    {
        return $this?->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this?->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getRestaurant(): ?Restaurant
    {
        return $this?->restaurant;
    }

    public function setRestaurant(?Restaurant $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getPurchasableProduct(): ?PurchasableProduct
    {
        return $this?->purchasableProduct;
    }

    public function setPurchasableProduct(?PurchasableProduct $purchasableProduct): self
    {
        $this->purchasableProduct = $purchasableProduct;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this?->customer;
    }

    public function setCustomer(Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getEntityName(): string
    {
        return "Commentaire";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "comments";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get",
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
