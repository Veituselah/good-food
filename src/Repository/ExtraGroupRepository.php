<?php

namespace App\Repository;

use App\Entity\Product\PurchasableProduct\Food\Extra\ExtraGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExtraGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtraGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtraGroup[]    findAll()
 * @method ExtraGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraGroupRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtraGroup::class);
    }

    // /**
    //  * @return ExtraGroup[] Returns an array of ExtraGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExtraGroup
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
