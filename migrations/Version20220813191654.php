<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220813191654 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_9CE58EE138248176');
        $this->addSql('DROP INDEX IDX_9CE58EE138248176 ON order_line');
        $this->addSql('ALTER TABLE order_line DROP currency_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_line ADD currency_id VARCHAR(10) NOT NULL');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE138248176 FOREIGN KEY (currency_id) REFERENCES currency (iso_code)');
        $this->addSql('CREATE INDEX IDX_9CE58EE138248176 ON order_line (currency_id)');
    }
}
