<?php


namespace App\Traits\Fields\Identifiers;

use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\Serializer\Annotation\Groups;

trait IDTrait
{

    #[Id()]
    #[GeneratedValue()]
    #[Column(type: "integer")]
    #[Groups([
        "id:read",
        "tag:read",
        "mail:read",
        'user_request:read',
        'voucher:read',
        'voucher_data:read',
        'order:read',
        'order_line:read',
        'comment:read',
        'reservation:read',
        'customer:read',
    ])]
    #[ApiProperty(identifier: true)]
    private int $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getEntityIdentifierName(): string
    {
        return "id";
    }

}