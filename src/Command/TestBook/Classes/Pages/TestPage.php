<?php

namespace App\Command\TestBook\Classes\Pages;

use App\Command\TestBook\Classes\Model\Entity;
use App\Command\TestBook\Classes\Model\TestLine;
use App\Command\TestBook\Classes\Test\TestType;
use DateTime;
use JetBrains\PhpStorm\Pure;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use ReflectionClass;
use ReflectionException;

class TestPage extends Page
{

    const DATE_FORMAT = 'd/m/Y';

    const FONT_HEADER_BODY_FOOTER_COLOR = "FFFFFF";
    const BG_HEADER_BODY_FOOTER_COLOR = "404040";
    const FIRST_COL_BODY_COLOR = "000000";
    const COLORED_BODY_CELL_COLOR = "FF9B00";
    const BODY_CELL_COLOR = "333333";
    const BORDER_BODY_COLOR = "000000";

    const BREAK_COLS = [
        'B',
        'C',
        'E',
    ];
    const BREAK_COL_WIDTH = 40;

    protected Entity $entity;

    /**
     * @throws ReflectionException
     * @throws Exception
     */
    public function __construct(
        private ReflectionClass $reflectionClass,
        Spreadsheet             $spreadsheet
    )
    {

        $this->entity = new Entity($this->reflectionClass);
        $sheet = new Worksheet($spreadsheet, $this->entity->getEntityName());
        $spreadsheet->addSheet($sheet);

        parent::__construct($sheet);

    }

    /**
     * @throws Exception
     */
    public function createPage()
    {
        $this->sheet->getDefaultRowDimension()->setRowHeight(25);

        $startRow = 1;

        $testTypes = TestType::getAllTestType(
            $this->entity
        );

        $arrayNumber = 1;

        foreach ($testTypes as $testType) {
            $startRow = $this->addArrayIntoWorksheet($arrayNumber, $testType, $startRow);
            $arrayNumber++;
        }

        $firstCol = self::FIRST_COL;
        $lastCol = $this->getLastCol();

        foreach (range($firstCol, $lastCol) as $columnID) {

            if (in_array($columnID, self::BREAK_COLS)) {
                $this->sheet->getColumnDimension($columnID)
                            ->setWidth(self::BREAK_COL_WIDTH);
            } else {
                $this->sheet->getColumnDimension($columnID)->setAutoSize(true);
            }
        }

    }

    /**
     * @throws Exception
     */
    public function addArrayIntoWorksheet(int $arrayNumber, TestType $testType, int $firstRow): int|string
    {
        $appTarget = $testType->getTestType();
        $data = $testType->getLinesToWriteInXlsx();

        $arrayNumberStr = $arrayNumber;

        while (strlen($arrayNumberStr) < 3) {
            $arrayNumberStr = "0" . $arrayNumberStr;
        }

        $now = new DateTime();

        $headerArray = [
            ["N°", $arrayNumberStr, "Responsable: Arthur GRENIER", "", "Date: " . $now->format(self::DATE_FORMAT), "Ver. 1.0.0"],
            ["Cibles", $appTarget],
            ["Sujets", $this->entity->getEntityName()],
        ];

        $footerArray = [
            ["Date de clôture", "27/08/2022"],
        ];

        $headerBody =
            [
                TestLine::HEADER,
            ];

        $headers = array_merge(
            $headerArray,
            self::EMPTY_LINE,
            $headerBody,
        );

        $rows =
            array_merge(
                $headers,
                $data,
                self::EMPTY_LINE,
                $footerArray,
                self::EMPTY_LINE,
            );

        $currentRow = $firstRow;

        $nbFooterLine = 0;

        $lastHeaderArrayLine = count($headerArray);
        $lastHeaderLine = count($headers) - 1;
        $lastDataLine = $lastHeaderLine + count($data);
        $firstFooterLine = $lastDataLine + 2;
        $lastFooterLine = $firstFooterLine + count($footerArray);

        foreach ($rows as $line => $row) {

            if ($row instanceof TestLine) {
                $row = $row->toArray();
            }

            $letter = self::FIRST_COL;

            $this->sheet->fromArray($row, '', $letter . $currentRow);

            $isEmptyLine = empty($row);
            $isBlackCellLine = $line < $lastHeaderArrayLine || $line == $lastHeaderLine || $line == $lastDataLine + 1;

            if (!$isEmptyLine) {

                $isContentLine = $line > (count($headers) - 1) && $line < (count($headers) + count($data));
                $isFooterLine = $line >= $firstFooterLine && $line <= $lastFooterLine;

                if ($isFooterLine) {
                    $nbFooterLine++;
                }

                $isHeaderLine = $line < count($headerArray);

                for ($col = 1; $col <= count(TestLine::HEADER); $col++) {

                    $style = $this->sheet->getStyle($letter . $currentRow);

                    if ($isBlackCellLine) {
                        $this->applyBlackCellArrayStyle($style);

                        if ($isHeaderLine) {
                            $this->applyHeaderCellArrayStyle($style);
                        }

                    } else {
                        $this->applyCellArrayStyle($style);
                    }

                    if ($isContentLine) {
                        $this->applyContentLineStyle($style, $currentRow);
                    }

                    $letter++;
                }

                if ($isHeaderLine) {
                    $this->applyHeaderLineArrayStyle($line, $currentRow);
                } else if ($isFooterLine) {
                    $this->applyFooterStyle($nbFooterLine, $currentRow);
                }

            } else {

                $this->applyEmptyLineStyle($currentRow, $isBlackCellLine);

            }

            $currentRow++;
        }

        $this->applyArrayStyle($firstRow, $currentRow);

        return $currentRow;
    }


    /**
     * @throws Exception
     */
    protected function applyHeaderCellArrayStyle(Style $style)
    {

        $style->getBorders()
              ->getOutline()
              ->setBorderStyle(Border::BORDER_THIN)
              ->getColor()
              ->setARGB(self::FONT_HEADER_BODY_FOOTER_COLOR);

    }

    /**
     * @throws Exception
     */
    protected function applyHeaderLineArrayStyle(int $line, int $row)
    {
        $lastCol = $this->getLastCol();

        switch ($line) {
            case 0:
                $this->sheet->mergeCells("C$row:D$row");
                $this->sheet->mergeCells("E$row:F$row");
                $this->sheet->mergeCells("G$row:$lastCol$row");
                break;
            default:
                $this->sheet->mergeCells("B$row:$lastCol$row");
                break;
        }

    }

    /**
     * @throws Exception
     */
    protected function applyFooterStyle(int $numFooterLine, int $row)
    {
        $lastCol = $this->getLastCol();

        $this->sheet->mergeCells("A$row:B$row");
        $this->sheet->mergeCells("C$row:$lastCol$row");

        $fullRange = "A$row:$lastCol$row";

        $this->sheet->getStyle($fullRange)
                    ->getFont()
                    ->setBold(true);

    }

    /**
     * @throws Exception
     */
    protected function applyEmptyLineStyle(int $row, bool $isBlackCellLine)
    {
        $lastCol = $this->getLastCol();

        $range = "A$row:$lastCol$row";
        $this->sheet->mergeCells($range);

        if ($isBlackCellLine) {
            $style = $this->sheet->getStyle($range);
            $this->applyBlackCellArrayStyle($style);
        }
    }

    protected function applyBlackCellArrayStyle(Style $style)
    {

        $style->getFill()
              ->setFillType(Fill::FILL_SOLID)
              ->getStartColor()
              ->setARGB(self::BG_HEADER_BODY_FOOTER_COLOR);

        $style->getFont()
              ->getColor()
              ->setARGB(self::FONT_HEADER_BODY_FOOTER_COLOR);

        $style->getFont()
              ->setBold(true)
              ->setItalic(true);
    }

    protected function applyContentLineStyle(Style $style, int $row)
    {
        $style->getFont()
              ->getColor()
              ->setARGB(self::BODY_CELL_COLOR);

        $style->getAlignment()
              ->setVertical(Alignment::VERTICAL_CENTER);

        $this->sheet->getRowDimension($row)
              ->setRowHeight(35);
    }

    /**
     * @throws Exception
     */
    protected function applyCellArrayStyle(Style $style)
    {

        $style->getBorders()
              ->getOutline()
              ->setBorderStyle(Border::BORDER_THIN)
              ->getColor()
              ->setARGB(self::BORDER_BODY_COLOR);
    }

    /**
     * @throws Exception
     */
    protected function applyArrayStyle(int $firstRow, int $lastRow)
    {
        $lastCol = $this->getLastCol();
        $lastArrayLine = $lastRow - 2;

        $firstCell = self::FIRST_COL . $firstRow;
        $lastCell = $lastCol . $lastArrayLine;

        $this->sheet->getStyle("$firstCell:$lastCell")
                    ->getBorders()
                    ->getOutline()
                    ->setBorderStyle(Border::BORDER_THIN)
                    ->getColor()
                    ->setARGB(self::BORDER_BODY_COLOR);

        $lastRowIndex = $lastArrayLine + 1;
        $lastRowRange = self::FIRST_COL . $lastRowIndex . ':' . $lastCol . ($lastRow - 1);

        $this->sheet->getStyle($lastRowRange)
                    ->getBorders()
                    ->getOutline()
                    ->setBorderStyle(Border::BORDER_THIN)
                    ->getColor()
                    ->setARGB(self::BORDER_BODY_COLOR);


        $this->sheet->getRowDimension($lastRowIndex)->setRowHeight(75);

        foreach (self::BREAK_COLS as $col) {

            $brealCells = "$col$firstRow:$col$lastRow";

            $this->sheet->getStyle($brealCells)
                        ->getAlignment()
                        ->setWrapText(true);

        }

    }


    #[Pure]
    protected function getLastCol(): int|string
    {
        return $this->parseColumnNumberToLetter(count(TestLine::HEADER));
    }

}