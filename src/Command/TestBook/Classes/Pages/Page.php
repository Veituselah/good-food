<?php

namespace App\Command\TestBook\Classes\Pages;

use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

abstract class Page
{

    const FIRST_COL = "A";

    const EMPTY_LINE = [
        [],
    ];

    public function __construct(
        protected Worksheet $sheet
    )
    {
    }

    public abstract function createPage();

    protected function parseColumnNumberToLetter(int $number): int|string
    {
        $letter = self::FIRST_COL;

        for ($i = 1; $i < $number; $i++) {
            $letter++;
        }

        return $letter;

    }

    /**
     * @throws Exception
     */
    protected function addBorder(Style $style, string $color)
    {

        $style->getBorders()
              ->getOutline()
              ->setBorderStyle(Border::BORDER_THIN)
              ->getColor()
              ->setARGB($color);

    }

    /**
     * @throws Exception
     */
    protected function addBorderToEachCellInRange(string $startCol, string $endCol, int $startRow, int $endRow, string $color)
    {

        $this->applyStyleToEachCellInRange(
            $startCol,
            $endCol,
            $startRow,
            $endRow,
            fn(Style $style) => $this->addBorder($style, $color)
        );

    }

    protected function applyStyleToEachCellInRange(string $startCol, string $endCol, int $startRow, int $endRow, callable $function)
    {

        foreach (range($startRow, $endRow) as $row) {

            foreach (range($startCol, $endCol) as $col) {

                $range = $col . $row;
                $style = $this->sheet->getStyle($range);
                $function($style);

            }

        }

    }

}