<?php

namespace App\Traits\Tests;

use App\Classes\CustomApiTestCase;
use App\Entity\Country\Tax\Tax;
use App\Tests\unit\TaxTest;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

trait TaxExampleTrait
{

    private ?Tax $taxExample;

    private ?string $taxExampleIndex;

    private ?string $taxExampleItemIRI;

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function loadTax()
    {
        $this->taxExampleIndex = TaxTest::TAX_EXAMPLE;
        $this->taxExample = TaxTest::getTaxFixtures()[$this->taxExampleIndex];
        $this->taxExampleItemIRI = CustomApiTestCase::replaceParamsRoute(TaxTest::TAX_ITEM_ROUTE, ['slug' => $this->taxExample->getSlug()]);
    }

}