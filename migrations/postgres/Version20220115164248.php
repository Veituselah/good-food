<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220115164248 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE variation_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE variation_value_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE variation (reference VARCHAR(15) NOT NULL, variable_product_id VARCHAR(15) NOT NULL, PRIMARY KEY(reference))');
        $this->addSql('CREATE INDEX IDX_629B33EA9CB74AEA ON variation (variable_product_id)');
        $this->addSql('CREATE TABLE variation_variation_value (variation_reference VARCHAR(15) NOT NULL, variation_value_id INT NOT NULL, PRIMARY KEY(variation_reference, variation_value_id))');
        $this->addSql('CREATE INDEX IDX_125B469CBA807CCA ON variation_variation_value (variation_reference)');
        $this->addSql('CREATE INDEX IDX_125B469CDB492DBE ON variation_variation_value (variation_value_id)');
        $this->addSql('CREATE TABLE variation_group (id INT NOT NULL, name VARCHAR(30) NOT NULL, position INT NOT NULL, slug VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, activated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_97B42B00989D9B62 ON variation_group (slug)');
        $this->addSql('COMMENT ON COLUMN variation_group.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN variation_group.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN variation_group.activated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN variation_group.deleted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE variation_value (id INT NOT NULL, variation_group_id INT NOT NULL, name VARCHAR(30) NOT NULL, position INT NOT NULL, slug VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, activated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E70337F1989D9B62 ON variation_value (slug)');
        $this->addSql('CREATE INDEX IDX_E70337F1DC3D4F5B ON variation_value (variation_group_id)');
        $this->addSql('COMMENT ON COLUMN variation_value.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN variation_value.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN variation_value.activated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN variation_value.deleted_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE variation ADD CONSTRAINT FK_629B33EA9CB74AEA FOREIGN KEY (variable_product_id) REFERENCES purchasable_product (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE variation ADD CONSTRAINT FK_629B33EAAEA34913 FOREIGN KEY (reference) REFERENCES product (reference) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE variation_variation_value ADD CONSTRAINT FK_125B469CBA807CCA FOREIGN KEY (variation_reference) REFERENCES variation (reference) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE variation_variation_value ADD CONSTRAINT FK_125B469CDB492DBE FOREIGN KEY (variation_value_id) REFERENCES variation_value (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE variation_value ADD CONSTRAINT FK_E70337F1DC3D4F5B FOREIGN KEY (variation_group_id) REFERENCES variation_group (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE variation_variation_value DROP CONSTRAINT FK_125B469CBA807CCA');
        $this->addSql('ALTER TABLE variation_value DROP CONSTRAINT FK_E70337F1DC3D4F5B');
        $this->addSql('ALTER TABLE variation_variation_value DROP CONSTRAINT FK_125B469CDB492DBE');
        $this->addSql('DROP SEQUENCE variation_group_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE variation_value_id_seq CASCADE');
        $this->addSql('DROP TABLE variation');
        $this->addSql('DROP TABLE variation_variation_value');
        $this->addSql('DROP TABLE variation_group');
        $this->addSql('DROP TABLE variation_value');
    }
}
