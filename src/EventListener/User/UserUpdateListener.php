<?php


namespace App\EventListener\User;


use App\Entity\User\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserUpdateListener
{
    public function __construct(
        protected UserPasswordHasherInterface $passwordHasherEncoder,
    )
    {
    }

    public function preUpdate(User $user, LifecycleEventArgs $event)
    {
        $this->updateUser($user, $event);
    }

    public function prePersist(User $user, LifecycleEventArgs $event)
    {
        $this->updateUser($user, $event);
    }

    public function updateUser(User $user, LifecycleEventArgs $event)
    {
        if (!empty($user->getPlainPassword())) {
            $user->setPassword(
                $this->passwordHasherEncoder->hashPassword($user, $user->getPlainPassword())
            );
            $user->eraseCredentials();
        }
    }
}