<?php

namespace App\Entity\User;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Entity\User\SuperAdmin\SuperAdmin;
use App\Interfaces\TestableEntityInterface;
use App\Repository\UserRepository;
use App\Security\Voter\UserVoter;
use App\Service\Tools\Tools;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\EmailTrait;
use App\Traits\Fields\Identifiers\UUIDTrait;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use PHPUnit\Framework\Test;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[Table(name: "`user`")]
#[Entity(repositoryClass: UserRepository::class)]
#[InheritanceType("JOINED")]
#[DiscriminatorColumn(name: "type", type: "string")]
#[DiscriminatorMap(
    [
        "customer"    => "App\Entity\User\Customer\Customer",
        "employee"    => "App\Entity\User\Employee\Employee",
        "franchisor"  => "App\Entity\User\Employee\FranchisorEmployee",
        "franchise"   => "App\Entity\User\Employee\FranchiseEmployee",
        "super_admin" => "App\Entity\User\SuperAdmin\SuperAdmin",
    ])]
#[HasLifecycleCallbacks()]
#[UniqueEntity(fields: ["email"], message: "L'email est déjà utilisé !")]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        "get" => [
            'security' => 'is_granted("' . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . '")',
        ],
    ],
    itemOperations: [
        "get"          => [
            'security' => 'is_granted("' . SuperAdmin::DEFAULT_ROLE_SUPER_ADMIN . '")',
        ],
        "update_roles" => [
            'method'                  => 'PATCH',
            'path'                    => '/users/{uuid}/roles',
            'security'                => '
		(
			is_granted("' . FranchiseEmployee::ROLE_LEAD . '") or 
			is_granted("' . FranchisorEmployee::ROLE_HR . '") 
		) and 
		is_granted("' . UserVoter::UPDATE_ROLES_SUPPORT . '", object)',
            "denormalization_context" => [
                'groups' => [
                    "user:roles", "user:roles:write",
                ],
            ],
            'validate'                => false,
        ],
    ],
    denormalizationContext: [
        "groups" => [],
    ],
    normalizationContext: [
        "groups" => [
            "user", "user:read",
        ],
    ]
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface, TestableEntityInterface
{

    use UUIDTrait;
    use EmailTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[Column(type: "string", length: 100)]
    #[Assert\NotBlank(message: "Le prénom doit être défini.")]
    #[Assert\Length(
        min: 2,
        max: 100,
        minMessage: "Le prénom ne doit pas être inférieur à {{ limit }} caractères.",
        maxMessage: "Le prénom ne doit pas dépasser {{ limit }} caractères."
    )]
    #[Groups([
        "user",
        'restaurant:read',
        'voucher:read',
        'voucher_data:read',
        'comment:read',
        'order:read',
        'reservation:read',
    ])]
    private string $firstname;


    #[Column(type: "string", length: 100)]
    #[Assert\NotBlank(message: "Le nom doit être défini.")]
    #[Assert\Length(
        min: 2,
        max: 100,
        minMessage: "Le nom doit faire plus de {{ limit }} caractères.",
        maxMessage: "Le nom doit faire moins de {{ limit }} caractères."
    )]
    #[Groups([
        "user",
        'restaurant:read',
        'voucher:read',
        'voucher_data:read',
        'comment:read',
        'order:read',
        'reservation:read',
    ])]
    private string $lastname;


    #[Column(type: "string", length: 64)]
    private string $password;

    #[Assert\NotBlank(
        message: Tools::PASSWORD_ERROR,
        groups: ['user:post:write']
    )]
    #[Assert\Regex(
        pattern: Tools::PASSWORD_REGEX,
        message: Tools::PASSWORD_ERROR
    )]
    #[SerializedName('password')]
    #[Groups("user:post:write")]
    private ?string $plainPassword;


    #[Column(type: "json")]
    #[Groups(["user:read", "user:roles:write"])]
    private array $roles = [];

    public function autoActiveOnCreation(): bool
    {
        return true;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    public function getAvailableRoles(): array
    {
        return [];
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword ?? null;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        // guarantee every user at least has ROLE_USER
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = array_values(array_unique($roles));

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return $this?->email;
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    protected function initRoles(): array
    {
        return [];
    }

    public function purgeData()
    {
        $this->lastname = self::getStringClearValue();
        $this->firstname = self::getStringClearValue();
        $this->email = self::getStringClearValue();
        $this->password = self::getStringClearValue();
        $this->setActive(false);
    }

    public function purgeDataOnDelete(): bool
    {
        return true;
    }

    #[PrePersist()]
    public function setRolesWhenCreateUser()
    {
        $roles = array_merge($this->initRoles(), $this->getRoles());
        $this->setRoles($roles);
    }

    public function getEntityName(): string
    {
        return "Utilisateur";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "users";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return false;
    }

    public function getEntityIdentifierName(): string
    {
        return "uuid";
    }

}
