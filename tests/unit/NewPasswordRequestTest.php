<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CustomApiTestCase;
use App\Entity\User\User;
use App\Entity\User\UserRequest\AccountConfirmationRequest;
use App\Entity\User\UserRequest\NewPasswordRequest;
use App\Traits\Tests\CustomerConfirmedExampleTrait;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class NewPasswordRequestTest extends CustomApiTestCase
{

    use CustomerConfirmedExampleTrait;

    // Routes
    const NEW_PASSWORD_ROUTE = self::API_ROUTE . '/new_password_requests';
    const NEW_PASSWORD_VALIDATE_ROUTE = self::NEW_PASSWORD_ROUTE . '/{id}/validate';

    // JSON LD Data
    const NEW_PASSWORD_REQUEST_TYPE = 'NewPasswordRequest';
    const NEW_PASSWORD_REQUEST_CONTEXT = self::CONTEXT . '/' . self::NEW_PASSWORD_REQUEST_TYPE;

    // Data
    const NEW_PASSWORD = 'aaAA&&11';

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadCustomerConfirmed();
    }

    /**
     * Tentative de demande de confirmation de compte avec le mauvais token d'auth
     * (se connecter en admin pour confirmé le compte d'un client)
     *
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testCreateNewPasswordRequest()
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                $this->customerConfirmedExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::NEW_PASSWORD_REQUEST_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::NEW_PASSWORD_REQUEST_TYPE,
                        'alreadyUsed'               => false,
                        'userTarget'                => $this->customerConfirmedExampleItemIRI,
                    ]
                );
                $this->assertMatchesResourceItemJsonSchema(AccountConfirmationRequest::class);


            },
            method: self::POST_METHOD,
            url: NewPasswordRequestTest::NEW_PASSWORD_ROUTE,
            json: [
                'userTarget' => $this->customerConfirmedExampleItemIRI,
            ],
        );
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testCreateNewPasswordRequestAndValidateIt()
    {
        $response = $this->doRequest(
            method: self::POST_METHOD,
            url: self::NEW_PASSWORD_ROUTE,
            json: [
                'userTarget' => $this->customerConfirmedExampleItemIRI,
            ],
            token: $this->customerConfirmedExampleToken,
        );

        $this->assertSuccess();
        $this->assertJsonContains(
            [
                self::CONTEXT_INDEX_JSON_LD => self::NEW_PASSWORD_REQUEST_CONTEXT,
                self::TYPE_INDEX_JSON_LD    => self::NEW_PASSWORD_REQUEST_TYPE,
                'alreadyUsed'               => false,
                'userTarget'                => $this->customerConfirmedExampleItemIRI,
            ]
        );
        $this->assertMatchesResourceItemJsonSchema(NewPasswordRequest::class);

        $idRequest = $response->toArray()['id'];

//        Changement de MDP

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->replaceParamsRoute(NewPasswordRequestTest::NEW_PASSWORD_VALIDATE_ROUTE, ['id' => $idRequest]),
            json: [
                'password' => self::NEW_PASSWORD,
            ]
        );

        $this->assertSuccess();
        $this->assertJsonContains(
            [
                self::CONTEXT_INDEX_JSON_LD => self::NEW_PASSWORD_REQUEST_CONTEXT,
                self::TYPE_INDEX_JSON_LD    => self::NEW_PASSWORD_REQUEST_TYPE,
                'alreadyUsed'               => true,
                'userTarget'                => $this->customerConfirmedExampleItemIRI,
            ]
        );
        $this->assertMatchesResourceItemJsonSchema(NewPasswordRequest::class);

//        Vérifie que l'utilisateur a son compte de validé

        $response = $this->doRequest(
            method: self::POST_METHOD,
            url: self::LOGIN_ROUTE,
            json: [
                'username' => $this->customerConfirmedExampleUsername,
                'password' => self::NEW_PASSWORD,
            ]
        );

        $json = $response->toArray();
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('token', $json);

    }

    /**
     * Test d'une erreur si le temps entre deux requêtes n'est pas valide (il faut attendre deux minutes)
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testCreateNewPasswordRequestWhenDelayBetweenTwoRequestIsNotRespected()
    {
//        1ère demande
        $this->doRequest(
            method: self::POST_METHOD,
            url: NewPasswordRequestTest::NEW_PASSWORD_ROUTE,
            json: [
                'userTarget' => $this->customerConfirmedExampleItemIRI,
            ],
            token: $this->customerConfirmedExampleToken,
        );

        $this->assertSuccess();

//        2ème demande
        $this->doRequest(
            method: self::POST_METHOD,
            url: NewPasswordRequestTest::NEW_PASSWORD_ROUTE,
            json: [
                'userTarget' => $this->customerConfirmedExampleItemIRI,
            ],
            token: $this->customerConfirmedExampleToken,
        );

        $this->assertBadRequest();

    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testCreateNewPasswordRequestWrongData()
    {
        $this->doRequest(
            method: self::POST_METHOD,
            url: NewPasswordRequestTest::NEW_PASSWORD_ROUTE,
            json: [
            ],
            token: $this->customerConfirmedExampleToken,
        );

        $this->assertAccessDenied();

        $this->doRequest(
            method: self::POST_METHOD,
            url: NewPasswordRequestTest::NEW_PASSWORD_ROUTE,
            json: [
                'userTarget' => '1ec6593a-8b60-608c-869c-89ede9f2c00a',
            ],
            token: $this->customerConfirmedExampleToken,
        );

        $this->assertBadRequest();

    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function testValidateNewPasswordRequestAlreadyValidated()
    {
        $response = $this->doRequest(
            method: self::POST_METHOD,
            url: NewPasswordRequestTest::NEW_PASSWORD_ROUTE,
            json: [
                'userTarget' => $this->customerConfirmedExampleItemIRI,
            ],
            token: $this->customerConfirmedExampleToken,
        );

        $this->assertSuccess();
        $idRequest = $response->toArray()['id'];

//        Validation du compte

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->replaceParamsRoute(NewPasswordRequestTest::NEW_PASSWORD_VALIDATE_ROUTE, ['id' => $idRequest]),
            json: [
                "password" => self::NEW_PASSWORD,
            ],
        );

        $this->assertSuccess();

//        Réutilisation de la demande => erreur (déjà utilisée)

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->replaceParamsRoute(NewPasswordRequestTest::NEW_PASSWORD_VALIDATE_ROUTE, ['id' => $idRequest]),
            json: [],
        );

        $this->assertBadRequest();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testValidateNewPasswordRequestWrongData()
    {
        $response = $this->doRequest(
            method: self::POST_METHOD,
            url: self::NEW_PASSWORD_ROUTE,
            json: [
                'userTarget' => $this->customerConfirmedExampleItemIRI,
            ],
            token: $this->customerConfirmedExampleToken,
        );

        $this->assertSuccess();

        $idRequest = $response->toArray()['id'];

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->replaceParamsRoute(NewPasswordRequestTest::NEW_PASSWORD_VALIDATE_ROUTE, ['id' => $idRequest]),
            json: [],
        );

        $this->assertBadRequest();

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->replaceParamsRoute(NewPasswordRequestTest::NEW_PASSWORD_VALIDATE_ROUTE, ['id' => $idRequest]),
            json: [
                'password' => ' ',
            ],
        );

        $this->assertBadRequest();

    }

}
