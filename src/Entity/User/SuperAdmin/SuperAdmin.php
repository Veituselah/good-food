<?php

namespace App\Entity\User\SuperAdmin;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\User;
use App\Repository\SuperAdminRepository;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;


#[Entity(repositoryClass: SuperAdminRepository::class)]
#[ApiResource(
    collectionOperations: [
        //	Seuls les super admins peuvent voir la liste des supers admins
        "get" => [
            "security" => 'is_granted("' . self::DEFAULT_ROLE_SUPER_ADMIN . '")',
        ],
    ],
    itemOperations: [
        //	Seuls les super admins peuvent voir les données d'un super admin
        "get"   => [
            "security" => 'is_granted("' . self::DEFAULT_ROLE_SUPER_ADMIN . '") or object === user',
        ],
        //	Le possesseur du compte peut modifier les infos du compte
        "patch" => [
            "security" => "object === user",
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "user", "user:write",
            "super_admin", "super_admin:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "user", "user:read",
            "super_admin", "super_admin:read",
        ],
    ]
)]
class SuperAdmin extends User
{

    const DEFAULT_ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    const AVAILABLE_ROLES = [self::DEFAULT_ROLE_SUPER_ADMIN];

    public function getAvailableRoles(): array
    {
        return array_merge(parent::getAvailableRoles(), self::AVAILABLE_ROLES);
    }

    protected function initRoles(): array
    {
        return [self::DEFAULT_ROLE_SUPER_ADMIN];
    }

    public function getEntityName(): string
    {
        return "Super Admin";
    }

    public function routesHaveBeenTested(): bool
    {
        return true;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "super_admins";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return false;
    }
}
