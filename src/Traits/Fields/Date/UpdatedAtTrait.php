<?php


namespace App\Traits\Fields\Date;


use DateTimeImmutable;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Gedmo\Mapping\Annotation\Timestampable;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @package App\Traits\Fields
 */
#[HasLifecycleCallbacks()]
trait UpdatedAtTrait
{

    #[Timestampable(on: "update")]
    #[Column(type: "datetime_immutable")]
    #[Groups([
        "date:read",
        "product:read",
        "supplier:read",
        "stock:read",
        "variation_value:read",
        "variation_group:read",
        "tax:read",
        "tax_type:read",
        "address:read",
        "user:read",
        "category:read",
        "country:read",
        "currency:read",
        "mail:read",
        "restaurant:read",
        "extra_group:read",
        "extra_value:read",
        "payment_method:read",
        'voucher:read',
        'voucher_data:read',
        'carrier_data:read',
        'carrier:read',
        'order:read',
        'reservation:read',
        'order_line:read',
        'comment:read',
        'order_status:read',
    ])]
    private DateTimeImmutable $updatedAt;

    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }

}