<?php

namespace App\Controller\Reservation;

use App\Entity\Reservation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ReservationCancelAction extends AbstractController
{

    public function __invoke(Reservation $data): Reservation
    {
        $data->setCanceledAt();

        return $data;
    }

}