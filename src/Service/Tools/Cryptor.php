<?php


namespace App\Service\Tools;


use Exception;

class Cryptor
{

    /**
     * @throws Exception
     */
    public function __construct(
        private string $cipherAlgo,
        private int $options = OPENSSL_RAW_DATA,
        private ?string $tag = null,
        private string $aad = "",
        private int $tag_length = 16
    )
    {
        if (!in_array($this->options, [0, OPENSSL_RAW_DATA, OPENSSL_ZERO_PADDING])) {
            throw new Exception("Options de disjonction non valide." .
                "Doit valoir : " .
                " - 0 " .
                " - 'OPENSSL_RAW_DATA' => " . OPENSSL_RAW_DATA .
                " - 'OPENSSL_ZERO_PADDING' => " . OPENSSL_ZERO_PADDING
            );
        }

        if (!in_array($this->cipherAlgo, self::getCipherAlgo())) {
            throw new Exception("Algorithme de chiffrement non valide : voir la fonction Cryptor::getCipherAlgo() pour connaître la liste diponible.");
        }

    }

    public static function getCipherAlgo(): array
    {
        return openssl_get_cipher_methods();
    }

    public function encrypt(string $data, string $passphrase = ""): string
    {
        $iv_length = openssl_cipher_iv_length($this->cipherAlgo);
        $iv = openssl_random_pseudo_bytes($iv_length);

        $dataEncrypted = openssl_encrypt(
            $data,
            $this->cipherAlgo,
            $passphrase,
            $this->options,
            $iv
//            ,
//            $this->tag,
//            $this->aad,
//            $this->tag_length
        );

        return Tools::base64_url_encode($iv . $dataEncrypted);
    }

    public function decrypt(string $data, string $passphrase = ""): string
    {
        $dataDecode = Tools::base64_url_decode($data);

//        On récupère le "iv"
        $iv_length = openssl_cipher_iv_length($this->cipherAlgo);
        $iv = substr($dataDecode, 0, $iv_length);

//        On récupère la valeur à décoder
        $dataEncrypted = substr($dataDecode, $iv_length);

        return openssl_decrypt(
            $dataEncrypted,
            $this->cipherAlgo,
            $passphrase,
            $this->options,
            $iv
//            ,
//            $this->tag,
//            $this->aad
        );
    }

}