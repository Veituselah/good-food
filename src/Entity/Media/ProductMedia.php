<?php

namespace App\Entity\Media;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Product\Product;
use App\Interfaces\TestableEntityInterface;
use App\Repository\ProductMediaRepository;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


#[ORM\Entity(repositoryClass: ProductMediaRepository::class)]
#[ApiResource(
    collectionOperations: [],
    itemOperations: [
        "get" => [],
    ],
    denormalizationContext: [
        'product_media', 'product_media:write',
    ],
    normalizationContext: [
        'product_media', 'product_media:read',
    ],
    compositeIdentifier: false
)]
class ProductMedia implements TestableEntityInterface
{

    use CreatedAtTrait;
    use UpdatedAtTrait;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Media::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([
        'product_media:post',
        'product_media:read',
        'product:read',
        'order:read',
    ])]
    private Media $media;

    #[ORM\Column(type: 'boolean')]
    #[Groups([
        'product_media',
        'product:read',
        'order:read',
    ])]
    private bool $mediaDefault;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Product::class, inversedBy: 'medias')]
    #[ORM\JoinColumn(referencedColumnName: 'reference', nullable: false)]
    #[Groups([
        'product_media:post',
        'product_media:read',
    ])]
    private Product $product;

    public function getMedia(): ?Media
    {
        return $this->media;
    }

    public function setMedia(?Media $media): self
    {
        $this->media = $media;

        return $this;
    }

    public function getMediaDefault(): ?bool
    {
        return $this->mediaDefault;
    }

    public function setMediaDefault(bool $mediaDefault): self
    {
        $this->mediaDefault = $mediaDefault;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getEntityName(): string
    {
        return "Images Produit";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "product_medias";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

    public function getEntityIdentifierName(): string
    {
        return "media";
    }

}
