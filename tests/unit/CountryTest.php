<?php
// tests/AuthenticationTest.php

namespace App\Tests\unit;

use App\Classes\CrudApiTestCase;
use App\Entity\Country\Country;
use App\Entity\Country\Currency;
use App\Entity\User\User;
use App\Service\Tools\Tools;
use App\Traits\Tests\AdminExampleTrait;
use App\Traits\Tests\CountryExampleTrait;
use App\Traits\Tests\CurrencyExampleTrait;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class CountryTest extends CrudApiTestCase
{

    use AdminExampleTrait;
    use CountryExampleTrait;
    use CurrencyExampleTrait;

    // Fixtures
    const COUNTRY_FIXTURES_FILES = 'country.yml';

    // Routes
    const COUNTRY_ROUTE = self::API_ROUTE . '/countries';
    const COUNTRY_ITEM_ROUTE = self::COUNTRY_ROUTE . '/{isoCode}';
    const COUNTRY_ACTIVE_ROUTE = self::COUNTRY_ITEM_ROUTE . '/active';

    // JSON LD Data
    const COUNTRY_TYPE = 'Country';
    const COUNTRY_CONTEXT = self::CONTEXT . '/' . self::COUNTRY_TYPE;

    // Data
    const ALL_FIELDS_VIOLATION = ['name', 'isoCode', "defaultCurrency"];
    const COUNTRY_EXAMPLE = "France";

    /**
     * @return Country[]
     */
    #[Pure] public static function getCountryFixtures(): array
    {
        return self::getDataFixturesOfClass(Country::class);
    }

    /**
     * @throws ExceptionInterface
     */
    public static function getJSONFromCountry(Country $country): array
    {
        $data = self::parseEntityToArray($country);
        $data['defaultCurrency'] = self::replaceParamsRoute(CurrencyTest::CURRENCY_ITEM_ROUTE, ['isoCode' => $country->getDefaultCurrency()->getIsoCode()]);

        return $data;
    }

    /**
     * @throws Exception
     */
    public static function getValidTestCountry(
        Currency $currency
    ): Country
    {
        $country = new Country();
        $country->setName('Test country');
        $country->setPhonePrefix('+321');
        $country->setIsoCode("TEST");
        $country->setDefaultCurrency($currency);

        return $country;
    }

    /**
     * @throws Exception
     * @throws ExceptionInterface
     */
    public static function getValidTestCountryInJSON(
        Currency $currency
    ): array
    {
        $country = self::getValidTestCountry($currency);

        return self::getJSONFromCountry($country);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->loadAdmin();
        $this->loadCountry();
        $this->loadCurrency();
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetCollection(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::ALL_USERS,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertCollection(
                    response: $response,
                    endpointCollectionRoute: self::COUNTRY_ROUTE,
                    context: self::COUNTRY_CONTEXT,
                    classToCheckValidity: Country::class,
                    totalItems: count(self::getCountryFixtures())
                );
            },
            method: self::GET_METHOD,
            url: self::COUNTRY_ROUTE,
        );
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function testGetItem(): void
    {

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::ALL_USERS,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccess();

                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::COUNTRY_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::COUNTRY_TYPE,
                        'active'                    => $this->countryExample->isActive(),
                        'deleted'                   => $this->countryExample->isDeleted(),
                    ]
                );

                $validJson = self::getJSONFromCountry($this->countryExample);
                $validJson['defaultCurrency'] = self::parseEntityToArray($this->currencyExample);
                $this->assertJsonContains($validJson);

                $country = $response->toArray();

                $this->assertIdentifierOfResponseData(self::COUNTRY_ROUTE, Tools::ISO_CODE_REGEX, $country);
                $this->assertMatchesResourceItemJsonSchema(Country::class);

            },
            method: self::GET_METHOD,
            url: $this->countryExampleItemIRI,
        );
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception|ExceptionInterface
     */
    public function testPostItem()
    {
        $data = $this->getValidTestCountryInJSON($this->currencyExample);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $validJson = $json;
                $validJson['defaultCurrency'] = self::parseEntityToArray($this->currencyExample);

                $countryCreated = $response->toArray();

                $this->assertSuccess();
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::COUNTRY_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::COUNTRY_TYPE,
                        'deleted'                   => false,
                        'active'                    => true,
                    ]
                );

                $this->assertJsonContains($validJson);
                $this->assertIdentifierOfResponseData(self::COUNTRY_ROUTE, Tools::ISO_CODE_REGEX, $countryCreated);
                $this->assertMatchesResourceItemJsonSchema(Country::class);

                $this->initClient();
            },
            method: self::POST_METHOD,
            url: self::COUNTRY_ROUTE,
            json: $data,
        );

    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ExceptionInterface
     */
    public function testPostInvalidItem()
    {
        $validCountry = self::getValidTestCountryInJSON($this->currencyExample);

        // Empty request error
        $requestEmptyData = $this->doRequest(
            method: self::POST_METHOD,
            url: self::COUNTRY_ROUTE,
            json: [],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestEmptyData);

        // Min carac error

        $requestNotRespectMinValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::COUNTRY_ROUTE,
            json: [
                "name"           => "",
                "rateForOneEuro" => "0",
                "isoCode"        => "",
                "sumbol"         => "",
            ],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMinValidation);

        // Max carac error

        $requestNotRespectMaxValidation = $this->doRequest(
            method: self::POST_METHOD,
            url: self::COUNTRY_ROUTE,
            json: [
                "name"           => self::STRING_LENGTH_256,
                "rateForOneEuro" => "0",
                "isoCode"        => self::STRING_LENGTH_256,
                "sumbol"         => self::STRING_LENGTH_256,
            ],
            token: $this->adminExampleToken,
        );

        $this->assertViolations(self::ALL_FIELDS_VIOLATION, $requestNotRespectMaxValidation);

//        Malformatted currency iri error

        $data = $validCountry;
        $data['defaultCurrency'] = 'azer';

        $this->doRequest(
            method: self::POST_METHOD,
            url: self::COUNTRY_ROUTE,
            json: $data,
            token: $this->adminExampleToken
        );

        $this->assertBadRequest();
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws Exception
     */
    public function testDeleteItem(): void
    {
        $superAdminToken = $this->getTokenFromUserIndex(self::CEDRIC_ADMIN);
        $nbCustomersInCollectionBefore = $this->getTotalItemsForRequest(self::COUNTRY_ROUTE, $superAdminToken);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                $this->adminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $this->assertSuccessDelete();

                $this->assertNull(self::getEntityManager()->getRepository(Country::class)->findOneBy(['isoCode' => $this->countryExample->getIsoCode()]));

                $this->doRequest(
                    method: self::GET_METHOD,
                    url: $this->countryExampleItemIRI,
                    token: $superAdminToken,
                );

                $this->assertNotFound();

                $nbCustomersInCollectionAfter = $this->getTotalItemsForRequest(self::COUNTRY_ROUTE, $superAdminToken);

                $this->assertEquals($nbCustomersInCollectionBefore - 1, $nbCustomersInCollectionAfter);

                $this->initClient();

            },
            method: self::DELETE_METHOD,
            url: $this->countryExampleItemIRI,
            dataToSendToFunctionAssertAhtorizeUser: compact('nbCustomersInCollectionBefore', 'superAdminToken')
        );
    }

    /**
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ExceptionInterface
     */
    public function testPatchItem(): void
    {
        $country = new Country();
        $country->setIsoCode($this->countryExample->getIsoCode());
        $country->setName("a" . $this->countryExample->getName());
        $country->setPhonePrefix($this->countryExample->getPhonePrefix() . '1');
        $country->setDefaultCurrency($this->currencyExample);
        $country->setActive(!$this->countryExample->isActive());

        $json = self::getJSONFromCountry($country);

        $this->doRequestForEachUsers(
            authorizedUserIndex: [
                self::CEDRIC_ADMIN,
                self::ARTHUR_ADMIN,
                self::CORENTIN_ADMIN,
                self::ROLE_ADMIN,
                $this->adminExampleIndex,
            ],
            functionAssertRequestAuthorizedUser: function (ResponseInterface $response, string $token, string $userIndex, User $userData, ?array $json, array $dataToSendToFunctionAssertAhtorizeUser) {

                extract($dataToSendToFunctionAssertAhtorizeUser);

                $validJson = $json;
                $validJson['defaultCurrency'] = self::parseEntityToArray($this->currencyExample);

                $this->assertSuccess();
                //        Vérification du contenu du retour
                $this->assertJsonContains(
                    [
                        self::CONTEXT_INDEX_JSON_LD => self::COUNTRY_CONTEXT,
                        self::TYPE_INDEX_JSON_LD    => self::COUNTRY_TYPE,
                        'deleted'                   => false,
                    ]
                );
                $this->assertJsonContains($validJson);

                $country = $response->toArray();

                $this->assertIdentifierOfResponseData(self::COUNTRY_ROUTE, Tools::ISO_CODE_REGEX, $country);
                $this->assertMatchesResourceItemJsonSchema(Country::class);

                $this->initClient();
            },
            method: self::PATCH_METHOD,
            url: $this->countryExampleItemIRI,
            json: $json,
        );
    }

    public function testPatchInvalidItem()
    {
        // TODO: Implement testPatchInvalidItem() method.
    }

    public function testGetCollectionWithFilter()
    {
        // TODO: Implement testGetCollectionWithFilter() method.
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception|ExceptionInterface
     */
    public function testCreateCountrySameIsoCode()
    {
        $data = $this->getValidTestCountryInJSON($this->currencyExample);
        $this->doRequest(
            method: self::POST_METHOD,
            url: self::COUNTRY_ROUTE,
            json: $data,
            token: $this->adminExampleToken
        );

        $this->assertSuccess();

        $requestResult = $this->doRequest(
            method: self::POST_METHOD,
            url: self::COUNTRY_ROUTE,
            json: $data,
            token: $this->adminExampleToken
        );

        $this->assertViolations(['isoCode'], $requestResult);

    }

}
