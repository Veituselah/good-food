<?php

namespace App\Command\TestBook\Classes\Model;

use App\Command\TestBook\GenerateTestBookCommand;
use App\Interfaces\TestableEntityInterface;
use JetBrains\PhpStorm\Pure;
use ReflectionClass;
use ReflectionException;

class Entity
{

    private string $entityName;

    private ?TestableEntityInterface $testableEntityInterface = null;

    private bool $routesHaveBeenTested = false;

    private ?string $entityRouteName = null;

    private ?string $entityIdentifierName = null;

    private array $routesDontNeedingAuth = [];

    private array $matchingRouteToTaskName = [];

    private array $collectionOperations = [];

    private array $itemOperations = [];

    private array $subresourceOperations = [];

    /**
     * @throws ReflectionException
     */
    public function __construct(
        protected ReflectionClass $reflectionClass
    )
    {
        $this->entityName = $reflectionClass->getShortName();
        $this->initTestableEntityInterface();
        $this->initApiResourceAttribute();
    }

    /**
     * @throws ReflectionException
     */
    protected function initTestableEntityInterface()
    {

        if ($this->isTestableEntityInterfaceInterface($this->reflectionClass)) {

            $instance = $this->reflectionClass->newInstance();

            if ($instance instanceof TestableEntityInterface) {
                $this->testableEntityInterface = $instance;
                $this->entityName = $this->testableEntityInterface->getEntityName();
                $this->routesHaveBeenTested = $this->testableEntityInterface->routesHaveBeenTested();
                $this->entityRouteName = $this->testableEntityInterface->getEntityRouteName();
                $this->entityIdentifierName = $this->testableEntityInterface->getEntityIdentifierName();
                $this->matchingRouteToTaskName = $this->testableEntityInterface->getMappingRouteToTaskName();
                $this->routesDontNeedingAuth = $this->testableEntityInterface->getRoutesDontNeedingAuth();
            }

        }

    }

    protected function initApiResourceAttribute()
    {

        $apiResource = GenerateTestBookCommand::getApiResource($this->reflectionClass);

        $this->collectionOperations = $apiResource->getArguments()['collectionOperations'] ?? [];
        $this->itemOperations = $apiResource->getArguments()['itemOperations'] ?? [];
        $this->subresourceOperations = $apiResource->getArguments()['subresourceOperations'] ?? [];

    }

    #[Pure]
    protected function isTestableEntityInterfaceInterface(ReflectionClass $reflectionClass): bool
    {
        return in_array(TestableEntityInterface::class, $reflectionClass->getInterfaceNames());
    }

    /**
     * @return string
     */
    public function getEntityName(): string
    {
        return $this->entityName;
    }

    /**
     * @return TestableEntityInterface|null
     */
    public function getTestableEntityInterface(): ?TestableEntityInterface
    {
        return $this->testableEntityInterface;
    }

    /**
     * @return bool
     */
    public function isRoutesHaveBeenTested(): bool
    {
        return $this->routesHaveBeenTested;
    }

    /**
     * @return string|null
     */
    public function getEntityRouteName(): ?string
    {
        return $this->entityRouteName;
    }

    /**
     * @return string|null
     */
    public function getEntityIdentifierName(): ?string
    {
        return $this->entityIdentifierName;
    }

    /**
     * @return array
     */
    public function getMatchingRouteToTaskName(): array
    {
        return $this->matchingRouteToTaskName;
    }

    /**
     * @return array
     */
    public function getCollectionOperations(): array
    {
        return $this->collectionOperations;
    }

    /**
     * @return array
     */
    public function getItemOperations(): array
    {
        return $this->itemOperations;
    }

    /**
     * @return array
     */
    public function getSubresourceOperations(): array
    {
        return $this->subresourceOperations;
    }

    /**
     * @return array
     */
    public function getRoutesDontNeedingAuth(): array
    {
        return $this->routesDontNeedingAuth;
    }

}

