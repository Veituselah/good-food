<?php


namespace App\Tests\func;


use App\Classes\CustomApiTestCase;
use App\Entity\User\Customer\Customer;
use App\Entity\User\UserRequest\AccountConfirmationRequest;
use App\Service\Tools\Tools;
use App\Tests\unit\AccountConfirmationRequestTest;
use App\Tests\unit\CustomerTest;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CustomerAccountCreationStoryTest extends CustomApiTestCase
{

    /**
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws Exception
     * @throws ExceptionInterface
     */
    public function testCreateAndConfirmCustomerAccount()
    {
        $testUser = CustomerTest::getValidTestCustomerInJSON();

        // Création du client
        $response = $this->doRequest(
            method: self::POST_METHOD,
            url: CustomerTest::CUSTOMER_ROUTE,
            json: $testUser,
        );

        $this->assertSuccess();
        $this->assertJsonContains(
            [
                self::CONTEXT_INDEX_JSON_LD => CustomerTest::CUSTOMER_CONTEXT,
                self::TYPE_INDEX_JSON_LD    => CustomerTest::CUSTOMER_TYPE,
                "birthdate"                 => $testUser['birthdate'],
                "lastname"                  => $testUser['lastname'],
                "firstname"                 => $testUser['firstname'],
                "email"                     => $testUser['email'],
                "roles"                     => [Customer::DEFAULT_ROLE_CUSTOMER],
                'confirmedAccount'          => false,
                'active'                    => true,
                'deleted'                   => false,
            ]
        );

        $customer = $response->toArray();

        $this->assertIdentifierOfResponseData(CustomerTest::CUSTOMER_ROUTE, Tools::UUID_REGEX, $customer);
        $this->assertMatchesResourceItemJsonSchema(Customer::class);

        $uuid = $customer['uuid'];
        $customerItemIRI = $this->replaceParamsRoute(CustomerTest::CUSTOMER_ITEM_ROUTE, ['uuid' => $uuid]);
        // $token = $this->getTokenForUser($testUser['email'], $testUser['password']);

//        -------------------------------------------------------------------------------------------

        AccountConfirmationRequestTest::removeAllRequestForUserWithUuidAndEntityManager($uuid);

        $response = $this->doRequest(
            method: self::POST_METHOD,
            url: AccountConfirmationRequestTest::ACCOUNT_CONFIRM_ROUTE,
            json: [
                'customerTarget' => $customerItemIRI,
            ],
            // token: $token
        );

        $assertJson =
            [
                self::CONTEXT_INDEX_JSON_LD => AccountConfirmationRequestTest::ACCOUNT_CONFIRMATION_REQUEST_CONTEXT,
                self::TYPE_INDEX_JSON_LD    => AccountConfirmationRequestTest::ACCOUNT_CONFIRMATION_REQUEST_TYPE,
                'alreadyUsed'               => false,
                'customerTarget'            => $customerItemIRI,
            ];

        $this->assertSuccess();
        $this->assertJsonContains($assertJson);

        $idRequest = $response->toArray()['id'];

        // Validation du compte

        $this->doRequest(
            method: self::PATCH_METHOD,
            url: $this->replaceParamsRoute(AccountConfirmationRequestTest::ACCOUNT_CONFIRM_VALIDATE_ROUTE, ['id' => $idRequest]),
            json: [],
        );

        $assertJson['alreadyUsed'] = true;

        $this->assertSuccess();
        $this->assertJsonContains($assertJson);
        $this->assertMatchesResourceItemJsonSchema(AccountConfirmationRequest::class);

        $token = $this->getTokenForUser($testUser['email'], $testUser['password']);

        // Vérifie que l'utilisateur a son compte de validé
        $this->assertMatchesResourceItemJsonSchema(AccountConfirmationRequest::class);

        $this->doRequest(
            method: self::GET_METHOD,
            url: $customerItemIRI,
            token: $token,
        );

        $this->assertSuccess();
        $this->assertJsonContains(
            [
                'confirmedAccount' => true,
            ]
        );

    }

}