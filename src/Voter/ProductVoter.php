<?php

namespace App\Voter;

use App\Entity\Product\Product;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use Prophecy\Argument\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ProductVoter extends Voter
{

    const IS_AUTHORIZED_TO_MANAGE_PRODUCT = 'MANAGE_PRODUCT';

    public function __construct(private Security $security)
    {
    }

    protected function supports($attribute, $subject): bool
    {
        $supportsAttribute = $attribute == self::IS_AUTHORIZED_TO_MANAGE_PRODUCT;
        $supportsSubject = $subject instanceof Product;

        return $supportsAttribute && $supportsSubject;
    }

    /**
     * @param string         $attribute
     * @param Product        $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, $token): bool
    {
        $user = $this->security->getUser();
        $isAuthorized = false;

        if ($attribute == self::IS_AUTHORIZED_TO_MANAGE_PRODUCT) {

            if ($this->security->isGranted(FranchisorEmployee::ROLE_EDITOR)) {
                $isAuthorized = true;
            } else {

                $productRestaurants = $subject->getOnlyInRestaurants();

                // Autorisé seulement si le produit est dispo seulement dans le restau de l'employé connecté
                if (
                    $this->security->isGranted(FranchiseEmployee::ROLE_LOGISTICIAN) &&
                    $user instanceof FranchiseEmployee &&
                    count($productRestaurants) == 1
                ) {

                    foreach ($productRestaurants as $restaurant) {
                        $isAuthorized |= $restaurant === $user->getRestaurant();
                    }

                }
            }
        }

        return $isAuthorized;
    }

}