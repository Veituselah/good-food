<?php

namespace App\Entity\Product\Category;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\Media\Media;
use App\Entity\Product\PurchasableProduct\PurchasableProduct;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\CategoryRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[ApiResource(
    collectionOperations: [
        "get"  => [],
        "post" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_EDITOR . "')",
        ],
    ],
    itemOperations: [
        "get"    => [],
        "patch"  => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_EDITOR . "')",
        ],
        "delete" => [
            "security" => "is_granted('" . FranchisorEmployee::ROLE_EDITOR . "')",
        ],
    ],
    denormalizationContext: [
        "groups" => [
            "category", "category:write",
        ],
    ],
    normalizationContext: [
        "groups" => [
            "category", "category:read",
        ],
    ],
)]
class Category implements TestableEntityInterface
{

    use SlugNameTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    /**
     * @var Category[]|Collection
     */
    #[ORM\OneToMany(mappedBy: 'parent', targetEntity: self::class)]
    #[ORM\JoinColumn(referencedColumnName: 'slug')]
    #[ApiSubresource]
    private array|Collection $children;

    #[ORM\ManyToOne(targetEntity: Media::class)]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups([
        'category',
        'purchasable_product:read',
    ])]
    private ?Media $image = null;

    #[ORM\Column(type: 'string', length: 50)]
    #[NotBlank(message: "Le nom de la catégorie doit être défini.")]
    #[Length(
        min: 2,
        max: 50,
        minMessage: "Le nom de la catégorie doit faire plus de {{ limit }} caractères.",
        maxMessage: "Le nom de la catégorie doit faire moins de {{ limit }} caractères."
    )]
    #[Groups([
        'category',
        'purchasable_product:read',
    ])]
    private string $name;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'children')]
    #[ORM\JoinColumn(referencedColumnName: 'slug')]
    #[ORM\InverseJoinColumn(referencedColumnName: 'slug')]
    #[Groups('category')]
    private ?Category $parent;

    /**
     * @var PurchasableProduct[]|Collection
     */
    #[ORM\ManyToMany(targetEntity: PurchasableProduct::class, inversedBy: 'categories')]
    #[ORM\JoinColumn(referencedColumnName: 'slug')]
    #[ORM\InverseJoinColumn(referencedColumnName: 'reference')]
    #[ApiSubresource]
    private array|Collection $products;

    #[Pure] public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function addProduct(PurchasableProduct $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    protected function autoActiveOnCreation(): bool
    {
        return false;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection|array
    {
        return $this->children;
    }

    public function getImage(): ?Media
    {
        return $this->image;
    }

    public function setImage(?Media $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|PurchasableProduct[]
     */
    public function getProducts(): Collection|array
    {
        return $this->products;
    }

    public function purgeDataOnDelete(): bool
    {
        return true;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function removeProduct(PurchasableProduct $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }

    public function getEntityName(): string
    {
        return "Catégorie";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "categories";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get",
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }


}
