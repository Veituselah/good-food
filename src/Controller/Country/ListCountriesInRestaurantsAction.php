<?php

namespace App\Controller\Country;

use App\Repository\CountryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListCountriesInRestaurantsAction extends AbstractController
{

    public function __construct(
        private CountryRepository $countryRepository
    )
    {
    }

    public function __invoke(): array
    {
        return $this->countryRepository->getCountriesInRestaurants();
    }

}