<?php

namespace App\Entity\Product\PurchasableProduct\Variation;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\Employee\FranchiseEmployee;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\VariationGroupRepository;
use App\Traits\Fields\Date\ActivatedAtTrait;
use App\Traits\Fields\Date\CreatedAtTrait;
use App\Traits\Fields\Date\DeletedAtTrait;
use App\Traits\Fields\Date\UpdatedAtTrait;
use App\Traits\Fields\Identifiers\SlugNameTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use JetBrains\PhpStorm\Pure;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;


#[ORM\Entity(repositoryClass: VariationGroupRepository::class)]
#[SoftDeleteable(fieldName: "deletedAt")]
#[UniqueEntity(fields: ['name'], message: "Il existe déjà un groupe de déclinaison nommé '{{value}}'")]
#[ApiResource(
    collectionOperations: [
        "get"  => [
            "security" => 'is_granted("' . FranchiseEmployee::ROLE_LOGISTICIAN . '") or is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "post" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    itemOperations: [
        "get"    => [],
        "patch"  => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
        "delete" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "variation_group", "variation_group:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "variation_group", "variation_group:read",
        ],
    ]
)]
class VariationGroup implements TestableEntityInterface
{

    use SlugNameTrait;
    use CreatedAtTrait;
    use UpdatedAtTrait;
    use ActivatedAtTrait;
    use DeletedAtTrait;

    #[ORM\Column(type: 'string', length: 30)]
    #[NotBlank(message: "Le libellé d'un groupe de déclinaison doit être renseigné")]
    #[Length(
        min: 2,
        max: 30,
        minMessage: "Le libellé d'un groupe de déclinaison doit faire plus de {{limit}} caractères",
        maxMessage: "Le libellé d'un groupe de déclinaison doit faire moins de {{limit}} caractères",
    )]
    #[Groups([
        'variation_group',
        "variation_value:read",
    ])]
    private string $name;

    #[ORM\Column(type: 'integer')]
    #[NotNull(message: "La position doit être définie")]
    #[GreaterThanOrEqual(value: 0, message: "La position doit être supérieure ou égale à {{compared_value}}")]
    #[Groups(['variation_group'])]
    private int $position;

    /**
     * @var VariationValue[]|Collection
     */
    #[ORM\OneToMany(mappedBy: 'variatioGroup', targetEntity: VariationValue::class, orphanRemoval: true)]
    #[Groups(['variation_group'])]
    private array|Collection $variationValues;

    #[Pure] public function __construct()
    {
        $this->variationValues = new ArrayCollection();
    }

    public function addVariationValue(VariationValue $variationValue): self
    {
        if (!$this->variationValues->contains($variationValue)) {
            $this->variationValues[] = $variationValue;
            $variationValue->setVariationGroup($this);
        }

        return $this;
    }

    function autoActiveOnCreation(): bool
    {
        return true;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Collection|VariationValue[]
     */
    public function getVariationValues(): array|Collection
    {
        return $this->variationValues;
    }

    function purgeDataOnDelete(): bool
    {
        return false;
    }

    public function removeVariationValue(VariationValue $variationValue): self
    {
        if ($this->variationValues->removeElement($variationValue)) {
            // set the owning side to null (unless already changed)
            if ($variationValue->getVariationGroup() === $this) {
                $variationValue->setVariationGroup(null);
            }
        }

        return $this;
    }

    public function getEntityName(): string
    {
        return "Groupe de déclinaison";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "varation_groups";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [
            "get"
        ];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
