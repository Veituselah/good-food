<?php

namespace App\Command\TestBook;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Command\TestBook\Classes\Pages\SummaryPage;
use App\Command\TestBook\Classes\Pages\TestPage;
use JetBrains\PhpStorm\Pure;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use ReflectionAttribute;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: self::NAME)]
class GenerateTestBookCommand extends Command
{

    protected const NAME = 'test:export';
    protected const ENTITIES_DIR = __DIR__ . '/../../Entity';
    protected const EXCLUDED_FILES = ['.', '..'];
    protected const EXPORT_DIR = __DIR__ . '/export';

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $classes = $this->getAllClassesThatCanBeExportedInTestBook();
            $spreadsheet = new Spreadsheet();

            $sheet = $spreadsheet->getActiveSheet();
            $summaryPage = new SummaryPage($sheet);
            $summaryPage->createPage();

            foreach ($classes as $reflectionClass) {

                $entityToWorksheet = new TestPage($reflectionClass, $spreadsheet);
                $entityToWorksheet->createPage();

            }

            $writer = new Xlsx($spreadsheet);
            $writer->save(self::EXPORT_DIR . '/Recettage-' . date('d-m-Y H:i:s') . '.xlsx');

            return Command::SUCCESS;

        } catch (Exception|ReflectionException|\PhpOffice\PhpSpreadsheet\Exception $e) {
            dd($e);
        }


    }

    /**
     * @return ReflectionClass[]
     * @throws ReflectionException
     */
    protected function getAllClassesThatCanBeExportedInTestBook($dir = self::ENTITIES_DIR): array
    {
        $entities = [];

        $files = scandir($dir);

        foreach ($files as $file) {

            if (!in_array($file, self::EXCLUDED_FILES)) {

                $filePath = $dir . '/' . $file;

                if (is_dir($filePath)) {

                    $entities = array_merge(
                        $entities,
                        $this->getAllClassesThatCanBeExportedInTestBook($filePath)
                    );

                } else if (str_contains($file, '.php')) {

                    include_once $filePath;

                    $pathInfo = pathinfo($filePath);
                    $fileName = $pathInfo['filename'];

                    $classes = get_declared_classes();

                    foreach ($classes as $class) {

                        if (
                            str_contains($class, $fileName) &&
                            str_contains($class, "App\\Entity\\")
                        ) {
                            $reflectionClass = new ReflectionClass($class);
                            $apiResource = $this->getApiResource($reflectionClass);

                            if (!empty($apiResource) && !$reflectionClass->isAbstract()) {
                                $entities[$class] = $reflectionClass;
                            }

                            break;
                        }
                    }

                }

            }

        }

        return array_unique($entities);
    }

    #[Pure]
    public static function getApiResource(ReflectionClass $reflectionClass): ?ReflectionAttribute
    {
        return $reflectionClass->getAttributes(ApiResource::class)[0] ?? null;
    }

}