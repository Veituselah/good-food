<?php

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model;
use ApiPlatform\Core\OpenApi\OpenApi;

final class PasswordGeneratorDecorator implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated
    )
    {
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Password'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'password' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);

        $pathItem = new Model\PathItem(
            ref: 'Password Generator',
            get: new Model\Operation(
            operationId: 'generateRandomPassword',
            tags: ['Tools'],
            responses: [
            '200' => [
                'description' => 'Get password',
                'content' => [
                    'application/json' => [
                        'schema' => [
                            '$ref' => '#/components/schemas/Password',
                        ],
                    ],
                ],
            ],
        ],
            summary: 'Generate random password.',
            requestBody: new Model\RequestBody(
            description: 'Generate random password',
        ),
        ),
        );
        $openApi->getPaths()->addPath('/api/generate_password', $pathItem);

        return $openApi;
    }
}