<?php

namespace App\Security;

use App\Controller\User\Request\UserRequestCreationAction;
use App\Entity\User\Customer\Customer;
use App\Entity\User\User;
use App\Entity\User\UserRequest\AccountConfirmationRequest;
use App\Repository\AccountConfirmationRequestRepository;
use App\Repository\UserRequestRepository;
use App\Service\User\Customer\CustomerConfirmAccount;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{

    public function __construct(
        private EntityManagerInterface               $entityManager,
        private AccountConfirmationRequestRepository $accountConfirmationRequestRepository
    )
    {
    }

    /**
     * @throws Exception
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof User) {
            return;
        }

        if ($user->isDeleted()) {
            // the message passed to this exception is meant to be displayed to the user
            throw new CustomUserMessageAccountStatusException("Votre compte n'existe plus.\nSi cela n'est pas normal veuillez contacter un administrateur.");
        } else if ($user instanceof Customer && !$user->hasConfirmedAccount()) {

            if (
                $this->accountConfirmationRequestRepository->hasPendingRequestWhichDosntHaveReachTimeBeforeNextRequest($user)
            ) {
                $error = 'Une requête de confirmation de compte est déjà en cours (veuillez vérifier vos mails avant d\'en faire une nouvelle d\'ici 2 minutes)';
            } else {
                $customerAccountConfirmRequest = new AccountConfirmationRequest();
                $customerAccountConfirmRequest->setCustomerTarget($user);
                $this->entityManager->persist($customerAccountConfirmRequest);
                $this->entityManager->flush();

                $error = "Un mail vous a été envoyé pour l'activé.";
            }

            throw new CustomUserMessageAccountStatusException($error);
        }else if (!$user->isActive()) {

            $error = "Votre compte n'est pas actif.\n";

            throw new CustomUserMessageAccountStatusException($error);

        }
    }

    public function checkPostAuth(UserInterface $user): void
    {
        if (!$user instanceof User) {
            return;
        }
    }

}