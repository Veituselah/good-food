<?php

namespace App\Command\TestBook\Classes\Pages;

use App\Command\TestBook\Classes\Model\TestLine;
use PhpOffice\PhpSpreadsheet\Exception;

class SummaryPage extends Page
{

    const BORDER_COLOR = '000000';

    const HEADER_EXPLANATION = [
        ["Colonne", "Explication",],
        ["ID", "Identifiant du test (par page)"],
        ["Tâche", "Nom du test"],
        ["Etape", "Description du test"],
        ["Niveau", "Zone de l'application testée"],
        ["Attendu", "Résultat attendu"],
        ["Résultat", "Résultat réel"],
        ["Code résultat", "Code du résultat (voir explications en dessous)"],
        ["Deadline", "Date avant laquelle le test devait être valide"],
        ["Commentaire", "Commentaire pour expliquer le résultat du test"],
    ];

    const RESULT_CODE_EXPLANATION = [
        ["Résultat des tests", "Explication"],
        [TestLine::TEST_OK, "Test validé"],
        [TestLine::TEST_KO, "Test invalidé"],
        [TestLine::TEST_NOT_TESTED, "Non testé"],
    ];

    /**
     * @throws Exception
     */
    public function createPage()
    {
        $data = array_merge(
            self::HEADER_EXPLANATION,
            self::EMPTY_LINE,
            self::RESULT_CODE_EXPLANATION,
        );

        $this->sheet->setTitle("Sommaire");
        $this->sheet->fromArray($data);
        $this->stylePage();
    }

    /**
     * @throws Exception
     */
    protected function stylePage()
    {

        $firstCol = self::FIRST_COL;
        $lastCol = $this->parseColumnNumberToLetter(2);

        foreach (range($firstCol, $lastCol) as $col) {
            $this->sheet->getColumnDimension($col)->setAutoSize(true);
        }

        $firstRowHeader = 1;
        $lastRowHeader = count(self::HEADER_EXPLANATION);
        $this->addBorderToEachCellInRange($firstCol, $lastCol, $firstRowHeader, $lastRowHeader, self::BORDER_COLOR);

        $firstRowResultCode = $lastRowHeader + 2;
        $lastRowResultCode = $firstRowResultCode + count(self::RESULT_CODE_EXPLANATION) - 1;
        $this->addBorderToEachCellInRange($firstCol, $lastCol, $firstRowResultCode, $lastRowResultCode, self::BORDER_COLOR);


    }

}