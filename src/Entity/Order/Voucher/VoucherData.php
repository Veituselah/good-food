<?php

namespace App\Entity\Order\Voucher;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Country\Currency;
use App\Entity\Product\PurchasableProduct\PurchasableProduct;
use App\Entity\User\Customer\Customer;
use App\Entity\User\Employee\FranchisorEmployee;
use App\Interfaces\TestableEntityInterface;
use App\Repository\VoucherDataRepository;
use App\Service\Tools\Tools;
use App\Traits\Fields\Identifiers\IDTrait;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Context\ExecutionContextInterface;


#[ORM\Entity(repositoryClass: VoucherDataRepository::class)]
#[ApiResource(
    collectionOperations: [
        "post" => [
            "security"                => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
            "denormalization_context" => [
                'groups' => [
                    "voucher_data", "voucher_data:write", "voucher_data:post",
                ],
            ],
        ],
    ],
    itemOperations: [
        "get" => [
            "security" => 'is_granted("' . FranchisorEmployee::ROLE_EDITOR . '")',
        ],
    ],
    denormalizationContext: [
        'groups' => [
            "voucher_data", "voucher_data:write",
        ],
    ],
    normalizationContext: [
        'groups' => [
            "voucher_data", "voucher_data:read",
        ],
    ]
)]
class VoucherData implements TestableEntityInterface
{

    use IDTrait;

    #[ORM\Column(type: 'float', nullable: true)]
    #[GreaterThan(value: 0, message: "Le montant de la réduction doit être null ou supérieur à {{compared_value}}")]
    #[Groups([
        'voucher_data',
        'voucher:read',
        'order:read',
    ])]
    private ?float $amountReductionTaxExcluded;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups([
        'voucher_data',
        'voucher:read',
        'order:read',
    ])]
    private ?DateTimeInterface $availableFrom;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups([
        'voucher_data',
        'voucher:read',
        'order:read',
    ])]
    private ?DateTimeInterface $availableTo;

    #[ORM\ManyToOne(targetEntity: Currency::class)]
    #[ORM\JoinColumn(referencedColumnName: 'iso_code', nullable: false)]
    #[NotNull(message: "La devise utilisée pour définir le montant de la réduction doit être renseignée")]
    #[Groups([
        'voucher_data',
        'voucher:read',
        'order:read',
    ])]
    private Currency $currency;

    #[ORM\Column(type: 'boolean')]
    #[Groups([
        'voucher_data',
        'voucher:read',
        'order:read',
    ])]
    private bool $freeShippingCost = false;

    #[ORM\Column(type: 'string', length: 50)]
    #[NotBlank(message: "Le nom doit être renseigné")]
    #[Length(
        min: 1,
        max: 50,
        minMessage: "Le nom doit faire au moins {{ limit }} caractère.",
        maxMessage: "Le nom doit faire maximum {{ limit }} caractères.",
    )]
    #[Groups([
        'voucher_data',
        'voucher:read',
        'order:read',
    ])]
    private string $name;

    /**
     * @var Customer[]|Collection
     */
    #[ORM\ManyToMany(targetEntity: Customer::class, inversedBy: 'voucher')]
    #[ORM\InverseJoinColumn(referencedColumnName: 'uuid')]
    #[Groups([
        'voucher_data',
        'voucher:read',
    ])]
    private array|Collection $onlyForCustomers;

    /**
     * @var PurchasableProduct[]|Collection
     */
    #[ORM\ManyToMany(targetEntity: PurchasableProduct::class)]
    #[ORM\InverseJoinColumn(referencedColumnName: 'reference')]
    #[Groups([
        'voucher_data',
        'voucher:read',
        'order:read',
    ])]
    private array|Collection $onlyForProducts;

    #[ORM\Column(type: 'float', nullable: true)]
    #[GreaterThan(value: 0, message: "Le pourcentage de la réduction doit être null ou supérieur à {{compared_value}}")]
    #[Groups([
        'voucher_data',
        'voucher:read',
        'order:read',
    ])]
    private float $percentageReductionTaxExcluded;

    #[ORM\ManyToOne(targetEntity: Voucher::class, inversedBy: 'voucherData')]
    #[ORM\JoinColumn(referencedColumnName: 'code', nullable: false)]
    #[NotNull(message: "La réduction associée à ces données doit être renseignée")]
    #[Groups([
        'voucher_data:post',
        'voucher_data:read',
        'voucher:read',
        'order:read',
    ])]
    private Voucher $voucher;

    #[Pure] public function __construct()
    {
        $this->onlyForProducts = new ArrayCollection();
        $this->onlyForCustomers = new ArrayCollection();
    }

    public function addOnlyForCustomer(Customer $onlyForCustomer): self
    {
        if (!$this->onlyForCustomers->contains($onlyForCustomer)) {
            $this->onlyForCustomers[] = $onlyForCustomer;
        }

        return $this;
    }

    public function addOnlyForProduct(PurchasableProduct $onlyForProduct): self
    {
        if (!$this->onlyForProducts->contains($onlyForProduct)) {
            $this->onlyForProducts[] = $onlyForProduct;
        }

        return $this;
    }

    #[Pure] public function getAmountReductionTaxExcluded(): ?string
    {
        return Tools::formatPrice($this?->amountReductionTaxExcluded);
    }

    public function setAmountReductionTaxExcluded(?float $amountReductionTaxExcluded): self
    {
        $this->amountReductionTaxExcluded = $amountReductionTaxExcluded;

        return $this;
    }

    public function getAvailableFrom(): ?DateTimeInterface
    {
        return $this->availableFrom;
    }

    public function setAvailableFrom(?DateTimeInterface $availableFrom): self
    {
        $this->availableFrom = $availableFrom;

        return $this;
    }

    public function getAvailableTo(): ?DateTimeInterface
    {
        return $this->availableTo;
    }

    public function setAvailableTo(DateTimeInterface $availableTo): self
    {
        $this->availableTo = $availableTo;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getFreeShippingCost(): ?bool
    {
        return $this->freeShippingCost;
    }

    public function setFreeShippingCost(bool $freeShippingCost): self
    {
        $this->freeShippingCost = $freeShippingCost;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Customer[]
     */
    public function getOnlyForCustomers(): array|Collection
    {
        return $this->onlyForCustomers;
    }

    /**
     * @return Collection|PurchasableProduct[]
     */
    public function getOnlyForProducts(): array|Collection
    {
        return $this->onlyForProducts;
    }

    #[Pure] public function getPercentageReductionTaxExcluded(): ?string
    {
        return Tools::formatPercentage($this?->percentageReductionTaxExcluded);
    }

    public function setPercentageReductionTaxExcluded(?float $percentageReductionTaxExcluded): self
    {
        $this->percentageReductionTaxExcluded = $percentageReductionTaxExcluded;

        return $this;
    }

    public function getVoucher(): ?Voucher
    {
        return $this->voucher;
    }

    public function setVoucher(?Voucher $voucher): self
    {
        $this->voucher = $voucher;

        return $this;
    }

    public function removeOnlyForCustomer(Customer $onlyForCustomer): self
    {
        $this->onlyForCustomers->removeElement($onlyForCustomer);

        return $this;
    }

    public function removeOnlyForProduct(PurchasableProduct $onlyForProduct): self
    {
        $this->onlyForProducts->removeElement($onlyForProduct);

        return $this;
    }

    #[Callback]
    public function validateAvailableDates(ExecutionContextInterface $context, $payload)
    {
        if (!is_null($this->availableFrom) && !is_null($this->availableTo) && $this->availableFrom > $this->availableTo) {
            $context->buildViolation('La date de départ doit être antérieur ou égale à la date de fin.')
                    ->atPath('availableFrom')
                    ->addViolation();
        }
    }

    public function getEntityName(): string
    {
        return "Coupons de réduction Bis";
    }

    public function routesHaveBeenTested(): bool
    {
        return false;
    }

    public function getMappingRouteToTaskName(): array
    {
        return [];
    }

    public function getEntityRouteName(): string
    {
        return "voucher_data";
    }

    public function getRoutesDontNeedingAuth(): array
    {
        return [];
    }

    public function requireBOTest(): bool
    {
        return true;
    }

}
